import useSWR from 'swr';

import { featuredkeywordsApi } from '../api-client/featuredkeywordsApi';

export const useKeyWord = ({ options }) => {
    const { data, error, mutate } = useSWR('/public/featuredkeywords', featuredkeywordsApi.getAll, {
        dedupingInterval: 60 * 60 * 1000, // 1hr
        ...options,
    });
    return {
        data,
        error,
        mutate
    };
};
