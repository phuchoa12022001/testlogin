import useSWR from "swr";

import companyApi from "../../api-client/companyApi";

export const useCompanyWishlist = ({ options, param }) => {
  const { data, error } = useSWR(
    param?.page ? [`/auth/wishlist?page_size=5`, param] : null,
    () => companyApi.getCompanyWishlist(param),
    {
      dedupingInterval: 0,
      ...options,
    }
  );
  const addCompanyWishlist = async (object) => {
    const NewRating = await companyApi.addCompanyWishlist(object);
    return NewRating;
  };
  const loading = data === undefined && error === undefined;
  return {
    loading,
    data,
    error,
    addCompanyWishlist,
  };
};
