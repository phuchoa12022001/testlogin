import useSWR from "swr";

import companyApi from "../../api-client/companyApi";

export const useCompanySlug = ({ options, slug }) => {
  const { data, error, mutate } = useSWR(
    slug ? `/company/${slug}` : null,
    () => companyApi.getcompanySlug(slug),
    {
      dedupingInterval: 60 * 60 * 1000, // 1hr
      ...options,
    }
  );
  const companyConfirm = async (data) => {
    const NewRating = await companyApi.postCompanyConfirm(data);
    mutate();
    return NewRating;
  };
  const loading = data === undefined && error === undefined;
  return {
    companyConfirm ,
    loading,
    data,
    error,
  };
};
