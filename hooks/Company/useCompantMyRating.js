import useSWR from "swr";

import companyApi from "../../api-client/companyApi";

export const useMyCompanyRating = ({ options , page}) => {
  const { data, error } = useSWR(
    page ? `/auth/my-rating?page=${page}` : null,
    () => companyApi.getcompanyMyRating(page),
    {
      dedupingInterval: 60 * 60 * 1000, // 1hr
      ...options,
    }
  );
  const loading = data === undefined && error === undefined;
  return {
    loading,
    data,
    error
  };
};
