import useSWR from "swr";

import companyApi from "../../api-client/companyApi";

export const useCompanyRating = ({ options, id , page }) => {
  const { data, error  } = useSWR(
    id ? `/rating?company_id=${id}?${page}` : null,
    () => companyApi.getcompanyRating(id , page),
    {
      dedupingInterval: 60 * 60 * 1000, // 1hr
      ...options,
    }
  );
  const addcompanyRating = async (object) => {
    const NewRating = await companyApi.addcompanyRating(object);
    return NewRating;
  };
  const loading = data === undefined && error === undefined;
  return {
    loading,
    data,
    error,
    addcompanyRating,
  };
};
