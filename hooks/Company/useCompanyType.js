import useSWR from "swr";

import companyApi from "../../api-client/companyApi";


export const useCompanyTypeApi = ({ options }) => {
  const { data, error, mutate } = useSWR(
    "/public/companyType",
    companyApi.getcompanyTypeAll,
    {
      dedupingInterval: 60 * 60 * 1000, // 1hr
      ...options,
    }
  );
  const loading = data === undefined && error === undefined;
  return {
    loading ,
    data,
    error,
    mutate,
  };
};
