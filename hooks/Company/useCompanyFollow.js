import useSWR from "swr";

import companyApi from "../../api-client/companyApi";

export const useCompanyFollow = ({ options, page }) => {
  const { data, error } = useSWR(
    page ? `/auth/company?page=${page}` : null,
    () => companyApi.getcompanyFollow(page),
    {
      ...options,
    }
  );
  const addcompanyFollow = async (object) => {
    const NewRating = await companyApi.addcompanyFollow(object);
    return NewRating;
  };
  const uploadImagecompanyFollow = async (object) => {
    const NewRating = await companyApi.uploadImagecompanyFollow(object);
    return NewRating;
  };
  const loading = data === undefined && error === undefined;
  return {
    addcompanyFollow,
    uploadImagecompanyFollow,
    loading,
    data,
    error,
  };
};
