import useSWR from "swr";

import { LocationsApi } from "../api-client/locationApi";

export const useLocations = ({ options }) => {
  const { data, error, mutate } = useSWR(
    "/auth/locations",
    LocationsApi.getLocations,
    {   
      ...options,
    }
  );
  const addLocations = async (object) => {
    const api = await LocationsApi.addLocations(object);
    return api;
  };
  const deleteLocations = async (id) => {
    const api = await LocationsApi.deleteLocations(id);
    return api;
  };
  const loading = data === undefined && error === undefined;
  return {
    data,
    loading,
    error,
    addLocations,
    deleteLocations,
    mutate,
  };
};
