import useSWR from 'swr';

import { BlogApi } from '../api-client/BlogApi';

export const useBlog = ({ options }) => {
  const option = {
    dedupingInterval: 60 * 60 * 1000, // 1hr
    ...options,
  }
  const { data, error, mutate } = useSWR('/auth/blog', BlogApi.getAll, option);
  const add = async (data) => {
    await BlogApi.add(data);
    await mutate();
  };

  const deleteBlog = async (id) => {
    await BlogApi.delete(id);

    await mutate(data);
  };
  const edit = async () => {
    await BlogApi.edit(data, id);

    await mutate(null, false);
  };

  const loading = data === undefined && error === undefined;
  return {
    blog: data,
    // error,
    add,
    isLoading : loading,
    deleteBlog,
    edit,
  };
};
