import useSWR from "swr";

import { commentsApi } from "../api-client/commentsApi";

export const useComments = ({ options, params }) => {
  const option = {
    dedupingInterval: 60 * 60 * 1000, // 1hr
    ...options,
  };
  const { data, error, mutate } = useSWR(
    params.id ? `/blog-comment?blog_id=${params.id}&page=${params.page}` : null,
    () => {
      return commentsApi.getAll(params);
    },
    option
  );
  const add = async (data) => {
    await commentsApi.add(data);
  };

  const deleteBlog = async (id) => {
    await commentsApi.delete(id);

    await mutate(data);
  };
  const edit = async () => {
    await commentsApi.edit(data, id);
    await mutate(null, false);
  };

  const loading = data === undefined && error === undefined;
  return {
    data: data,
    // error,
    add,
    loading,
    deleteBlog,
    edit,
  };
};
