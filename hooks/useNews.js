import useSWR from "swr";

import { newApi } from "../api-client/newsApi";

export const useNew = ({ options, page }) => {
  const { data, error, mutate } = useSWR(
    page ? [`/news?page_size=8`, page] : null,
    () => newApi.getAll(page),
    {
      dedupingInterval: 60 * 60 * 1000, // 1hr
      ...options,
    }
  );
  return {
    data,
    error,
    mutate,
  };
};
