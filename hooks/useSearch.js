import useSWR from "swr";
import _isEmpty from "lodash/isEmpty";

import { searchApi } from "../api-client/searchApi";

export const useSearch = (params, loading) => {
  const isBool = !_isEmpty(params) && !loading;
  const result = useSWR(
    isBool? ["/search", params] : null,
    () => searchApi.getAll(params)
  );
  return {
    ...result,
    isLoading: !result.error && !result.data,
    isError: result.error,
  };
};
