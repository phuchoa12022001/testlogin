import useSWR from 'swr';

import { footerApi } from '../api-client/footerApi';

export const useFooter = ({ options }) => {
    const { data, error, mutate } = useSWR('/footer', footerApi.getAll, {
        dedupingInterval: 60 * 60 * 1000, // 1hr
        ...options,
    });
    return {
        data,
        error,
        mutate
    };
};
