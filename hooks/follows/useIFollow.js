import useSWR from "swr";

import { FollowApi } from "../../api-client/followApi";

export const useIFollow = ({ options, page }) => {
  const { data, error, mutate } = useSWR(
    `/auth/follow/i-follow?page_size=6&page=${page}`,
    () => FollowApi.getIFollow(page),
    {
      dedupingInterval: 60 * 60 * 1000, // 1hr
      ...options,
    }
  );
  const loading = data === undefined && error === undefined;
  return {
    data,
    loading ,
    error,
    mutate
  };
};
