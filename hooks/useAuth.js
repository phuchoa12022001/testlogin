import useSWR from "swr";

import { authApi } from "../api-client/authApi";

export const useAuth = ({ options }) => {
  const { data, error, mutate } = useSWR("/auth/profile", {
    dedupingInterval: 60 * 60 * 1000, // 1hr
    revalidateOnFocus: false,
    ...options,
  });

  const login = async (values) => {
    await authApi.login(values);
    await mutate(); // Đợi get profile ==> Thực khi các cái khác
  };

  const logout = async () => {
    await authApi.logout();
    await mutate(null, false);
  };
  const upload = async (form) => {
    const api = await authApi.updateProfile(form);
    return api;
  };
  const edit = async (data) => {
    await authApi.editProfile(data);
  };

  const loading = data === undefined && error === undefined;

  const loggedOut = !!error;

  return {
    profile: data || {},
    error,
    login,
    logout,
    upload,
    loading,
    loggedOut,
    edit,
  };
};
