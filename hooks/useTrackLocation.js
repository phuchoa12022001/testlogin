import { useEffect, useState } from "react";

export function useTrackLocation() {
  const [locationError, setLocationError] = useState("");
  const [isFindingLocation, setIsFindingLocation] = useState(false);

  const success = (position, callback) => {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;

    setLocationError("");
    setIsFindingLocation(false);
    callback({
      lat: latitude,
      long: longitude,
    });
  };

  const error = (e, callback) => {
    setLocationError("UNABLE_LOCATION");
    setIsFindingLocation(false);

    callback("UNABLE_LOCATION");
  };

  const handleTrackLocation = (callback, callbackError) => {
    setIsFindingLocation(true);

    if (!navigator.geolocation) {
      setLocationError("NOT_SUPPORTED_LOCATION");
      callbackError("NOT_SUPPORTED_LOCATION");
      setIsFindingLocation(false);
    } else {
      navigator.geolocation.getCurrentPosition(
        (position) => success(position, callback),
        (e) => error(e, callbackError)
      );
    }
  };

  return {
    locationError,
    isFindingLocation,
    handleTrackLocation,
  };
}

export const UseGetPosition = () => {
  const [coords, setCoords] = useState({
    atitude: null,
    longitude: null,
  });
  const [loading , setLoading ] = useState(true);

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        setCoords({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
      });
      setLoading(false)
    }
  }, []);
  return {loading  , coords};
};
