import useSWR from "swr";

import { publicApi } from "../api-client/publicApi";

export const useServices = (params) => {
  const result = useSWR(
    ["/public/services", params],
    () => publicApi.getAllServices(params),
    {
      dedupingInterval: 60 * 60 * 1000,
    }
  );

  return {
    ...result,
    isLoading: !result.error && !result.data,
    isError: result.error,
  };
};

export const useProvinces = (params) => {
  const result = useSWR(
    ["/public/provinces", params],
    () => publicApi.getAllProvinces(params),
    {
      dedupingInterval: 60 * 60 * 1000,
    }
  );

  return {
    ...result,
    isLoading: !result.error && !result.data,
    isError: result.error,
  };
};

export const useDistricts = (params) => {
  const result = useSWR(
    params.province_id ? ["/public/districts", params] : null,
    () => publicApi.getAllDistricts(params),
    {
      dedupingInterval: 60 * 60 * 1000,
    }
  );

  return {
    ...result,
    isLoading: !result.error && !result.data,
    isError: result.error,
  };
};

export const useWards = (params) => {
  const result = useSWR(
    params.province_id && params.district_id ? ["/public/wards", params] : null,
    () => publicApi.getAllWards(params),
    {
      dedupingInterval: 60 * 60 * 1000,
    }
  );

  return {
    ...result,
    isLoading: !result.error && !result.data,
    isError: result.error,
  };
};

export const useStreets = (params) => {
  const result = useSWR(
    params.province_id && params.district_id
      ? ["/public/streets", params]
      : null,
    () => publicApi.getAllStreets(params),
    {
      dedupingInterval: 60 * 60 * 1000,
    }
  );

  return {
    ...result,
    isLoading: !result.error && !result.data,
    isError: result.error,
  };
};

export const useAddress = ({ province_id, district_id }) => {
  const AllProvinces = useSWR(
    ["/public/provinces"],
    () => publicApi.getAllProvinces(),
    {
      dedupingInterval: 60 * 60 * 1000,
    }
  );
  const AllDistricts = useSWR(
    province_id ? ["/public/districts" + province_id] : null,
    () => publicApi.getAllDistricts({ province_id }),
    {
      dedupingInterval: 60 * 60 * 1000,
    }
  );
  const AllWarn = useSWR(
    province_id && district_id
      ? ["/public/wards", { province_id, district_id }]
      : null,
    () => publicApi.getAllWards({ province_id, district_id }),
    {
      dedupingInterval: 60 * 60 * 1000,
    }
  );
  const AllStreets = useSWR(
    province_id && district_id
      ? ["/public/streets", { province_id, district_id }]
      : null,
    () => publicApi.getAllStreets({ province_id, district_id }),
    {
      dedupingInterval: 60 * 60 * 1000,
    }
  );

  return {
    data: {
      AllProvinces: AllProvinces.data || [],
      AllDistricts: AllDistricts.data || [],
      AllWarn: AllWarn.data || [],
      AllStreets: AllStreets.data || [],
    },
    isLoading:
      !AllProvinces.error &&
      !AllProvinces.data &&
      !AllDistricts.error &&
      !AllDistricts.data &&
      !AllWarn.error &&
      !AllWarn.data &&
      !AllStreets.error &&
      !AllStreets.data,
    isError:
      AllProvinces.error ||
      AllDistricts.error ||
      AllWarn.error ||
      AllStreets.error,
  };
};

export const useCategories = () => {
  const {data , error} = useSWR("categories", () => publicApi.getCategories(), {
    dedupingInterval: 60 * 60 * 1000,
  });
  if (data) {
    for (let i = 0; i < data.length; i++) {
      data[i].label = data[i].name;
      data[i].value = data[i].id;
    }
  }
  return {
    data,
    isLoading: !error && !data,
    isError: error,
  };
};
