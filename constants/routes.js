export const PATH_NAME = {
  ROOT: '/',
  SIGN_IN: '/login',
  REGISTER: '/register',
};
