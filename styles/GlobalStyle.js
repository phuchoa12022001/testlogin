import { createGlobalStyle } from "styled-components";

export const theme = {
  colors: {
    white: "#fff",
    black: "#000",
    black19: "#191919",
    black2E: "#2E2828",
    black4D: "#4D4D4D",
    blackF2: "#F2F2F2",
    black82: "#828282",
    primary: "#007575",
    secondary: "#057642",
    grayEC: "#ECECEC",
    grayD3: "#D3D3D3",
    grayBD: "#BDBDBD",
    grayC4: "#C4C4C4",
    gray66: "#66676B",
    green05: "#057642",
    yellow: "#F2C94C",
    yellowD2: "#D2A10A",
    green: "#219653",
    purple: "#BABAF3",
    red: "#EB5757",
    blue: "#2F80ED",
  },

  border: {
    4: "4px",
  },

  fontSizes: {
    default: "14px !important",
    mobile: "12px !important",
    sizeSM: "10px !important",
    sizeMD: "14px !important",
    sizeLG: "28px !important",
  },

  breakpoint: {
    maxMobileSM: "575px",
    minMobileSM: "575px",
  },
};

export const GlobalStyle = createGlobalStyle`
  body {
    font-family: 'Inter', sans-serif;
    font-weight: 400;
    color: ${({ theme }) => theme.colors.black4D};
    overflow-x: hidden;
  }

  h1, h2, h3, h4, h5, h6, p {
    margin-bottom: 0;
  }
  ul {
    list-style-type: none;
    padding-inline-start: 0;
    margin-bottom: 0;
  }

  a {
    display: inline-block;
    color: inherit;
  }

  .ant-typography {
    color: inherit !important;
  }

  .ant-space{
    display: flex;
  }
  .ant-picker-cell-in-view.ant-picker-cell-selected .ant-picker-cell-inner, .ant-picker-cell-in-view.ant-picker-cell-range-start .ant-picker-cell-inner, .ant-picker-cell-in-view.ant-picker-cell-range-end .ant-picker-cell-inner{
    background:#057642;
  }

  .container {
    width: 100%;
    margin-left: auto;
    margin-right: auto;
    padding: 0 12px;

    @media (min-width: 576px) {
      max-width: 576px;
    }

    @media (min-width: 768px) {
      max-width: 768px;
    }

    @media (min-width: 992px) {
      max-width: 800px;
    }

    @media (min-width: 1200px) {
      max-width: 924px;
    }

    // @media (min-width: 1600px) {
    //   max-width: 1600px;
    // }
  }



  .ant-btn > .anticon {
    line-height: 0;
  }

  div.ant-typography, .ant-typography p {
    margin-bottom: 0;
  }

  @media (max-width: ${theme.breakpoint.maxMobileSM}) {
    .mobileHidden {
        display: none !important;
    }
    .banner__image {
      > span {
        max-width: 50% !important;
      }
      margin-bottom: 13px;
    }
  }

  @media (min-width: ${theme.breakpoint.minMobileSM}) {
    .decktopHidden {
      display: none !important;
    }
  }

  // HAMBURGER MENU
  .ant-dropdown-menu {
    .ant-dropdown-menu-item, .ant-dropdown-menu-submenu-title {
      font-size: 12px;
    }

    .ant-dropdown-menu-item:hover, .ant-dropdown-menu-submenu-title:hover, .ant-dropdown-menu-item.ant-dropdown-menu-item-active, .ant-dropdown-menu-item.ant-dropdown-menu-submenu-title-active, .ant-dropdown-menu-submenu-title.ant-dropdown-menu-item-active, .ant-dropdown-menu-submenu-title.ant-dropdown-menu-submenu-title-active {
      background-color: #ECECEC;
    }

    .dropdown-footer {
      font-size: 10px;
      font-weight: 500;
      color: #828282;

      .links {
        display: flex;
        align-items: center;

        > a {
          &:not(:first-child) {
            padding-left: 10px;
            margin-left: 10px;
            position: relative;

            &::before {
              content: '';
              position: absolute;
              width: 5px;
              height: 5px;
              background-color: currentColor;
              border-radius: 50%;
              top: 50%;
              left: 0;
              transform: translateY(-50%);
            }
          }

          &:hover {
            color: #007575;
          }
        }
      }
    }
  }


  // MENU DRAWER
  .menuMobileDrawer {
    .ant-menu {
      border-right: none;
      font-size: 12px;
    }

    .ant-menu:not(.ant-menu-sub) {
      > .ant-menu-item, .ant-menu-submenu > .ant-menu-submenu-title {
        padding: 0 !important;
      }
    }

    .ant-menu.ant-menu-sub {
      background-color: transparent;
      > .ant-menu-item {
        padding-left: 24px !important;
        padding-right: 0;
      }
    }


    .ant-menu-item {
      &::after {
        content: none;
      }
    }
    .ant-menu-item:hover, .ant-menu-item-active, .ant-menu-submenu-selected, .ant-menu-submenu-arrow, .ant-menu-submenu-title:hover, .ant-menu-item-selected {
      color: ${({ theme }) => theme.colors.primary} !important;
    }

    .ant-menu-item-selected {
      background-color: transparent !important;
    }

    .ant-menu-vertical > .ant-menu-item, .ant-menu-vertical-left > .ant-menu-item, .ant-menu-vertical-right > .ant-menu-item, .ant-menu-inline > .ant-menu-item, .ant-menu-vertical > .ant-menu-submenu > .ant-menu-submenu-title, .ant-menu-vertical-left > .ant-menu-submenu > .ant-menu-submenu-title, .ant-menu-vertical-right > .ant-menu-submenu > .ant-menu-submenu-title, .ant-menu-inline > .ant-menu-submenu > .ant-menu-submenu-title, .ant-menu-sub.ant-menu-inline > .ant-menu-item, .ant-menu-sub.ant-menu-inline > .ant-menu-submenu > .ant-menu-submenu-title {
      height: 24px;
      line-height: 24px;
    }
  }
  /* custom-notification */
  .custom-notification {
    background: ${({ theme }) => theme.colors.primary};
    width: 552px;
    height: auto;
    min-height: 70px;
    display: flex;
    align-items: center;
    
    .ant-notification-notice-with-icon {
      display: flex;
      align-items: center;
    }
    .ant-notification-notice-icon {
      margin-left: 0px;
      margin-right: 10px;
    }
    .ant-notification-notice-message {
      font-weight: 400;
      font-size: 16px;
      line-height: 19px;
      color: #fff;
    }
    .ant-notification-notice-close {
      top: 50%;
      transform: translate(-100% , -50%);
      height: 20px;
      right : 10px;
    }
  }

  /* custom Menu */
  .ant-dropdown-menu-item:hover , .ant-dropdown-menu-submenu-title:hover , 
  .ant-dropdown-menu-item:hover 
   {
    background-color: transparent !important; 
    color: ${({ theme }) => theme.colors.primary};
  }
  .ant-dropdown-menu {
    border-radius: 8px;
  }
`;
