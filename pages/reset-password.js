import Link from 'next/link';
import Image from 'next/image';
import { Row, Col, Typography, Form, Input, Checkbox, Space } from 'antd';

import { PATH_NAME } from '../constants/routes';
import authBg from '../assets/images/authBg.png';
import { AuthStyles, ButtonStyles } from '../components/styles';

export default function ResetPassword() {
  return (
    <AuthStyles>
      <Row gutter={[30, 0]}>
        <Col span={14}>
          <Image src={authBg} layout='responsive' alt='' />
        </Col>
        <Col span={9}>
          <div style={{ marginBottom: 14 }}>
            <Typography.Title level={3} className='title'>
              Đổi mật khẩu
            </Typography.Title>
            <Typography.Paragraph>
              Tạo mật khẩu mới để đăng nhập vào trang
            </Typography.Paragraph>
          </div>

          <Form layout='vertical'>
            <Form.Item name='new_password'>
              <Input.Password placeholder='Mật khẩu mới' />
            </Form.Item>

            <Form.Item name='new_confirm_password'>
              <Input.Password placeholder='Xác nhận mật khẩu' />
            </Form.Item>

            <Form.Item style={{ textAlign: 'center', marginTop: 45 }}>
              <ButtonStyles type='green'>ĐỔI MẬT KHẨU</ButtonStyles>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </AuthStyles>
  );
}
