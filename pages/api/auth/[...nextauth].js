import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import GoogleProvider from 'next-auth/providers/google';
import FacebookProvider from 'next-auth/providers/facebook';
import { authApi } from '../../../api-client/authApi';

export default NextAuth({
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      async authorize(credentials) {
        try {
          const result = await authApi.login({
            email: credentials.email,
            password: credentials.password,
          });

          return result;
        } catch (error) {
          return null;
        }
      },
    }),
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    }),
    FacebookProvider({
      clientId: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
    }),
  ],
  callbacks: {
    async jwt({ token, user, account }) {
      try {
        if (account.provider === 'google') {
          const result = await authApi.loginGoogle(user);

          user = result;
        } else if (account.provider === 'facebook') {
          const result = await authApi.loginFacebook(user);

          user = result;
        }

        if (user) {
          token.accessToken = user.token;
          token.accessTokenExpiry = user.expires_at;
          token.user = user.user;
        }

        return token;
      } catch (error) {
        return token;
      }
    },
    async session({ session, token }) {
      session.accessToken = token.accessToken;
      session.user = token.user;
      session.expires = new Date(token.accessTokenExpiry).toISOString();

      return session;
    },
  },
});
