import React from "react";
import { Row, Col } from "antd";
import styled from "styled-components";

import BoxInfos from "../../components/boxInfo/boxInfo";
import Protected from "../../components/auth/Protected";
import EvaluatePage from "../../components/reviews/evaluatePage";

const CompanymanagerContainer = styled.div`
  .EvaluatePage {
    max-width: 592px;
    padding-right: 24px;
  }
  .Boxinfos {
    width: 100%;
    flex: 1;
  }
  @media only screen and (max-width: 959px) {
    .boxPosts {
      max-width: 100%;
      padding-right: 0px;
    }
    .EvaluatePage {
      max-width: 100%;
      padding-right: 0px;
    }
  }
`;

function Evaluate() {
  return (
    <Protected>
      <CompanymanagerContainer>
        <Row gutter={[0, 0]}>
          <Col
            xxl={{ span: 16 }}
            xl={{ span: 16 }}
            lg={{ span: 24 }}
            md={{ span: 24 }}
            sm={{ span: 24 }}
            xs={{ span: 24 }}
            className="EvaluatePage"
          >
            <EvaluatePage />
          </Col>
          <Col className="Boxinfos">
            <BoxInfos />
          </Col>
        </Row>
      </CompanymanagerContainer>
    </Protected>
  );
}

export default Evaluate;
