import { useEffect, useState, useContext, useRef } from "react";
import { useRouter } from "next/router";
import { Row, Col, Form, Spin } from "antd";
import styled from "styled-components";
import SortPath from "../components/search/SortPath";

import CompanyList from "../components/company/CompanyList";
import FormWrapper from "../components/common/FormWrapper";
import SearchedKeyword from "../components/SearchedKeyword";
import AdvertingItem from "../components/Adverting/AdvertingItem";
import { useSearch } from "../hooks/useSearch";
import {
  FilterLocationModal,
  FilterLocationPath,
} from "../components/search/index";
import SelectBox from "../components/selectBox/selectBox";
import { useCompanyTypeApi } from "../hooks/Company/useCompanyType";
import { useAuth } from "../hooks/useAuth";
import { ThemeContext } from "../Context/Context";

const SearchContainer = styled.div`
  .boxSearch {
    max-width: 592px;
    padding-right : 12px;
  }
  .boxAds {
    max-width: 308px;
    flex: 1;
  }
  .search_ads_title {
    line-height: 28px;
    margin-top: 0px;
    font-size: 18px;
    color: ${({ theme }) => theme.colors.primary};
  }
  .search_ads_list {
    margin-top: 29px;
  }
  @media only screen and (max-width: 959px) {
    .boxSearch,
    .boxAds {
      max-width: 100%;
    }
    .ant-form-item {
      margin-bottom: 12px;
    }
    .search_ads_title {
      margin-top: 24px;
      text-align: center;
      font-size: 12px;
      line-height: 15px;
    }
    .search_ads_list {
      margin-top: 15px;
    }
    .type {
      padding-left: 0px !important;
    }
    .BoxOption {
      display: flex;
    }
    .boxSearch {
      padding-right: 0px;
    }
  }
`;
export default function SearchPage() {
  const formRef = useRef();
  const form = formRef?.current;
  const arr = [1, 2, 3];
  const [select, setSelect] = useState([]);
  const { location, loading } = useContext(ThemeContext);
  const { profile } = useAuth({});
  const router = useRouter();

  //  Biết người có bật ví trị hãy không . vì nếu tắc vị trí nó trả về Bạn đã tắt vị trí
  const locationApi =
    location.address !== "Bạn đã tắt vị trí"
      ? {
          latitude: location.lat,
          longitude: location.long,
        }
      : {};
  const params = {
    ...router.query,
    type: select || [],
    user_id: profile.id,
    ...locationApi,
  };
  const { data: searchData, isLoading } = useSearch(params, loading);
  const { data: CompanyTypes, loading: CompanyTypeloading } = useCompanyTypeApi(
    {}
  );
  const [locationData, setLocationData] = useState({
    province: null,
    district: null,
    ward: null,
    street: null,
  });

  const filterSort = () => {
    if (location.lat && location.long) {
      if (router.query.distance) {
        return "2";
      }
      return "1";
    }
    return undefined;
  };
  useEffect(() => {
    if (form) {
      form.setFieldsValue({
        province: router.query.province,
        district: router.query.district,
        ward: router.query.ward,
        query: router.query.query,
        street: router.query.street,
        type: select,
        sort: filterSort(),
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location, form]);
  const handleChangePage = (page) => {
    router.push(
      {
        pathname: router.pathname,
        query: {
          ...router.query,
          page,
        },
      },
      undefined,
      { shallow: true }
    );
  };

  const handleFormChange = (_, allValues) => {
    let { sort, type, ...convertedAllValues } = { ...allValues };
    if (!convertedAllValues.province) {
      convertedAllValues = {
        ...convertedAllValues,
        district: null,
        ward: null,
        street: null,
      };
    }
    if (type) {
      setSelect(type);
    }
    if (!convertedAllValues.district) {
      convertedAllValues = { ...convertedAllValues, ward: null, street: null };
    }
  };

  return (
    <SearchContainer>
      <Row gutter={[0, 0]}>
        <Col
          xxl={{ span: 16 }}
          xl={{ span: 16 }}
          lg={{ span: 24 }}
          md={{ span: 24 }}
          sm={{ span: 24 }}
          xs={{ span: 24 }}
          className="boxSearch"
        >
          <FormWrapper ref={formRef} onValuesChange={handleFormChange}>
            <Row gutter={[15, 0]}>
              <Col
                lg={{ span: 8 }}
                md={{ span: 8 }}
                sm={{ span: 24 }}
                xs={{ span: 24 }}
              >
                <Form.Item>
                  <FilterLocationModal
                    data={locationData}
                    setData={setLocationData}
                  />
                </Form.Item>
              </Col>
              <Col
                lg={{ span: 8 }}
                md={{ span: 8 }}
                sm={{ span: 24 }}
                xs={{ span: 24 }}
              >
                <Form.Item>
                  <SortPath />
                </Form.Item>
              </Col>
              {!loading && !CompanyTypeloading ? (
                <Col
                  lg={{ span: 8 }}
                  md={{ span: 8 }}
                  sm={{ span: 24 }}
                  xs={{ span: 24 }}
                >
                  <Form.Item name="type">
                    <SelectBox
                      data={CompanyTypes}
                      onChange={setSelect}
                      select={select}
                    />
                  </Form.Item>
                </Col>
              ) : (
                <Spin />
              )}
              <Col xs={{ span: 24 }}>
                <FilterLocationPath data={Object.values(locationData)} />
              </Col>
            </Row>
          </FormWrapper>
          <SearchedKeyword
            total={searchData?.total}
            keyword={router.query.query}
          />
          {!isLoading && !loading ? (
            <CompanyList
              companies={searchData?.data || []}
              current={searchData?.current_page}
              total={searchData?.total}
              onChange={handleChangePage}
              params={params}
            />
          ) : (
            <Spin />
          )}
        </Col>
        <Col
          xxl={{ span: 8 }}
          xl={{ span: 8 }}
          lg={{ span: 24 }}
          md={{ span: 24 }}
          sm={{ span: 24 }}
          xs={{ span: 24 }}
          className="boxAds"
        >
          <div className="search_wapper">
            <p className="search_ads_title">Adverting....</p>
            <div className="search_ads_list">
              {arr.map((item) => (
                <AdvertingItem key={item} />
              ))}
            </div>
          </div>
        </Col>
      </Row>
    </SearchContainer>
  );
}
