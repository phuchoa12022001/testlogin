import { useState } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { Row, Col, message } from "antd";

import authBg from "../assets/images/authBg.png";
import { AuthStyles } from "../components/styles";

import { authApi } from "../api-client/authApi";
import { RequestOTP, VerifyOTP, ConfirmPassword } from "../components/auth";

export default function LoginPage() {
  const router = useRouter();
  const [step, setStep] = useState(0);
  const [email, setEmail] = useState(null);

  const handleSubmitRequestOTP = async (values) => {
    try {
      await authApi.requestOTP(values);

      setEmail(values.email);
      setStep(1);
    } catch (error) {
      message.error(error?.response?.data?.message || "Error");
    }
  };

  const handleSubmitVerifyOTP = async (values) => {
    try {
      await authApi.verifyOTP({ ...values, email });
      setStep(2);
    } catch (error) {
      message.error(error?.response?.data?.message || "Error");
    }
  };

  const handleSubmitConfirmPassword = async ({ password }) => {
    try {
      await authApi.register({ password, email });

      message.success("Đăng ký thành công");

      router.replace("/login");
    } catch (error) {
      message.error(error?.response?.data?.message || "Error");
    }
  };

  return (
    <AuthStyles>
      <Row gutter={[30, 0]}>
        <Col
          xxl={{ span: 14 }}
          xl={{ span: 14 }}
          lg={{ span: 14 }}
          md={{ span: 24 }}
          sm={{ span: 24 }}
          xs={{ span: 24 }}
        >
          <div className="boxImage">
            <Image src={authBg} alt="" />
          </div>
        </Col>
        <Col
          xxl={{ span: 9 }}
          xl={{ span: 9 }}
          lg={{ span: 9 }}
          md={{ span: 24 }}
          sm={{ span: 24 }}
          xs={{ span: 24 }}
          style={{
            display: "flex",
            alignItems: "center"
          }}
        >
          <div className="box">
            {
              {
                0: <RequestOTP onFinish={handleSubmitRequestOTP} />,
                1: <VerifyOTP onFinish={handleSubmitVerifyOTP} />,
                2: <ConfirmPassword onFinish={handleSubmitConfirmPassword} />,
              }[step]
            }
          </div>
        </Col>
      </Row>
    </AuthStyles>
  );
}
