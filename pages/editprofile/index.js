import React, { useEffect, useState } from "react";
import FormData from "form-data";
import {
  Row,
  Col,
  Form,
  Input,
  Radio,
  DatePicker,
  Button,
  Spin,
  Avatar,
  Image,
  message,
  Upload,
} from "antd";
import { PlusOutlined } from "@ant-design/icons";
import moment from "moment";
import { useSWRConfig } from "swr";

import IconSign from "../../assets/icons/Sign In/Icon.png";
import styled from "styled-components";
import Protected from "../../components/auth/Protected";
import BoxInfos from "../../components/boxInfo/boxInfo";
import { useAuth } from "../../hooks/useAuth";
import { authApi } from "../../api-client/authApi";
import TextArea from "antd/lib/input/TextArea";
const ProfileContainer = styled.div`
  .boxPosts {
    max-width: 592px;
    padding-right: 24px;
  }
  .listBlog {
    margin-top: 48px;
  }
  .title-manager-profile {
    border-bottom: 1px solid #ececec;
    padding-bottom: 7px;
  }
  .title-manager-profile h2 {
    color: #007575;
    font-weight: 700;
  }
  .profile-infor {
    padding-top: 12px;
  }
  .ant-form-item-control-input {
    margin-left: 0;
    margin-right: 0;
    outline: none;
    border: none;
    border-color: #d9d9d9 !important;
    box-shadow: none !important;
  }
  .date-control {
    width: 50%;
    height: 25px;
    border-radius: 4px;
    margin-left: 12px;
  }
  .input-antd {
    border: none;
    outline: none;
  }
  .input {
    border: none;
    outline: none;
  }
  .input:focus {
    border: 1px solid #ddd;
    outline: 0;
  }
  .ant-input:focus {
    border: none;
    outline: none;
  }

  .ant-radio-checked .ant-radio-inner {
    border-color: #007575 !important ;
  }

  .ant-radio-checked .ant-radio-inner:after {
    background-color: #007575;
  }

  .ant-radio:hover .ant-radio-inner {
    border-color: #007575;
  }
  .rd-group {
    padding-left: 12px;
  }
  .ant-form-item {
    margin-bottom: 15px;
  }
  button.btn-submit {
    color: #ffffff;
    background: #007575;
    padding-left: 25px;
    padding-right: 30px;
    height: 35px;
    border-radius: 4px;
    gap: 7px;
    display: flex;
    align-items: center;
  }
  img.img-button {
    margin-right: 8px;
  }
  .ant-picker-cell .ant-picker-cell-inner {
    background: #007575 !important;
  }
  .ant-form-item-control-input-content {
    min-width: 130%;
  }
  .ant-picker-cell-in-view.ant-picker-cell-selected {
    background: yellow;
  }
  .span-image {
    margin-left: 10px;
    cursor: pointer;
  }
  .span-image:hover {
    opacity: 0.9;
  }
  .info_avatar {
    width: 100px;
    height: 100px;
    overflow: hidden;

    position: relative;
    cursor: pointer;
  }
  .info_avatar img {
    width: 100%;
    height: 100%;
    display: block;
    object-fit: cover;
  }
  .info_avatar span {
    position: absolute;
    bottom: -30%;
    left: 0;
    width: 100%;
    height: 50%;
    text-align: center;
    color: orange;
    transition: 0.3s ease-in-out;
    background: #fff5;
  }

  @media only screen and (max-width: 959px) {
    .boxPosts {
      max-width: 100%;
    }
    .listBlog {
      margin-top: 27px;
    }
    .boxPosts {
      padding-right: 0px;
    }
  }
  @media only screen and (max-width: 575px) {
    // .ant-form .ant-form-item .ant-form-item-label,
    // .ant-form .ant-form-item .ant-form-item-control {
    //   max-width: 100%;
    // }
    .ant-form-item-control-input {
      width: 65%;
    }
    .ant-form-item-label-left {
      margin-left: 10px !important;
    }
  }
`;

function EditProfile(props) {
  const initState = {
    name: "",
    phone: "",
    email: "",
    address: "",
    date_of_birth: "",
    sex: "",
    avatar: "",
  };
  const { profile, loading, edit, upload } = useAuth({});
  const { mutate } = useSWRConfig();
  const [base64, setBase64] = useState(null);
  const [userData, setuserData] = useState(initState);
  const [hideUserData, setHideUserData] = useState({
    email: false,
    phone: false,
  });
  const [loadingImage, setLoadingImage] = useState(false);
  const [componentSize, setComponentSize] = useState("default");
  const [loadingPage, setLoadingPage] = useState(false);

  const convertEmail = (email) => {
    const [name, domain] = email.split("@");
    return `${name[0]}${new Array(name.length).join("*")}@${domain}`;
  };

  const convertPhone = (profilePhone, mask, n = 1) => {
    return (
      ("" + profilePhone).slice(0, -n).replace(/./g, mask) +
      ("" + profilePhone).slice(-n)
    );
  };

  useEffect(() => {
    if (!loading) {
      setuserData(profile);
    }
  }, [loading]);

  if (!profile) return false;

  const dateFormat = "YYYY/MM/DD";

  const handleChange = (e) => {
    const { name, value } = e.target;
    setuserData({ ...userData, [name]: value });
  };

  const handleDatePickerChange = (date, dateString) => {
    setuserData({ ...userData, date_of_birth: dateString });
  };
  const handleClick = (e) => {
    const { name } = e.target;
    setHideUserData((prev) => ({
      ...prev,
      [name]: true,
    }));
  };
  const handleBlur = (e) => {
    const { name } = e.target;
    setHideUserData((prev) => ({
      ...prev,
      [name]: false,
    }));
  };
  const handleFinish = async () => {
    setLoadingPage(true);
    if (userData.avatar?.name) {
      var form = new FormData();
      form.append("image", userData.avatar);

      const api = await upload(form);
      userData.avatar = api.path;
    }
    await edit(userData);
    mutate("/auth/profile");
    setLoadingPage(false);
  };
  const handleBeforeUpload = (file) => {
    setuserData((prev) => ({ ...prev, avatar: file }));
    if (
      file.type === "image/jpeg" ||
      file.type === "image/png" ||
      file.type === "image/jpg"
    ) {
      const reader = new FileReader();
      reader.addEventListener("loadstart", () => {
        // setIsLoading(true);
      });
      reader.addEventListener("load", () => {
        setBase64(reader.result);
      });

      reader.addEventListener("loadend", () => {
        // setIsLoading(false);
      });

      reader.readAsDataURL(file);
    } else {
      message.error("You can only upload JPG/PNG/JPEG file!");
      return Upload.LIST_IGNORE;
    }
  };
  const valueDate = moment(userData.date_of_birth, dateFormat);

  return (
    <>
      <Spin spinning={loadingPage || loading} className="spin-detail-new">
        <Protected>
          <ProfileContainer>
            <Row gutter={[0, 0]}>
              <Col
                xxl={{ span: 16 }}
                xl={{ span: 16 }}
                lg={{ span: 24 }}
                md={{ span: 24 }}
                sm={{ span: 24 }}
                xs={{ span: 24 }}
                className="boxPosts"
              >
                <div className="title-manager-profile">
                  <h2>Quản lý tài khoản</h2>
                </div>

                <div className="profile-infor">
                  <Form
                    labelCol={{
                      span: 4,
                    }}
                    wrapperCol={{
                      span: 14,
                    }}
                    layout="horizontal"
                    initialValues={{
                      size: componentSize,
                    }}
                    labelAlign="left"
                    size={componentSize}
                    onFinish={handleFinish}
                  >
                    <Form.Item label="Hình ảnh" valuePropName="avatar">
                      <Upload
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader upload-list-inline"
                        accept=".jpeg, .png, .jpg"
                        showUploadList={false}
                        beforeUpload={handleBeforeUpload}
                      >
                        <Spin spinning={loadingImage}>
                          {base64 ? (
                            <Image
                              accept="image/png, image/gif, image/jpeg"
                              src={base64}
                              preview={false}
                              style={{ width: 80, height: 80 }}
                            />
                          ) : userData.avatar ? (
                            <Image
                              accept="image/png, image/gif, image/jpeg"
                              src={userData.avatar}
                              preview={false}
                              alt="avatar"
                              style={{ width: 80, height: 80 }}
                            />
                          ) : (
                            <PlusOutlined style={{ maxWidth: "100%" }} />
                          )}
                        </Spin>
                      </Upload>
                    </Form.Item>
                    <Form.Item
                      label="Họ tên:"
                      rules={[
                        {
                          required: true,
                          message: "Hãy nhập họ tên!",
                        },
                      ]}
                      className="form-item-edit"
                    >
                      <Input
                        className="input-antd"
                        name="name"
                        value={userData.name}
                        onChange={handleChange}
                      />
                    </Form.Item>

                    <Form.Item
                      label="Email:"
                      rules={[
                        {
                          required: true,
                          message: "Hãy nhập email!",
                        },
                      ]}
                    >
                      <Input
                        className="input-antd"
                        name="email"
                        onClick={handleClick}
                        onBlur={handleBlur}
                        value={
                          !hideUserData.email
                            ? convertEmail(userData.email)
                            : userData.email
                        }
                        onChange={handleChange}
                      />
                    </Form.Item>
                    <Form.Item
                      label="Điện thoại:"
                      rules={[
                        {
                          required: true,
                          message: "Hãy nhập số điện thoại!",
                        },
                      ]}
                    >
                      <Input
                        className="input-antd"
                        name="phone"
                        placeholder={
                          userData.phone ? userData.phone : "Chưa cập nhật"
                        }
                        value={
                          !hideUserData.phone
                            ? convertPhone(userData.phone, "*", 3)
                            : userData.phone
                        }
                        onClick={handleClick}
                        onBlur={handleBlur}
                        onChange={handleChange}
                      />
                    </Form.Item>
                    <Form.Item
                      label="Địa chỉ:"
                      rules={[
                        {
                          required: true,
                          message: "Hãy nhập địa chỉ!",
                        },
                      ]}
                    >
                      <TextArea
                        className="input-antd input-address"
                        name="address"
                        placeholder={
                          userData.address ? userData.address : "Chưa cập nhật"
                        }
                        value={userData.address ? userData.address : ""}
                        onChange={handleChange}
                        autoSize={{
                          minRows: 2,
                          maxRows: 6,
                        }}
                      />
                    </Form.Item>
                    <Form.Item label="Giới tính:">
                      <Radio.Group
                        onChange={handleChange}
                        value={userData.sex}
                        name="sex"
                        className="rd-group"
                      >
                        <Radio value={1} className="radio-antd">
                          Nam
                        </Radio>
                        <Radio value={0} className="radio-antd">
                          Nữ
                        </Radio>
                      </Radio.Group>
                    </Form.Item>
                    {userData && (
                      <Form.Item label="Ngày sinh:">
                        <DatePicker
                          name="date_of_birth"
                          className="date-control "
                          // value={
                          //   userData.date_of_birth ? valueDate : "Khong co"
                          // }
                          value={userData.date_of_birth ? valueDate : undefined}
                          format={dateFormat}
                          onChange={handleDatePickerChange}
                        />
                      </Form.Item>
                    )}
                    <Form.Item>
                      <Button className="btn-submit" htmlType="submit">
                        <span className="image-button-sign">
                          <Image
                            src="https://res.cloudinary.com/luuphuchung2810/image/upload/v1659344841/avatar/Icon_jn4fgd.png"
                            alt=""
                            className="img-button "
                            width={14}
                            height={15}
                            preview={false}
                          />
                        </span>

                        <span>Chỉnh sửa thông tin</span>
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </Col>
              <Col
                xxl={{ span: 8 }}
                xl={{ span: 8 }}
                lg={{ span: 24 }}
                md={{ span: 24 }}
                sm={{ span: 24 }}
                xs={{ span: 24 }}
              >
                <BoxInfos />
              </Col>
            </Row>
          </ProfileContainer>
        </Protected>
      </Spin>
    </>
  );
}

export default EditProfile;
