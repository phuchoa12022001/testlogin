import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import styled from "styled-components";
import { footerApi } from "../../api-client/footerApi";


const SlugContainer = styled.div`
  .name {
    color:  ${({ theme }) => theme.colors.primary};
    font-size : 30px;
  }
  .title {
    color: rgba(0,0,0,0.5);
    font-size : 12px;
  }
`
function FooterId() {
  const [data, setData] = useState({});
  const router = useRouter();
  const { slug } = router.query;
  useEffect(() => {
    if (slug) {
      footerApi.getSlug(slug).then((res) => {
        setData(res);
      });
    }
  }, [slug]);
  return (
    <SlugContainer>
      <h3 className="name">{data.name}</h3>
      <p className="title">{data.title}</p>
      <>
          <div dangerouslySetInnerHTML={{ __html: data.content }} />
        </>
    </SlugContainer>
  );
}

export default FooterId;
