/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */

import React, { useState, useRef, useEffect } from "react";
import { Row, Col, Carousel, Spin, Button } from "antd";
import { useRouter } from "next/router";
import styled from "styled-components";

import { LinkIcon } from "../../assets/icons";
import {
  PrintIcon,
  ShareIcon,
  MailIcon2,
  ActiveIcon,
} from "../../assets/icons/index";
import { useAuth } from "../../hooks/useAuth";
import Popconfirm from "../../components/form/Popconfirm";
import UserCompanyinfo from "../../components/company/UserCompanyinfo";
import OnlineStatus from "../../components/OnlineStatus";
import InfoList from "../../components/company/infoList";
import TitleCompany from "../../components/company/title";
import GoogleMapWithSearch from "../../components/GoogleMap/GoogleMapWithSearch";
import Dots from "../../components/company/dots";
import { useCompanySlug } from "../../hooks/Company/useCompanySlug";
import Evaluate from "../../components/reviews/evaluate";
import Head from "next/head";

const CompanyInfoContainer = styled.div`
  .boxInfo {
    max-width: 592px;
    padding-right: 24px;
    .career {
      display: flex;
      align-items: center;
      color: ${({ theme }) => theme.colors.green05};
      font-size: 12px;
      line-height: 15px;
      margin-bottom: 13px;
    }
    .action {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 15px;
    }
    .career__icon {
      margin-right: 7px;
      color: #fff;
      path {
        stroke: ${({ theme }) => theme.colors.green05};
      }
    }
    .name {
      font-weight: 700;
      font-size: 16px;
      line-height: 19px;
      margin-bottom: 5px;
    }
    .action__icon {
      margin-right: 10px;
      color: #fff;
      path {
        stroke: #057642;
      }
    }
    .action__profile {
      align-items: center;
      display: flex;
      position: relative;
    }
    .UserCompanyinfo {
      margin-right: 5px;
    }
    .desc {
      font-weight: 400;
      font-size: 12px;
      line-height: 15px;
      width: 560px;
      color: ${({ theme }) => theme.colors.black4D};
      margin-bottom: 9px;
    }
  }
  .infoList {
    margin-bottom: 12px;
    transform: translateX(0px);
  }
  .imageCompany {
    margin: 15px 0px;
  }
  .ant-carousel .slick-dots-bottom {
    display: none !important;
  }
  .box__Career {
    margin-top: 20px;
  }
  .box {
    h3 {
      margin-bottom: 10px;
      font-weight: 700;
      font-size: 12px;
      line-height: 20px;
      color: ${({ theme }) => theme.colors.primary};
    }
  }
  .geo {
    margin-bottom: 15px;
  }
  .box__desc {
    display: flex;
    align-items: center;
    margin-bottom: 6px;
    p {
      font-weight: 400;
      font-size: 12px;
      line-height: 15px;
      color: ${({ theme }) => theme.colors.black82};
    }
  }
  .evaluate__title {
    margin-bottom: 15px;
  }
  .evaluate {
    background: ${({ theme }) => theme.colors.primary};
    border-radius: 8px;
    width: 544px;
    .evaluate__box {
      display: flex;
    }
    .evaluate__left {
      border-right: 1px solid #fff;
      h3 {
        font-weight: 700;
        font-size: 28px;
        line-height: 34px;
        color: #ffffff;
        margin-left: 25px;
        margin-right: 20px;
      }
      display: flex;
    }
    .evaluate__right {
      h3 {
        font-weight: 700;
        font-size: 20px;
        line-height: 24px;
        color: #ffffff;
        margin-left: 26px;
      }
      display: flex;
      align-items: center;
    }
    .evaluate__left,
    .evaluate__right {
      width: 100%;
      padding: 15px 0px 11px 0px;
    }
  }
  .confirm {
    font-size: 12px;
    line-height: 15px;
    cursor: pointer;
  }
  .Popconfirm {
    position: absolute;
    top: 150%;
    right: 0px;
    width: 250px;
    background: #fff;
    padding: 10px 0px;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 0 3px 6px -4px #0000001f, 0 6px 16px #00000014,
      0 9px 28px 8px #0000000d;
    z-index: 90;
    .title {
      font-size: 12px;
    }
    .btn {
      margin-left: 10px;
    }
    .action {
      display: flex;
      width: 100%;
      justify-content: center;
      margin-top: 10px;
    }
  }
  @media only screen and (max-width: 959px) {
    .boxInfo {
      max-width: 100%;
      padding-right: 0px;
    }
  }
`;
function CompanyInfo() {
  const [location, setLocation] = useState({
    lat: 10.7994154,
    lng: 106.7116815,
  });
  const [loadinglocation, setLoadingLocation] = useState(true);
  const [open, setOpen] = useState(false);
  const { profile } = useAuth({});
  const isBoolBoxLater = !!(profile.name || profile.email);
  const router = useRouter();
  const { slug } = router.query;
  const { data, loading } = useCompanySlug({ slug });
  useEffect(() => {
    if (!loading) {
      setLoadingLocation(false);
      setLocation({
        lat: +data.horizontal_axis,
        lng: +data.vertical_axis,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);
  const DataCategories = !loading
    ? data.categories.length > 0
      ? data.categories.reduce(
          (previousValue, currentValue) => [
            ...previousValue,
            currentValue.name,
          ],
          []
        )
      : data.category
      ? data.category.split(", ")
      : []
    : [];
  const carouselRef = useRef();
  const handleChange = (value) => {
    setLocation(() => {
      return {
        lat: +value.lat(),
        lng: +value.lng(),
      };
    });
  };
  const handleConfirm = () => {
    if (isBoolBoxLater) {
      router.push(`/companies/confirm/${data.id}`);
    } else {
      setOpen(true);
    }
  };
  function fullAddress(data) {
    const Address =
      `${data?.address} ${data?.street?.prefix} ${data?.street?.name}, ${data?.ward?.prefix} ${data?.ward?.name}, ` +
      `${data?.district?.prefix} ${data?.district?.name}, ${data?.province?.name}`;
    return Address;
  }
  return (
    <CompanyInfoContainer>
      <Head>
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"
        />
      </Head>
      <Row gutter={[24, 0]}>
        {!loading ? (
          <Col
            xxl={{ span: 16 }}
            xl={{ span: 16 }}
            lg={{ span: 24 }}
            md={{ span: 24 }}
            sm={{ span: 24 }}
            xs={{ span: 24 }}
            className="boxInfo"
          >
            <div className="career">
              <LinkIcon className="career__icon" />
              <p>Kinh doanh bất động sản</p>
            </div>
            <h3 className="name">{data.name}</h3>
            <div className="action">
              <div className="action__box">
                <PrintIcon className={"action__icon"} />
                <ShareIcon className={"action__icon"} />
                <MailIcon2 className={"action__icon"} />
              </div>
              <div className="action__profile">
                {data?.user_info ? (
                  <>
                    <UserCompanyinfo
                      img={data?.user_info?.avatar}
                      name={data?.user_info?.name || data?.user_info?.email}
                      className="UserCompanyinfo"
                    />
                    <OnlineStatus isOnlineStatus={true} />
                  </>
                ) : (
                  <a className="confirm" onClick={handleConfirm}>
                    Xác nhận
                  </a>
                )}
                {open && (
                  <Popconfirm className={"Popconfirm"} setBool={setOpen}>
                    <p className="title">
                      Bạn chưa đăng nhập bạn có muốn đăng nhập để xác nhận công
                      ty
                    </p>
                    <div className="action">
                      <Button
                        type="primary"
                        size="small"
                        className="btn"
                        onClick={() => router.push("/login")}
                      >
                        Đồng ý
                      </Button>
                      <Button
                        size="small"
                        className="btn"
                        onClick={() => setOpen(false)}
                      >
                        Hủy bỏ
                      </Button>
                    </div>
                  </Popconfirm>
                )}
              </div>
            </div>
            <p className="desc">{data.description && data.description}</p>
            <InfoList
              className={"infoList"}
              fullAddress={fullAddress(data)}
              geo={data.fullAddress}
              website={data.website}
              mobiles={data.mobiles}
              phones={data.phones}
              email={data.email}
            />
            <TitleCompany title={"Vị trí công ty"} className="geo" />
            {!loading && !loadinglocation ? (
              <GoogleMapWithSearch
                onChange={handleChange}
                value={location}
                hideAutocomplete={true}
                height="100px"
              />
            ) : (
              false
            )}
            {data.images.length !== 0 && (
              <>
                <TitleCompany
                  title={"Hình ảnh công ty"}
                  className="imageCompany"
                />
                <Carousel slidesToShow={3} ref={carouselRef} dots={false}>
                  <div className="item">
                    <img src="/static/images/slide.png" />
                    <p>1</p>
                  </div>
                  <div className="item">
                    <img src="/static/images/slide.png" />
                    <p>2</p>
                  </div>
                  <div className="item">
                    <img src="/static/images/slide.png" />
                    <p>3</p>
                  </div>
                  <div className="item">
                    <img src="/static/images/slide.png" />
                    <p>4</p>
                  </div>
                  <div className="item">
                    <img src="/static/images/slide.png" />
                    <p>5</p>
                  </div>
                </Carousel>

                <Dots number={3} refs={carouselRef} />
              </>
            )}
            {data.category && (
              <div className="box box__Career">
                <h3>Ngành nghề kinh doanh</h3>
                {DataCategories.map((item, index) => (
                  <div className="box__desc" key={index}>
                    <ActiveIcon />
                    <p>{item}</p>
                  </div>
                ))}
              </div>
            )}
            <div className="box">
              <h3>Chi nhánh</h3>
              <div className="box__desc">
                <ActiveIcon />
                <p>Quận Bình Thạnh</p>
              </div>
              <div className="box__desc">
                <ActiveIcon />
                <p>Quận 10</p>
              </div>
              <div className="box__desc">
                <ActiveIcon />
                <p>Quận Bình Tân</p>
              </div>
            </div>
            <TitleCompany
              title={"Đánh giá chung"}
              className="evaluate__title"
            />
            <Evaluate id={data.id} />
          </Col>
        ) : (
          <Spin />
        )}
      </Row>
    </CompanyInfoContainer>
  );
}

export default CompanyInfo;
