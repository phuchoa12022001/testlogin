import React, { useRef, createRef, useState } from "react";
import styled from "styled-components";
import { useRouter } from "next/router";
import { Form, Button } from "antd";

import InputForm from "../../../components/form/input";
import { useCompanySlug } from "../../../hooks/Company/useCompanySlug";
import openNotificationWithIcon from "../../../components/notification/Notification";
import { useAuth } from "../../../hooks/useAuth";
import Protected from "../../../components/auth/Protected";
import Recaptcha from "../../../components/recaptcha/reCaptcha";
import { CheckIcon } from "../../../assets/icons/index";

const ConfirmStyle = styled.div`
  .box {
    display: flex;  
    width: 100%;
    justify-content: center;
    align-items: center;
    .add {
      width: 150px;
    }
  }
  .error {
    color: red;
    margin-bottom: 10px;
  }
`;
const layout = {
  labelCol: {
    span: 24,
  },
  wrapperCol: {
    span: 24,
  },
};

function Confirm() {
  const router = useRouter();
  const { profile, loading } = useAuth({});
  const [error, setError] = useState("");
  const form = useRef();
  const { company_id } = router.query;
  const recaptchaRef = createRef(null);
  const { companyConfirm } = useCompanySlug({});

  const handleChangeRecaptcha = () => {
    setError("");
  };
  const onFinish = async (values) => {
    const token = recaptchaRef.current.getValue();
    if (!token) {
      setError("Bạn chưa xác nhận tôi không phải là người máy");
    } else {
      companyConfirm({ ...values, company_id: +company_id });
      router.back();
      openNotificationWithIcon(
        "success",
        "Bạn đã gữi xác nhận thành công",
        <CheckIcon className={"CheckIcon"} />
      );
    }
  };
  return (
    <Protected>
      <ConfirmStyle>
        {!loading && (
          <Form
            name="form"
            onFinish={onFinish}
            ref={form}
            initialValues={profile}
          >
            <InputForm
              name={"name"}
              label={"Tên"}
              placeholder="Nhập tên công ty xác nhận"
              focus={true}
              layout={layout}
            />
            <InputForm
              name={"email"}
              label={"Email"}
              placeholder="Nhập tên email  xác nhận"
              layout={layout}
            />
            <InputForm
              name={"phone"}
              label={"Điện thoại"}
              placeholder="Nhập tên điện thoại xác nhận"
              layout={layout}
            />
            <Recaptcha ref={recaptchaRef} onChange={handleChangeRecaptcha} />
            <p className="error">{error}</p>
            <div className="box">
              <Button type="primary" className="add" htmlType="submit">
                Gữi xác nhận
              </Button>
            </div>
          </Form>
        )}
      </ConfirmStyle>
    </Protected>
  );
}

export default Confirm;
