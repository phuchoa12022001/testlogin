import { SessionProvider } from 'next-auth/react';
import { ThemeProvider } from 'styled-components';
import { SWRConfig } from 'swr';
import { ConfigProvider } from 'antd';
import viVN from 'antd/lib/locale/vi_VN';
require("../styles/variables.less");


import { GlobalStyle, theme } from '../styles/GlobalStyle';

import MainLayout from '../layout/MainLayout';
import axiosInstance from '../api-client/api';

function MyApp({ Component, pageProps: { session, ...pageProps } }) {
  return (
    <SessionProvider session={session}>
      <SWRConfig
        value={{
          fetcher: (url) => axiosInstance.get(url),
          shouldRetryOnError: false,
          revalidateOnFocus: false,
        }}
      >
        <ConfigProvider locale={viVN}>
          <ThemeProvider theme={theme}>
            <GlobalStyle />
            <MainLayout>
              <Component {...pageProps} />
            </MainLayout>
          </ThemeProvider>
        </ConfigProvider>
      </SWRConfig>
    </SessionProvider>
  );
}

export default MyApp;
