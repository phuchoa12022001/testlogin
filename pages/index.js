import Image from "next/image";
import { Row, Col } from "antd";

import Home from "../components/home/Home";
import PopularKeywords from "../components/popular-keyword/PopularKeywords";
import NewsList from "../components/news/NewsList";

export default function HomePage() {
  return (
    <div>
      <Row gutter={[24, 0]}>
        <Col
          span={24}
          order={2}
          lg={{ span: 12 }}
          md={{ span: 24, order: 1 }}
          sm={{ span: 24, order: 2 }}
          xs={{ span: 24, order: 2 }}
        >
          <Home />

          <PopularKeywords />
        </Col>

        <Col
          span={24}
          order={1}
          lg={{ span: 12 }}
          md={{ span: 0, order: 2 }}
          sm={{ span: 0, order: 2 }}
          xs={{ span: 0, order: 2 }}
          style={{ textAlign: "center" }}
          className="banner__image"
        >
          <Image
            src="/static/images/hero.png"
            alt=""
            width={517}
            height={347}
            objectFit="cover"
          />
        </Col>

        <Col span={24} order={3}>
          <NewsList />
        </Col>
      </Row>
    </div>
  );
}
