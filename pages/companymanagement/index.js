import React from "react";
import { Row, Col } from "antd";
import styled from "styled-components";

import BoxInfos from "../../components/boxInfo/boxInfo";
import Protected from "../../components/auth/Protected";
import Companyfollow from "../../components/companymanagement/companyfollow";

const CompanymanagerContainer = styled.div`
  .SettingAccounnt {
    max-width: 592px;
    padding-right: 24px;
  }
  @media only screen and (max-width: 959px) {
    .SettingAccounnt {
      max-width: 100%;
    }
  }
`;
function Companymanager() {
  return (
    <Protected>
      <CompanymanagerContainer>
        <Row gutter={[24, 0]}>
          <Col
            xxl={{ span: 16 }}
            xl={{ span: 16 }}
            lg={{ span: 24 }}
            md={{ span: 24 }}
            sm={{ span: 24 }}
            xs={{ span: 24 }}
            className="SettingAccounnt"
          >
            <Companyfollow />
          </Col>
          <Col
            xxl={{ span: 8 }}
            xl={{ span: 8 }}
            lg={{ span: 24 }}
            md={{ span: 24 }}
            sm={{ span: 24 }}
            xs={{ span: 24 }}
          >
            <BoxInfos />
          </Col>
        </Row>
      </CompanymanagerContainer>
    </Protected>
  );
}

export default Companymanager;
