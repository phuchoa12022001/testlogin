import { useEffect } from "react";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import {
  Row,
  Col,
  Typography,
  Form,
  Input,
  Checkbox,
  Space,
  message,
} from "antd";
import { signIn, useSession } from "next-auth/react";

import { PATH_NAME } from "../constants/routes";
import authBg from "../assets/images/authBg.png";
import { AuthStyles, ButtonStyles } from "../components/styles";

export default function LoginPage() {
  const router = useRouter();
  const { data: session, status } = useSession();
  const loading = status === "loading";

  useEffect(() => {
    if (session) {
      router.push("/editprofile");
    }
  }, [session, router]);

  const handleSubmit = async (values) => {
    const { ok } = await signIn("credentials", {
      ...values,
      redirect: false,
    });
    if (ok) {
      router.push("/editprofile");
    } else {
      message.error("Incorrect account or password");
    }
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <AuthStyles>
      <Row gutter={[30, 0]}>
        <Col
          xxl={{ span: 14 }}
          xl={{ span: 14 }}
          lg={{ span: 14 }}
          md={{ span: 24 }}
          sm={{ span: 24 }}
          xs={{ span: 24 }}
        >
          <div className="boxImage">
            <Image src={authBg} height={450} width={520} alt="" />
          </div>
        </Col>
        <Col
          xxl={{ span: 9 }}
          xl={{ span: 9 }}
          lg={{ span: 9 }}
          md={{ span: 24 }}
          sm={{ span: 24 }}
          xs={{ span: 24 }}
          style={{
            display: "flex",
            alignItems: "center"
          }}
        >
          <div className="box">
            <div style={{ marginBottom: 14 }}>
              <Typography.Title level={3} className="title">
                Đăng nhập
              </Typography.Title>
              <Typography.Paragraph className="login">
                Đăng nhập bằng
              </Typography.Paragraph>

              <Space size={12} style={{ margin: "4px 0" }}>
                <ButtonStyles
                  type="blue"
                  onClick={() => signIn("facebook", { callbackUrl: "/" })}
                >
                  FACEBOOK
                </ButtonStyles>
                <ButtonStyles
                  type="red"
                  onClick={() => signIn("google", { callbackUrl: "/" })}
                >
                  GOOGLE
                </ButtonStyles>
              </Space>

              <Typography.Paragraph className="text">
                Hoặc sử dụng tài khoản của bạn
              </Typography.Paragraph>
            </div>

            <Form layout="vertical" onFinish={handleSubmit}>
              <Form.Item name="email">
                <Input placeholder="Email *" />
              </Form.Item>

              <Form.Item name="password">
                <Input.Password placeholder="Mật khẩu *" />
              </Form.Item>

              <Form.Item>
                <Space style={{ justifyContent: "space-between" }}>
                  <Form.Item
                    name="remember"
                    valuePropName="checked"
                    style={{ marginBottom: 0 }}
                  >
                    <Checkbox>Ghi nhớ thông tin đăng nhập</Checkbox>
                  </Form.Item>

                  <Link href="/">
                    <a
                      className="link forgotpassword"
                      style={{ fontSize: 10, fontWeight: 500 }}
                    >
                      Quên mật khẩu
                    </a>
                  </Link>
                </Space>
              </Form.Item>

              <Form.Item  className="formStyle">
                <ButtonStyles type="green" htmlType="submit">
                  ĐĂNG NHẬP
                </ButtonStyles>
              </Form.Item>
            </Form>

            <Typography.Paragraph
              style={{ marginTop: 50 }}
              className="more"
            >
              Bạn đã có tài khoản?{" "}
              <Link href={PATH_NAME.REGISTER}>
                <a className="link">Đăng ký ngay</a>
              </Link>
            </Typography.Paragraph>
          </div>
        </Col>
      </Row>
    </AuthStyles>
  );
}
