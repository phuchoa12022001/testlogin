import React from "react";
import { Row, Col, Spin } from "antd";
import styled from "styled-components";

import CreatePost from "../components/Post/CreatePost";
import { useBlog } from "../hooks/useBlog";
import BoxInfos from "../components/boxInfo/boxInfo";
import Blogs from "../components/Blog/Blog";
import Protected from "../components/auth/Protected";

const ProfileContainer = styled.div`
  .boxPosts {
    max-width: 592px;
    padding-right: 24px;
  }
  .listBlog {
    margin-top: 48px;
  }
  .Boxinfos {
    width: 100%;
    flex: 1;
  }
  @media only screen and (max-width: 959px) {
    .boxPosts {
      max-width: 100%;
      padding-right: 0px;
    }
    .listBlog {
      margin-top: 27px;
    }
  }
`;
function Profile() {
  const { blog, deleteBlog, isLoading } = useBlog({});
  const listBlog = blog || [];
  return (
    <Protected>
      <ProfileContainer>
        <Row gutter={[0, 0]}>
          <Col
            xxl={{ span: 16 }}
            xl={{ span: 16 }}
            lg={{ span: 24 }}
            md={{ span: 24 }}
            sm={{ span: 24 }}
            xs={{ span: 24 }}
            className="boxPosts"
          >
            <CreatePost />
            <div className="listBlog">
              {isLoading ? (
                <Spin />
              ) : (
                listBlog.map((item) => (
                  <Blogs key={item.id} {...item} deleteBlog={deleteBlog} />
                ))
              )}
            </div>
          </Col>
          <Col className="Boxinfos">
            <BoxInfos />
          </Col>
        </Row>
      </ProfileContainer>
    </Protected>
  );
}

export default Profile;
