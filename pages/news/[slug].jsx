import React from "react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import styled from "styled-components";
import { newApi } from "../../api-client/newsApi";
import NewsList from "../../components/news/NewsList";
import { Row, Spin } from "antd";

const SlugContainer = styled.div`
  .content-new {
    width: 77%;
    padding: 0 15px;
    margin-top: -15px;
  }
  .newlist {
    margin-bottom: 50px;
    margin-top: 50px;
  }
  .newlist .date {
    display: flex;
    flex-direction: column;
  }
  .newlist h2 {
    display: none;
  }
  .newlist a.load__more {
    display: none;
  }
  .date {
    display: flex;
    justify-content: end;
    padding-bottom: 10px;
  }
  .main-content h3 {
    font-weight: 700;
    font-size: 24px;
    padding-bottom: 10px;
    padding-top: 10px;
  }
  .main-content .maim-content-detail {
    padding-top: 10px;
    padding-bottom: 10px;
  }
  @media (max-width: 990px) and (min-width: 770px) {
    .content-new {
      width: 100%;
    }
    .content-new-date {
      margin-right: 70px;
    }
  }
  @media (max-width: 990px) and (min-width: 770px) {
    .content-new {
      width: 100%;
    }
    .content-new-date {
      margin-right: 70px;
    }
  }
  @media (max-width: 770px) and (min-width: 500px) {
    .content-new {
      width: 100%;
    }
    .content-new-date {
      margin-right: 45px;
    }
  }
  @media (max-width: 500px) {
    .content-new {
      width: 100%;
    }
    .content-new-date {
      margin-right: 0px;
    }
  }
`;

const NewId = () => {
  const weekday = [
    "Chủ nhật ",
    "Thứ 2",
    "Thứ 3 ",
    "Thứ tư",
    "Thứ năm",
    "Thứ sáu",
    "Thứ 7",
  ];

  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const { slug } = router.query;
  useEffect(() => {
    setLoading(true);
    if (slug) {
      newApi
        .getSlug(slug)
        .then((res) => {
          console.log(res);
          setData(res);
        })
        .finally(() => setLoading(false));
    }
  }, [slug]);
  const d = new Date(data.created_at);
  let date = weekday[d.getDay()];
  let day = d.getDate();
  let month = d.getMonth() + 1;

  let year = d.getFullYear();
  let fullDate = `${date}, ${day}/${month}/${year}`;

  return (
    <>
      <Spin spinning={loading} className="spin-detail-new">
        <SlugContainer>
          <div className="content-new">
            <div className="content-new-date">
              <Row className="date">
                {date && day && month && year !== null ? fullDate : ""}
              </Row>
            </div>
            <div className="main-content">
              <h3>{data.title}</h3>
              <div className="maim-content-detail">
                <div dangerouslySetInnerHTML={{ __html: data.detail }} />
              </div>
            </div>
          </div>

          <div className="newlist">
            <NewsList />
          </div>
        </SlugContainer>
      </Spin>
    </>
  );
};

export default NewId;
