import Link from 'next/link';
import Image from 'next/image';
import { Row, Col, Typography, Form, Input, Checkbox, Space } from 'antd';

import { PATH_NAME } from '../constants/routes';
import authBg from '../assets/images/authBg.png';
import { AuthStyles, ButtonStyles } from '../components/styles';

export default function ForgotPassword() {
  return (
    <AuthStyles>
      <Row gutter={[30, 0]}>
        <Col span={14}>
          <Image src={authBg} layout='responsive' alt='' />
        </Col>
        <Col span={9}>
          <div style={{ marginBottom: 14 }}>
            <Typography.Title level={3} className='title'>
              Lấy lại mật khẩu
            </Typography.Title>
            <Typography.Paragraph>
              Vui lòng nhập email bạn đã đăng ký tài khoản để lấy lại mật khẩu
            </Typography.Paragraph>
          </div>

          <Form layout='vertical'>
            <Form.Item name='email'>
              <Input placeholder='Email *' />
            </Form.Item>

            <Form.Item>
              <Typography.Paragraph>
                Vui lòng kiểm tra email của bạn để thay đổi mật khẩu
              </Typography.Paragraph>
            </Form.Item>

            <Form.Item style={{ textAlign: 'center' }}>
              <ButtonStyles type='green'>GỬI YÊU CẦU</ButtonStyles>
            </Form.Item>
          </Form>

          <Link href='/'>
            <a className='link'>Bạn chưa nhận được emai?</a>
          </Link>
        </Col>
      </Row>
    </AuthStyles>
  );
}
