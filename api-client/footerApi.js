import axiosInstance from "./api";

export const footerApi = {
  getAll() {
    return axiosInstance.get("/footer   ");
  },
  getSlug(slug) {
    console.log(slug);
    return axiosInstance.get(`/${slug}`);
  },
};
