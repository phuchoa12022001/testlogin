import axiosInstance from "./api";
import axiosInstanceFull from "./axiosInstanceFull";

export const newApi = {
  getAll(page) {
    return axiosInstanceFull.get(`/news?page_size=8&page=${page}`);
  },
  getSlug(slug) {
    return axiosInstance.get(`/news/${slug}`);
  },
};
