import axiosInstanceFull from "./axiosInstanceFull";

export const commentsApi = {
  getAll(params) {
    return axiosInstanceFull.get(
      `/blog-comment?blog_id=${params.id}&page=${params.page}&page_size=4`
    );
  },
  getId(id) {
    return axiosInstanceFull.get("/auth/blog");
  },
  add(data) {
    return axiosInstanceFull.post("/auth/blog-comment", data);
  },
  delete(id) {
    return axiosInstanceFull.delete(`/auth/blog/${id}`);
  },
  edit(data, id) {
    return axiosInstanceFull.patch("/auth/blog", data);
  },
};
