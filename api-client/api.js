import axios from 'axios';
import queryString from 'query-string';
import { getSession } from 'next-auth/react';

// Default config for axios instance
const axiosParams = {
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
  paramsSerializer: (params) =>
    queryString.stringify(params, {
      skipNull: true,
      skipEmptyString: true,
    }),
};
// Create axios instance with default params
const axiosInstance = axios.create(axiosParams);

axiosInstance.interceptors.request.use(
  async function (config) {
    const session = await getSession();

    if (session) {
      config.headers.Authorization = `Bearer ${session.accessToken}`;
    }

    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  function (response) {
    if (response.data?.data) {
      return response.data.data;
    }

    return response.data;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default axiosInstance;
