
import axiosInstanceFull from './axiosInstanceFull';

export const FollowApi = {
  getFollowMe() {
    return axiosInstanceFull.get("/auth/follow/follow-me");
  },
  getIFollow(page) {
    return axiosInstanceFull.get(`/auth/follow/i-follow?page_size=6&page=${page}`);
  },
};
