import axiosInstance from './api';

export const BlogApi = {
    getAll() {
        return axiosInstance.get('/auth/blog');
    },
    add(data) {
        return axiosInstance.post('/auth/blog' , data);
    },
    delete(id) {
        return axiosInstance.delete(`/auth/blog/${id}`);
    },
    edit(data, id) {
        return axiosInstance.patch('/auth/blog', data);
    },
};
