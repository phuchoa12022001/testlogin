import axiosInstance from './api';

export const searchApi = {
  getAll(params) { 
    return axiosInstance.post('/search', params);
  },
};
