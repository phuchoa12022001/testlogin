import axiosInstance from "./api";
import axiosInstanceFull from "./axiosInstanceFull";

const compantApi = {
  getcompanyTypeAll() {
    return axiosInstance.get("/public/company_types");
  },
  getcompanySlug(slug) {
    return axiosInstance.get(`/company/${slug}`);
  },
  // Get Rating Company
  getcompanyRating(id, page) {
    return axiosInstanceFull.get(
      `/rating?company_id=${id}&page_size=3&page=${page}`
    );
  },
  addcompanyRating(data) {
    return axiosInstance.post(`/auth/rating`, data);
  },
  getcompanyMyRating(page) {
    return axiosInstanceFull.get(`/auth/my-rating?page_size=6&page=${page}`);
  },
  // Get Company

  getcompanyFollow(page) {
    return axiosInstanceFull.get(`/auth/company?page_size=5&page=${page}`);
  },
  addcompanyFollow(data) {
    return axiosInstanceFull.post(`/auth/company`, data);
  },
  uploadImagecompanyFollow(form) {
    return axiosInstanceFull.post(`/auth/company/image`, form);
  },

  // get Company wishlist
  getCompanyWishlist(param) {
    const stringLocationId = param.LocationId
      ? `&location_id=${param.LocationId}`
      : "";
    return axiosInstanceFull.get(
      `/auth/wishlist?page_size=6&page=${param.page}${stringLocationId}`
    );
  },
  addCompanyWishlist(data) {
    return axiosInstanceFull.post(`/auth/wishlist`, data);
  },

  // get Company Confirm
  postCompanyConfirm(data) {
    return axiosInstanceFull.post(`/auth/company/confirm`, data);
  },
};

export default compantApi;
