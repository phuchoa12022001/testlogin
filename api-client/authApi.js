import axiosInstance from "./api";

export const authApi = {
  login(data) {
    return axiosInstance.post("/login", data);
  },
  loginGoogle(data) {
    return axiosInstance.post("/google/login", data);
  },
  loginFacebook(data) {
    return axiosInstance.post("/facebook/login", data);
  },
  getProfile() {
    return axiosInstance.get("/auth/profile");
  },
  logout() {
    return axiosInstance.get("/auth/logout");
  },

  requestOTP(data) {
    return axiosInstance.post("/request_otp", data);
  },
  verifyOTP(data) {
    return axiosInstance.post("/verify_otp", data);
  },
  register(data) {
    return axiosInstance.post("/register", data);
  },
  editProfile(data) {
    return axiosInstance.patch("/auth/profile", data);
  },
  updateProfile(form) {
    return axiosInstance.post("/auth/profile/avatar", form);
  },
};
