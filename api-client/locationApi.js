import axiosInstance from "./api";

export const LocationsApi = {
  getLocations() {
    return axiosInstance.get("/auth/locations");
  },
  addLocations(data) {
    return axiosInstance.post("/auth/locations" , data);
  },
  deleteLocations(id) {
    return axiosInstance.delete(`/auth/locations/${id}`);
  },
};
