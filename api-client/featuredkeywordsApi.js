import axiosInstance from './api';

export const featuredkeywordsApi = {
    getAll() {
        return axiosInstance.get('/public/featuredkeywords');
    },
};
