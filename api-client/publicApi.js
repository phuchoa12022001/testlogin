import axiosInstance from './api';

export const publicApi = {
  getAllServices(params) {
    return axiosInstance.get('/service', { params });
  },
  getAllProvinces(params) {
    return axiosInstance.get('/province', { params });
  },
  getAllDistricts(params) {
    return axiosInstance.get('/district', { params });
  },
  getAllWards(params) {
    return axiosInstance.get('/ward', { params });
  },
  getAllStreets(params) {
    return axiosInstance.get('/street', { params });
  },
  getCategories() {
    return axiosInstance.get("categories");
  },
};
