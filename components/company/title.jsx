import React from "react";
import styled from "styled-components";

const TitleContainer = styled.div`
  .title {
    font-weight: 700;
    font-size: 14px;
    line-height: 17px;
    color: ${({ theme }) => theme.colors.primary};
  }
`;
function TitleCompany({ title, className }) {
  return (
    <TitleContainer className={className}>
      <h3 className="title">{title}</h3>
    </TitleContainer>
  );
}

export default TitleCompany;
