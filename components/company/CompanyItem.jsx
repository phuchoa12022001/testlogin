import PropTypes from "prop-types";
import { useState, useContext } from "react";
import Link from "next/link";
import styled from "styled-components";
import { Space, Typography, Rate, Button } from "antd";
import Notification from "../notification/Notification";
import { useSWRConfig } from "swr";
import { useRouter } from "next/router";

import { StarIcon, ConfirmedIcon, CheckIcon } from "../../assets/icons";
import { useAuth } from "../../hooks/useAuth";
import InfoList from "./infoList";
import { useCompanyWishlist } from "../../hooks/Company/useCompanyWishlist";
import Popconfirm from "../form/Popconfirm";
import { ThemeContext } from "../../Context/Context";

const CompanyItemContainer = styled.div`
  padding: 4px 8px;
  background-color: ${({ theme }) => theme.colors.white};
  border: 1px solid ${({ theme }) => theme.colors.grayD3};
  border-radius: 4px;

  h3.ant-typography {
    font-size: 16px;
    font-weight: 400;
    color: ${({ theme }) => theme.colors.yellowD2 + "!important"};
    transition: all 0.3s ease;
    margin-bottom: 3px;

    &:hover {
      color: ${({ theme }) => theme.colors.primary};
    }
  }
  span.ant-typography {
    font-size: 12px;
  }
  .ant-rate {
    color: ${({ theme }) => theme.colors.yellow};
  }
  .ant-space-item {
    display: inline-flex;
  }
  .ant-rate-star:not(:last-child) {
    margin-right: 0;
  }
  .ant-rate-star-second,
  .ant-rate {
    font-size: 10px;
  }
  .desc {
    color: ${({ theme }) => theme.colors.black82};
    margin: 8px 0;
  }

  .info {
    color: ${({ theme }) => theme.colors.grayBD};

    > span {
      color: ${({ theme }) => theme.colors.primary};
    }
  }
  .star {
    font-weight: 400;
    font-size: 12px;
    line-height: 15px;
    padding: 0px 8px;
    color: ${({ theme }) => theme.colors.black82};
    border-right: 1px solid ${({ theme }) => theme.colors.black82};
  }
  .time,
  .confirmed,
  .later {
    font-weight: 500;
    font-size: 10px;
    line-height: 12px;
    padding: 0px 8px 0px 3px;
    color: ${({ theme }) => theme.colors.black82};
    border-right: 1px solid ${({ theme }) => theme.colors.black82};
  }
  .later {
    border-right: unset;
  }
  .Boxlater {
    display: flex;
    cursor: pointer;
    position: relative;
    .Popconfirm {
      position: absolute;
      top: 150%;
      right: 0px;
      width: 250px;
      background: #fff;
      padding: 10px 0px;
      display: flex;
      justify-content: center;
      align-items: center;
      box-shadow: 0 3px 6px -4px #0000001f, 0 6px 16px #00000014,
        0 9px 28px 8px #0000000d;
      .title {
        font-size: 12px;
      }
      .btn {
        margin-left: 10px;
      }
      .action {
        display: flex;
        width: 100%;
        justify-content: center;
        margin-top: 10px;
      }
    }
  }
  .laterIcon {
    margin: 0px 3px;
    .later:hover {
      opacity: 0.5;
    }
  }
  .laterIcon.active {
    color: ${({ theme }) => theme.colors.primary};
  }
  @media only screen and (max-width: 959px) {
    h3.ant-typography {
      margin-bottom: 4px;
    }
  }
`;

export default function CompanyItem({
  name,
  fullAddress,
  website,
  email,
  phones,
  updatedAt,
  user_id,
  mobiles,
  slug,
  star_number = 4,
  id,
  params,
  watch_later,
  enable,
  hideBoxlater = false,
}) {
  // làm tròn lên
  const [open, setOpen] = useState(false);
  const { location } = useContext(ThemeContext);
  const { addCompanyWishlist } = useCompanyWishlist({});
  const router = useRouter();
  const d = new Date(updatedAt);
  let year = d.getFullYear();
  let month = d.getMonth() + 1;
  const { mutate } = useSWRConfig();
  const { profile } = useAuth({});
  const isBoolwL = watch_later === 1;
  const isBoolBoxLater = !!(profile.name || profile.email);
  const addZero = (number) => {
    if (number < 10) {
      return "0" + number;
    }
    return number;
  };
  const handleBoxLater = () => {
    if (isBoolBoxLater) {
      const param = location.LocationId
        ? { location_id: location.LocationId }
        : {};
      addCompanyWishlist({ company_id: id, ...param });
      Notification(
        "success",
        isBoolwL ? "Đã bỏ mục xem sau" : "Đã thêm mục xem sau",
        <CheckIcon className={"CheckIcon"} />
      );
      mutate(["/search", params]);
    } else {
      setOpen(true);
    }
  };
  return (
    <CompanyItemContainer>
      <Link href={`/companies/${slug}`} target="_blank">
        <a>
          <Typography.Title level={3}>{name}</Typography.Title>
        </a>
      </Link>

      <Space size={0} style={{ maxHeight: "15px" }}>
        <Rate
          defaultValue={star_number}
          disabled
          character={<StarIcon style={{ fontSize: 15 }} />}
          allowHalf
        />
        <p className="star">4.3/5</p>
        <p className="time">
          {addZero(month)}/{year}
        </p>
        <p
          className="confirmed"
          style={hideBoxlater ? { borderRight: "none" } : {}}
        >
          {user_id && enable === 1 ? "Đã xác nhận" : "Chưa xác nhận"}
        </p>
        {!hideBoxlater && (
          <div className="Boxlater" onClick={handleBoxLater}>
            <ConfirmedIcon
              className={isBoolwL ? "laterIcon active" : "laterIcon"}
            />
            <p className="later">{isBoolwL ? "Bỏ xem sau" : "Xem sau"}</p>
            {open && (
              <Popconfirm className={"Popconfirm"} setBool={setOpen}>
                <p className="title">
                  Bạn chưa đăng nhập bạn có muốn đăng nhập để thêm xem sau
                </p>
                <div className="action">
                  <Button
                    type="primary"
                    size="small"
                    className="btn"
                    onClick={() => router.push("/login")}
                  >
                    Đồng ý
                  </Button>
                  <Button
                    size="small"
                    className="btn"
                    onClick={() => setOpen(false)}
                  >
                    Hủy bỏ
                  </Button>
                </div>
              </Popconfirm>
            )}
          </div>
        )}
      </Space>

      <InfoList
        fullAddress={fullAddress}
        geo={fullAddress}
        website={website}
        mobiles={mobiles}
        phones={phones}
        email={email}
      />
    </CompanyItemContainer>
  );
}

CompanyItem.defaultProps = {
  phones: [],
};

CompanyItem.propTypes = {
  name: PropTypes.string,
  fullAddress: PropTypes.string,
  description: PropTypes.string,
  website: PropTypes.string,
  email: PropTypes.string,
  phones: PropTypes.array,
  updatedAt: PropTypes.string,
};
