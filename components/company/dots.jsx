import React, { useState } from "react";
import styled from "styled-components";

const DotsContainer = styled.div`
  .dot {
    width: 10px;
    height: 10px;
    background: #828282;
    border-radius: 50%;
    box-shadow: 0px 3px 6px #828282;
    margin-left: 20px;
    cursor: pointer;
  }
  .dot.active {
    background: #007575;
  }
  margin-top: 20px;
  width: 100%;
  display: flex;
  justify-content: center;
`;
function Dots({ number, refs }) {
  const [active, setActive] = useState(1);
  const arr = [];
  for (let i = 0; i < number; i++) {
    arr.push(i);
  }
  const handleGoto = (index) => {
    refs.current.goTo(index, false);
    setActive(index + 1);
  };
  return (
    <DotsContainer>
      {arr.map((dot) => (
        <div
          className={`dot ${dot + 1 === active ? "active" : ""}`}
          key={dot + 1}
          onClick={() => handleGoto(dot)}
        ></div>
      ))}
    </DotsContainer>
  );
}

export default Dots;
