import React from "react";
import styled from "styled-components";

import { MakerIcon, GlobalIcon, PhoneIcon, MailIcon } from "../../assets/icons";
const InfoListContainer = styled.div`
  .list_phones {
    display: flex;
    flex-wrap: wrap;
    flex: 1;
  }
  .infoList {
    .link {
      color: ${({ theme }) => theme.colors.black4D};
      font-size: 12px;
      line-height: 15px;
    }
    li span {
      display: flex;
      align-items: center;
      align-items: start;
    }
    .anticon {
      width: 25px;
      text-align: left;
    }
    .anticon svg {
      width: 100%;
    }
    > li + li {
      margin-top: 5px;
    }
    margin-top: 8px;
  }
  @media only screen and (max-width: 959px) {
    .infoList {
      margin-top: 13px;
      > li + li {
        margin-top: 7px;
      }
    }
  }
`;
function InfoList({
  fullAddress,
  geo,
  website,
  mobiles,
  phones,
  email,
  className,
}) {
  return (
    <InfoListContainer className={className}>
      <ul className="infoList">
        {fullAddress && (
          <li>
            <span>
              <MakerIcon />
              <a
                href={`https://www.google.com/maps/search/${geo}`}
                className="link"
                target="_blank"
                rel="noopener noreferrer"
              >
                {fullAddress}
              </a>
            </span>
          </li>
        )}

        {website && (
          <li>
            <span>
              <GlobalIcon />
              <a
                href={website.replace("www.", "https://")}
                target="_blank"
                className="link"
                rel="noopener noreferrer"
              >
                {website}
              </a>
            </span>
          </li>
        )}
        <>
          {mobiles.length > 0 && (
            <li>
              <span>
                <PhoneIcon />
                <div className="list_phones">
                  {mobiles
                    .map((phone, index) => (
                      <a key={index} className="link" href={`tel:${phone}`}>
                        {phone}
                      </a>
                    ))
                    .reduce((pver, curr, index) => [
                      pver,
                      <span style={{ fontSize: "12px" }} key={index}>
                        ,
                      </span>,
                      curr,
                    ])}
                </div>
              </span>
            </li>
          )}
          {phones.length > 0 && (
            <li>
              <span>
                <PhoneIcon />
                <div className="list_phones">
                  {phones
                    .map((phone, index) => (
                      <a
                        key={Math.random()}
                        className="link"
                        href={`tel:${phone}`}
                      >
                        {phone}
                      </a>
                    ))
                    .reduce((pver, curr, index) => [
                      pver,
                      <span key={Math.random()} style={{ fontSize: "12px" }}>
                        ,
                      </span>,
                      curr,
                    ])}
                </div>
              </span>
            </li>
          )}
        </>

        {email && (
          <li>
            <span>
              <MailIcon />
              <a href={`mailto:${email}`} className="link">
                {email}
              </a>
            </span>
          </li>
        )}
      </ul>
    </InfoListContainer>
  );
}

export default InfoList;
