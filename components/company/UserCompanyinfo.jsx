/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */

import React from "react";
import styled from "styled-components";
import Link from "next/link";

const UserCompanyContainer = styled.div`
  display: flex;
  align-items: center;
  .image {
    height: 20px;
    width: 20px;
    border-radius: 50%;
    margin-right: 5px;
  }
  .text {
    font-weight: 500;
    font-size: 10px;
    line-height: 12px;
    color: #007575;
  }
`;
function UserCompanyinfo({
  color = "#007575",
  img,
  name,
  nameUrl = "/",
  className = "",
  sizeAvatar = 20,
}) {
  return (
    <UserCompanyContainer className={className}>
      <img
        src={img}
        alt="?"
        className="image"
        style={{ height: sizeAvatar, width: sizeAvatar }}
      />
      <Link href={nameUrl}>
        <p className="text">{name}</p>
      </Link>
    </UserCompanyContainer>
  );
}

export default UserCompanyinfo;
