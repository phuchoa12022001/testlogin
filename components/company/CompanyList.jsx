import PropTypes from "prop-types";
import { Space, Empty } from "antd";

import CompanyItem from "./CompanyItem";
import PaginationWrapper from "../common/PaginationWrapper";
import { formatFullAddress } from "../../helpers/common";

export default function CompanyList({ companies, params = {}, ...props }) {
  if (companies.length === 0) {
    return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />;
  }
  return (
    <Space direction="vertical" size={12}>
      {companies.map((company) => {
        const fullAddress = formatFullAddress(
          company.address,
          company.street?.name,
          company.ward?.name,
          company.district?.name,
          company.province?.name
        );

        return (
          <CompanyItem
            key={company.id}
            id={company.id}
            name={company.name}
            fullAddress={fullAddress}
            description={company.description}
            email={company.email}
            website={company.website}
            phones={company.phones}
            updatedAt={company.updated_at}
            isOnlineStatus={!!company.status}
            mobiles={company.mobiles}
            geo={company.geo}
            params={params}
            watch_later={company.watch_later}
            star_number={company.star_number}
            slug={company.slug}
            user_id={company.user_id}
            enable={company.enable}
          />
        );
      })}
      <div className="Pagination">
        <PaginationWrapper {...props} />
      </div>
    </Space>
  );
}

CompanyList.defaultProps = {
  companies: [],
};

CompanyList.propTypes = {
  companies: PropTypes.array,
};
