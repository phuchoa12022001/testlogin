import TimeAgo from "timeago-react";
import * as timeago from "timeago.js";
import React from 'react';
import vi from "timeago.js/lib/lang/vi";

timeago.register("vi", vi);



function ChangeTime(props) {
  return (
    <TimeAgo datetime={props.datetime} locale="vi" />
  )
}

export default ChangeTime;