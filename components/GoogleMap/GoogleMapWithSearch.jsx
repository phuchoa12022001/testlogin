import { useState, useRef, useCallback, memo, useMemo } from "react";
import {
  GoogleMap,
  useJsApiLoader,
  MarkerF,
  Autocomplete,
} from "@react-google-maps/api";
import { Spin, message } from "antd";

const GoogleMapWithSearch = ({
  value,
  onChange,
  height = "400px",
  hideAutocomplete = false,
}) => {
  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAP_KEY,
    libraries: ["places"],
  });
  const [map, setMap] = useState(null);
  const searchBox = useRef(null);
  const marker = useMemo(() => {
    if (isLoaded) {
      if (!value) {
        return null;
      }

      if (value.lat && value.lng) {
        return new window.google.maps.LatLng(value);
      }

      return value;
    }
  }, [value, isLoaded]);
  const [center, setCenter] = useState(() => value);
  const [isFindingLocation, setIsFindingLocation] = useState(false);

  const onLoad = useCallback((map) => {
    if (!value) {
      setIsFindingLocation(true);
      if (!navigator.geolocation) {
        setCenter({ lat: 10.8105831, lng: 106.7091422 });
        setIsFindingLocation(false);
      } else {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            setCenter({
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            });
            setIsFindingLocation(false);
          },
          () => {
            setCenter({ lat: 10.8105831, lng: 106.7091422 });
            setIsFindingLocation(false);
          }
        );
      }
    }

    setMap(map);
    // loại bỏ tầm vì nó sẽ được sửa
    // eslint-disable-next-line
  }, []);

  const onUnmount = useCallback((map) => {
    setMap(null);
  }, []);

  const handleClick = (e) => {
    onChange(e.latLng);
  };

  const handlePlaceChanged = () => {
    const place = searchBox.current?.getPlace();
    if (place?.name && map) {
      if (!place?.geometry || !place?.geometry?.location) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        message.error(
          "Không có thông tin chi tiết về vị trí. Vui lòng nhập lại"
        );
        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }
      onChange(place.geometry.location);
    }
  };
  const handleLoad = () => {};
  const onLoadAutoComplete = (ref) => {
    searchBox.current = ref;
  };

  return (
    <div>
      {isLoaded && (
        <Spin spinning={isFindingLocation}>
          <GoogleMap
            mapContainerStyle={{
              width: "100%",
              height: height,
            }}
            center={center}
            zoom={17}
            onLoad={onLoad}
            onUnmount={onUnmount}
            onClick={handleClick}
          >
            {!hideAutocomplete && (
              <Autocomplete
                onLoad={onLoadAutoComplete}
                onPlaceChanged={handlePlaceChanged}
              >
                <input
                  type="text"
                  placeholder="Nhập vị trí chính xác"
                  style={{
                    boxSizing: `border-box`,
                    border: `1px solid transparent`,
                    width: `240px`,
                    height: `32px`,
                    padding: `0 12px`,
                    borderRadius: `3px`,
                    boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                    fontSize: `14px`,
                    outline: `none`,
                    textOverflow: `ellipses`,
                    position: "absolute",
                    left: "50%",
                    marginLeft: "-120px",
                  }}
                />
              </Autocomplete>
            )}
            {marker && <MarkerF position={marker} onLoad={handleLoad} />}
          </GoogleMap>
        </Spin>
      )}
    </div>
  );
};

export default memo(GoogleMapWithSearch);
