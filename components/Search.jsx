import { useEffect } from "react";
import { useRouter } from "next/router";
import styled from "styled-components";
import { Form, Input } from "antd";

import FormWrapper from "./common/FormWrapper";
import { SearchIcon } from "../assets/icons";

const SearchContainer = styled.div`
  box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.15);

  .ant-input-search > .ant-input-group > .ant-input-group-addon:last-child {
    left: 0;
  }

  .ant-input-search .ant-input:hover,
  .ant-input-search .ant-input:focus {
    border-color: transparent;
  }

  .ant-form-item {
    margin-bottom: 0;
  }

  .anticon {
    font-size: 20px;
    color: ${({ theme, hasColor }) =>
      hasColor ? theme.colors.white : theme.colors.primary};
    cursor: pointer;
  }

  .ant-input-group-addon {
    border-radius: 0 4px 4px 0;

    .ant-btn-primary {
      border-color: transparent;
      background: ${({ theme, hasColor }) =>
        hasColor ? theme.colors.primary : theme.colors.grayEC};
      text-shadow: none;
      box-shadow: none;
      border: none;
      height: 42px;
    }
  }

  .ant-input {
    padding: 9px 11px;
    border-radius: 4px;
  }
  .submit {
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

export default function Search({ hasColor = false }) {
  const router = useRouter();
  const [form] = Form.useForm();
  useEffect(() => {
    form.setFieldsValue({ query: router.query.query });
  }, [router.query, form]);
  const handleSearch = ({ query }) => {
    router.push(
      {
        pathname: "/search",
        query: {
          query,
        },
      },
      undefined,
      { shallow: true }
    );
  }


  return (
    <SearchContainer hasColor={hasColor}>
      <FormWrapper form={form} onFinish={handleSearch}>
        <Form.Item name="query">
          <Input.Search
            placeholder="Tìm kiếm......"
            onSearch={() => {
              form.submit();
            }}
            enterButton={
              <div className="submit">
                <SearchIcon />
              </div>
            }
          />
        </Form.Item>
      </FormWrapper>
    </SearchContainer>
  );
}
