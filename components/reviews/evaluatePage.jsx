import React from "react";
import styled from "styled-components";

import TitleBoxInfo from "../title/TitleBoxInfo";
import Listevaluate from "./listevaluate";

const EvaluatePageStyled = styled.div`
  .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-bottom: 7px;
    border-bottom: 1px solid #ececec;
    margin-bottom: 17px;
  }
`;
function EvaluatePage() {
  return (
    <EvaluatePageStyled>
      <div className="header">
        <TitleBoxInfo text={"Những đánh giá của bạn"} />
      </div>
      <Listevaluate />
    </EvaluatePageStyled>
  );
}

export default EvaluatePage;
