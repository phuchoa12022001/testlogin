import React from "react";
import styled from "styled-components";
import Router from "next/router";
import { useRouter } from "next/router";

import ItemReviews from "./ItemReviews";
import CreateReviews from "./createReviews";
import PaginationWrapper from "../common/PaginationWrapper";

const ListrReviewsContainer = styled.div`
  margin-top: 20px;
`;
function ListReviews({ data, id , page }) {
  const router = useRouter();
  const handleChange = (page) => {
    Router.push(
      `/companies/${router.query.slug}?page=${page}`,
      undefined,
      {
        scroll: false,
      }
    );
  };
  return (
    <ListrReviewsContainer>
      <>
        {[...data.data].reverse().map((rating) => (
          <ItemReviews {...rating} key={rating.id} />
        ))}
      </>
      {data.data.length !== 0 && (
        <PaginationWrapper
          HideText={true}
          current={data.meta.current_page}
          defaultPageSize={+data.meta.per_page}
          total={data.meta.total}
          onChange={handleChange}
        />
      )}
      <CreateReviews id={id} page={page} />
    </ListrReviewsContainer>
  );
}

export default ListReviews;
