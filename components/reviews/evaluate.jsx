import React from "react";
import { Rate, Spin } from "antd";
import styled from "styled-components";

import { StarIcon } from "../../assets/icons/index";
import ListReviews from "./listReviews";
import { useCompanyRating } from "../../hooks/Company/useCompanyRating";
import { useRouter } from "next/router";

const EvaluateStyles = styled.div`
  @media only screen and (max-width: 576px) {
    width: 100% !important;
    .evaluate__box {
      flex-wrap: wrap;
      .evaluate__left , .evaluate__right{
        justify-content: center;
      }
      .evaluate__left  {
        border-right: none !important ;
      }
    }
  }
  .evaluate__box {
    .evaluate__left {
      display: flex;
      justify-content: center;

      li.ant-rate-star {
        margin-right: 0 !important;
      }
    }
    .evaluate__right {
      h3 {
        margin-left: 30px !important;
      }
    }
    
  }
  
`;

function Evaluate({ id }) {
  const router = useRouter();
  const { page } = router.query;
  const { data, loading } = useCompanyRating({ id, page: page || 1 });
  return (
    <>
      {!loading ? (
        <>
          <EvaluateStyles className="evaluate">
            <div className="evaluate__box">
              <div className="evaluate__left">
                <h3>{data.average || "0"}</h3>    
                  <Rate
                    defaultValue={data?.average}
                    disabled
                    character={<StarIcon style={{ fontSize: 25 }} />}
                    allowHalf
                    className="rate-list"
                  />
               
              </div>
              <div className="evaluate__right">
                <h3>
                  {data.meta.total !== 0
                    ? `${data.meta.total} Đánh giá`
                    : "Chưa có đánh giá"}
                </h3>
              </div>
            </div>
          </EvaluateStyles>
          <ListReviews data={data} id={id} page={page || 1} />
        </>
      ) : (
        <Spin />
      )}
    </>
  );
}

export default Evaluate;
