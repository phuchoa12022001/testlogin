import React, { useEffect, createRef, useRef, useState } from "react";
import styled from "styled-components";
import { Rate, Form, Modal } from "antd";
import { useSWRConfig } from "swr";

import TitleCompany from "../company/title";
import { Star2Icon } from "../../assets/icons/index";
import { TextAreaPostTest } from "../Text Editor/TextAreaPost";
import Recaptcha from "../recaptcha/reCaptcha";
import { useCompanyRating } from "../../hooks/Company/useCompanyRating";
import openNotificationWithIcon from "../notification/Notification";
import { CheckIcon } from "../../assets/icons/index";

const ModalReviewsContainer = styled.div`
  .boxModal {
    background: #fff;
    border-radius: 8px;
    margin: auto;
    .title {
      width: 100%;
      margin-top: 20px;
      font-weight: 700;
      text-align: center;
      font-size: 28px;
      line-height: 34px;
      color: ${({ theme }) => theme.colors.secondary};
    }
  }
  .form {
    padding: 0px 83px;
    padding-bottom: 30px;
  }
  .boxRate {
    display: flex;
    justify-content: center;
    width: 100%;
    margin-top: 10px;
  }
  .rate {
    color: #fff;
  }
  .ant-rate-star.ant-rate-star-full {
    .rate {
      color: #fadb14;
      path {
        fill: #fadb14;
      }
    }
  }
  .boxtextArea {
    width: 100%;
    display: flex;
    justify-content: center;
  }
  .textAreaModal {
    border: 1px solid #007575;
    border-radius: 8px;
    width: 470px;
    margin-bottom: 10px;
    textarea {
      padding: 10px;
      width: 100%;
      background: none;
      border: none;
      outline: none;
      font-weight: 400;
      font-size: 12px;
      line-height: 15px;
    }
  }
  .demo-editor {
    min-height: 140px;
    font-weight: 400;
    font-size: 12px;
    line-height: 15px;
  }
  .reCAPTCHA {
    margin-bottom: 10px;
  }
  .btn.btnModal {
    font-weight: 600;
    font-size: 16px;
    line-height: 19px;
    padding: 9px 70px;
    border-radius: 4px;
    border: none;
    color: #fff;
    background: ${({ theme }) => theme.colors.primary};
  }
  .boxBtn {
    width: 100%;
    display: flex;
    justify-content: center;

    .btn {
      cursor: pointer;
    }
  }
  .error {
    color: red;
    margin-bottom: 10px;
  }
  .CheckIcon {
    color: transparent;
    path {
      stroke: #fff;
    }
  }
  @media only screen and (max-width: 576px) {
    .form {
      padding: 0px 10px;
      padding-bottom: 30px;
    }
  }
`;

const ModalStyle = styled(Modal)`
  .ant-modal-content {
    border-radius: 8px;
  }
`;

function ModalReviews({
  isModalVisible,
  onClose,
  valueRate,
  setValueRate,
  comment,
  setComment,
  id,
  page,
}) {
  const [value, setValue] = useState(valueRate);
  const [error, setError] = useState("");
  const { mutate } = useSWRConfig();
  const recaptchaRef = createRef(null);
  const formElemt = useRef();
  const { addcompanyRating } = useCompanyRating({});
  useEffect(() => {
    if (formElemt.current) {
      formElemt.current.setFieldsValue({
        star_number: valueRate,
      });
    }
    setValue(valueRate);
  }, [valueRate, isModalVisible]);
  const desc = ["Quá tệ", "Tệ", "Bình thường", "Tốt", "Rất tốt"];
  const handleChange = (value) => {
    setValueRate(value);
    setValue(value);
  };

  const handleChangeRecaptcha = () => {
    setError("");
  };
  const resetForm = () => {
    formElemt.current.resetFields();
    setComment("");
    setValueRate(1);
    onClose();
  };
  const handleFinish = async (values) => {
    const token = recaptchaRef.current.getValue();
    if (!token) {
      setError("Bạn chưa xác nhận tôi không phải là người máy");
    } else {
      await addcompanyRating({
        ...values,
        company_id: id,
      });
      await mutate(`/rating?company_id=${id}?${page}`);
      openNotificationWithIcon(
        "success",
        "Cảm ơn bạn đã đánh giá thành công",
        <CheckIcon className={"CheckIcon"} />
      );
      resetForm();
    }
  };
  const hanleChangText = (value) => {
    setComment(value);
    formElemt.current.setFieldsValue({
      comment: value,
    });
  };
  const handleCancel = () => {
    onClose(false);
  };
  return (
    <ModalStyle
      visible={isModalVisible}
      onCancel={handleCancel}
      closable={false}
      footer={null}
      width={677}
    >
      <ModalReviewsContainer>
        <div className={"boxModal"}>
          <TitleCompany title={"Vui lòng đánh giá "} className={"title"} />
          <Form
            ref={formElemt}
            name="form2222"
            onFinish={handleFinish}
            className="form"
          >
            <Form.Item
              name="star_number"
              rules={[
                {
                  required: true,
                  message: "Bạn chưa đánh giá sao",
                },
              ]}
            >
              <Rate
                tooltips={desc}
                onChange={handleChange}
                className="boxRate"
                value={value}
                character={
                  <Star2Icon style={{ fontSize: 25 }} className="rate" />
                }
              />
            </Form.Item>
            <Form.Item
              name="review"
              rules={[
                {
                  required: true,
                  message: "Bạn thiếu trường đánh giá",
                },
              ]}
            >
              <div className="boxtextArea">
                <TextAreaPostTest
                  className="textAreaModal"
                  onChange={hanleChangText}
                  value={comment}
                  isModalVisible={isModalVisible}
                />
              </div>
            </Form.Item>
            <Recaptcha ref={recaptchaRef} onChange={handleChangeRecaptcha} />
            <p className="error">{error}</p>
            <div className="boxBtn">
              <button className="btn btnModal">GỬI ĐÁNH GIÁ</button>
            </div>
          </Form>
        </div>
      </ModalReviewsContainer>
    </ModalStyle>
  );
}

export default ModalReviews;
