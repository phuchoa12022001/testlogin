/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */

import React, { useState } from "react";
import styled from "styled-components";
import { Input, Rate } from "antd";

import { Star2Icon } from "../../assets/icons/index";
import ModalReviews from "./modalReviews";
import { useAuth } from "../../hooks/useAuth";
import ImageComponent from "../image/Image";

const { TextArea } = Input;

const CreateReviewsContainer = styled.div`
  margin-top: 20px;
  width: 393px;
  display: flex;
  .image {
    height: 64px;
    width: 64px;
    margin-right: 15px;
    img {
      border-radius: 50%;
    }

    span:first-child {
      width: 100% !important;
      height: 100% !important;
    }
  }
  .reviews {
    flex: 1;
    .title {
      font-weight: 700;
      font-size: 14px;
      line-height: 17px;
      margin-bottom: 11px;
    }
    .desc {
      font-weight: 400;
      font-size: 14px;
      line-height: 15px;
      color: ${({ theme }) => theme.colors.black82};
      margin-bottom: 10px;
    }
    .textArea {
      border: 1px solid #bfbfbf;
      border-radius: 8px;
      width: 314px;
      margin-bottom: 10px;
    }
    .review {
      display: flex;
      align-items: center;
      margin-bottom: 10px;
      ul.ant-rate {
        display: flex;
        align-items: center;
      }       
      p {
        font-weight: 700;
        font-size: 12px;
        line-height: 15px;
        margin-left: 20px;
      }
      .rate {
        color: #fff;
      }
    }
    .ant-rate-star.ant-rate-star-full {
      .rate {
        color: #fadb14;
        path {
          fill: #fadb14;
        }
      }
    }
    .btn {
      font-weight: 600;
      font-size: 16px;
      line-height: 19px;
      background: ${({ theme }) => theme.colors.primary};
      color: #fff;
      padding: 10px 40px;
      border-radius: 4px;
      border: none;
      cursor: pointer;
    }
    .ant-rate-star-second {
    display: flex;
    justify-content: center;
    align-items: center;
}
  }
  @media only screen and (max-width: 576px) {
    width: 100%;
    flex-wrap: wrap;
    .reviews .desc {
      width: 100%;
    }
  }
`;
function CreateReviews({ id, data , page }) {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const { profile } = useAuth({});
  const desc = ["Quá tệ", "Tệ", "Bình thường", "Tốt", "Rất tốt"];
  const isBool = profile.name || profile.email;
  const [value, setValue] = useState(1);
  const [comment, setComment] = useState("");
  const handleChange = (value) => {
    setIsModalVisible(true);
    setValue(value);
  };
  const handleFocus = (e) => {
    setIsModalVisible(true);
    e.target.blur();
  };
  return (
    <CreateReviewsContainer>
      <div className="avatar">
        <ImageComponent src={profile.avatar} className="image" size={50} />
      </div>
      <div className="reviews">
        <h3 className="title">Đánh giá của bạn</h3>
        <p className="desc">Gửi đánh giá của bạn trước khi bắt đầu sử dụng</p>
        <TextArea
          className="textArea"
          placeholder={isBool ? "Viết lời nhận xét" : "Đăng nhập để đánh giá"}
          onFocus={handleFocus}
          disabled={!isBool}
          autoSize={{
            minRows: 3,
            maxRows: 5,
          }}
          value={comment}
        />
        <div className="review">
          <Rate
            tooltips={desc}
            defaultValue={value}
            character={<Star2Icon style={{ fontSize: 25 }} className="rate" />}
            onChange={handleChange}
            value={value}
            disabled={!isBool}
          />
          <p className="vote-rate">{value}/5 Đánh giá của bạn</p>
        </div>
        <ModalReviews
          isModalVisible={isModalVisible}
          onClose={setIsModalVisible}
          valueRate={value}
          setValueRate={setValue}
          comment={comment}
          setComment={setComment}
          id={id}
          data={data}
          page={page}
        />
        <button
          className="btn"
          onClick={() => setIsModalVisible(true)}
          disabled={!isBool}
        >
          Gửi
        </button>
      </div>
    </CreateReviewsContainer>
  );
}

export default CreateReviews;
