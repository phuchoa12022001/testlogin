import React from "react";
import { Spin } from "antd";
import styled from "styled-components";
import Router from "next/router";
import { useRouter } from "next/router";

import { formatFullAddress } from "../../helpers/common";
import CompanyItem from "../company/CompanyItem";
import { useMyCompanyRating } from "../../hooks/Company/useCompantMyRating";
import PaginationWrapper from "../common/PaginationWrapper";

const ListEvaluateStyled = styled.div`
  .item {
    margin-bottom: 10px;
  }
  .boxCenter {
    display: flex;
    width: 100%;
    justify-content: center;
    margin-top: 27px;
  }
`;
function Listevaluate() {
  const router = useRouter();
  const { page } = router.query;
  const { data, loading } = useMyCompanyRating({ page: page || 1 });
  const handleChange = (page) => {
    Router.push(`/evaluate?page=${page}`, undefined, {
      scroll: false,
    });
  };
  return (
    <ListEvaluateStyled>
      {!loading ? (
        data.data.map((company) => {
          const fullAddress = formatFullAddress(
            company.company_info.address,
            company.company_info.street_info?.name,
            company.company_info.ward_info?.name,
            company.company_info.district_info?.name,
            company.company_info.province_info?.name
          );

          return (
            <div className="item" key={company.id}>
              <CompanyItem
                id={company.company_info.id}
                star_number={company.star_number}
                name={company.company_info.name}
                fullAddress={fullAddress}
                description={company.description}
                email={company.company_info.email}
                website={company.company_info.website}
                phones={company.company_info.phones}
                updatedAt={company.updated_at}
                isOnlineStatus={!!company.status}
                mobiles={company.company_info.mobiles || []}
                geo={company.geo}
                slug={company.company_info.slug}
                created_at={company.updated_at}
                user_id={company.user_info.id}
                enable={1}
                hideBoxlater={true}
              />
            </div>
          );
        })
      ) : (
        <Spin />
      )}
      {!loading && (
        <div className="boxCenter">
          <PaginationWrapper
            HideText={true}
            current={data.meta.current_page}
            defaultPageSize={+data.meta.per_page}
            total={data.meta.total}
            onChange={handleChange}
          />
        </div>
      )}
    </ListEvaluateStyled>
  );
}

export default Listevaluate;
