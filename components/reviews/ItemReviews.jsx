/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */

import React from "react";
import { Rate } from "antd";
import styled from "styled-components";

import { StarIcon } from "../../assets/icons/index";
import Image from "../image/Image";
const ItemReviewsContainer = styled.div`
  display: flex;
  .Reviews {
    margin-right: 15px;
    .image {
      width: 64px;
      height: 64px;
      img {
        border-radius: 50%;
      }
    }
    ul li.ant-rate-star {
      margin-right: 0 !important;
    }
    .name {
      font-weight: 700;
      font-size: 14px;
      line-height: 17px;
      margin: 5px 0px;
    }
  }
  .comment {
    flex: 1;
    .list {
      margin-top: 33px;
      p {
        font-weight: 400;
        font-size: 14px;
        line-height: 15px;
        color: ${({ theme }) => theme.colors.black82};
        margin-bottom: 5px;
      }
    }
  }
  margin-bottom: 15px;
`;
function ItemReviews({ user_info, star_number, review, updated_at }) {
  const d = new Date(updated_at);
  let year = d.getFullYear();
  let month = d.getMonth() + 1;
  let day = d.getDate();

  const AddZero = (string) => (+string < 10 ? "0" + string : string);

  return (
    <ItemReviewsContainer>
      <div className="Reviews">
        <Image src={user_info.avatar} className="image" size={64} />
        <h3 className="name">{user_info.name || user_info.email}</h3>
        <Rate
          defaultValue={star_number}
          disabled
          character={<StarIcon style={{ fontSize: 20 }} />}
          allowHalf
          className="rate-list"
        />
      </div>
      <div className="comment">
        <div className="list">
          <p>{`${AddZero(day)} - ${AddZero(month)} - ${AddZero(year)}`}</p>
          <p>{review}</p>
        </div>
      </div>
    </ItemReviewsContainer>
  );
}

export default ItemReviews;
