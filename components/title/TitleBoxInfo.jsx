import React from "react";
import styled from "styled-components";

const TitleBoxInfoStyle = styled.h3`
  font-weight: 700;
  font-size: 20px;
  line-height: 24px;
  color: ${({ theme }) => theme.colors.primary};
`;
function TitleBoxInfo({  text}) {
  return <TitleBoxInfoStyle >{text}</TitleBoxInfoStyle>;
}

export default TitleBoxInfo;
