/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
import React from "react";
import styled from "styled-components";

const TitleFolderContainer = styled.div`
  .title_info {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .title__text {
    font-weight: 700;
    font-size: 16px;
    line-height: 19px;
    color: ${({ theme }) => theme.colors.primary};
  }
`;
function TitleFolder({ text, icon }) {
  return (
    <TitleFolderContainer>
      <div className="title_info">
        <h3 className="title__text">{text}</h3>
        {icon && <img src="/static/icon/i.svg" />}
      </div>
    </TitleFolderContainer>
  );
}

export default TitleFolder;
