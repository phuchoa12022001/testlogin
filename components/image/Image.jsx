import React from "react";
import Image from "next/image";
import { UserOutlined } from "@ant-design/icons";

//  Image : nếu !!src  === false thì nó sẽ lấy icon;
function ImageComponent({ src, className, alt, size }) {
  return (
    <div className={className}>
      {src ? (
        <Image src={src} alt={alt || "image"} width={size} height={size} />
      ) : (
        <UserOutlined className={className} style={{ fontSize: size }} />
      )}
    </div>
  );
}

export default ImageComponent;
