import { useState, useMemo } from "react";
import { useRouter } from "next/router";
import styled from "styled-components";
import { ArrowLeftOutlined, SearchOutlined } from "@ant-design/icons";
import { Modal, Button, Space, Input, Radio, Typography } from "antd";

import {
  useProvinces,
  useDistricts,
  useWards,
  useStreets,
} from "../../hooks/usePublic";
import { FilterLocationItem } from "./FilterLocationItem";
import { serializeSearchParams } from "../../helpers/common";
import { FilterIcon } from "../../assets/icons";
import { removeVietnameseFromString } from "../../helpers/stringUtils";

const ModalStyles = styled(Modal)`
  .titleWrapper {
    display: flex;
    align-items: center;

    .titleInfo {
      flex-grow: 1;
      text-align: center;
    }
  }

  .filterInput {
    border: none;

    .ant-input-prefix {
      .anticon {
        font-size: 16px;
      }
    }

    .ant-input {
      &::-webkit-input-placeholder {
        color: #057642;
      }
      &::-moz-placeholder {
        color: #057642;
      }
      &:-ms-input-placeholder {
        color: #057642;
      }
      &:-moz-placeholder {
        color: #057642;
      }
    }
  }

  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: transparent;
  }

  .ant-input-affix-wrapper-focused {
    box-shadow: none;
  }
`;

export const FilterLocationModal = ({ data, setData }) => {
  const router = useRouter();
  const [isVisible, setIsVisible] = useState(false);
  const [current, setCurrent] = useState(0);

  const [option, setOption] = useState("WARD");
  const [query, setQuery] = useState("");

  const { data: provinces = [] } = useProvinces();
  const { data: districts = [] } = useDistricts({
    province_id: data.province?.id,
  });
  const { data: wards = [] } = useWards({
    province_id: data.province?.id,
    district_id: data.district?.id,
  });
  const { data: streets = [] } = useStreets({
    province_id: data.province?.id,
    district_id: data.district?.id,
  });

  const filteredData = useMemo(() => {
    if (current === 0) {
      return [{ id: "", name: "Toàn quốc" }, ...provinces];
    } else if (current === 1) {
      return [{ id: "", name: "Tất cả" }, ...districts];
    } else if (current === 2) {
      if (option === "WARD") {
        return [{ id: "", name: "Tất cả" }, ...wards];
      } else if (option === "STREET") {
        return [{ id: "", name: "Tất cả" }, ...streets];
      }
    }
    return [];
  }, [current, provinces, districts, wards, streets, option]);

  const filteredDataWithQuery = filteredData.filter((item) =>
    removeVietnameseFromString(item.name)
      .toUpperCase()
      .includes(removeVietnameseFromString(query).toUpperCase())
  );

  const renderedTitle = useMemo(() => {
    return (
      <div className="titleWrapper">
        {current !== 0 && (
          <ArrowLeftOutlined onClick={() => setCurrent((prev) => prev - 1)} />
        )}
        <p className="titleInfo">
          {current === 0
            ? "Chọn Tỉnh/Thành phố"
            : current === 1
            ? "Chọn Quận/Huyện"
            : current === 2
            ? "Chọn Phường/Xã/Đường"
            : ""}
        </p>
      </div>
    );
  }, [current]);
  const getName = (array) => {
    //  Nếu arrChangeName có trong mảng này thì nó sẽ đổi tên lại
    const arrChangeName = ["Phường", "Xã", "Thị trấn", "Đường"];
    let name = "";

    if (!array?.name) {
      name = "Toàn quốc";
    } else {
      arrChangeName.findIndex((item) => item === array.prefix) !== -1
        ? (name = `${array.prefix} ${array.name}`) 
        : (name = array.name)
    }
    return name;
  };
  const renderedLabel = useMemo(() => {
    const array = Object.values(data).filter((item) => item);
    return getName(array[array.length - 1]);
  }, [data]);

  const handleCancel = () => setIsVisible(false);

  const handleClick = (item) => {
    // Chuyển chữ tìm kiếm về bằng rỗng .
    setQuery("");
    if (current === 0) {
      setData((prev) => ({
        ...prev,
        province: item,
        district: null,
        ward: null,
        street: null,
      }));

      router.push(
        {
          pathname: router.pathname,
          query: {
            ...serializeSearchParams({
              ...router.query,
              province: item.id,
              district: null,
              ward: null,
              street: null,
            }),
            page: 1,
          },
        },
        undefined,
        { shallow: true }
      );
    } else if (current === 1) {
      setData((prev) => ({
        ...prev,
        district: item,
        ward: null,
        street: null,
      }));

      router.push(
        {
          pathname: router.pathname,
          query: {
            ...serializeSearchParams({
              ...router.query,
              district: item.id,
              ward: null,
              street: null,
            }),
            page: 1,
          },
        },
        undefined,
        { shallow: true }
      );
    } else if (current === 2) {
      setData((prev) => ({
        ...prev,
        ward: option === "WARD" ? item : null,
        street: option === "STREET" ? item : null,
      }));

      router.push(
        {
          pathname: router.pathname,
          query: {
            ...serializeSearchParams({
              ...router.query,
              ward: option === "WARD" ? item.id : null,
              street: option === "STREET" ? item.id : null,
            }),
            page: 1,
          },
        },
        undefined,
        { shallow: true }
      );
    }

    if (item.id === "") {
      handleCancel();
      return;
    }

    if (current < 2) {
      setCurrent((prev) => prev + 1);
    } else {
      handleCancel();
    }
  };

  return (
    <>
      <div
        onClick={() => setIsVisible(true)}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          border: "1px solid #ECECEC",
          borderRadius: 4,
          height: 34,
          minWidth: 180,
          cursor: "pointer",
        }}
      >
        <FilterIcon style={{ fontSize: 16 }} />
        <span style={{ marginLeft: 4 }}>{renderedLabel}</span>
      </div>

      <ModalStyles
        visible={isVisible}
        footer={null}
        bodyStyle={{
          maxHeight: 500,
          height: "100%",
          overflowY: "auto",
          padding: 0,
        }}
        title={renderedTitle}
        onCancel={handleCancel}
      >
        <Input
          placeholder="Tìm kiếm nhanh"
          prefix={<SearchOutlined />}
          className="filterInput"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />

        {current === 2 && (
          <Radio.Group
            value={option}
            buttonStyle="solid"
            onChange={(e) => setOption(e.target.value)}
          >
            <Radio.Button value="WARD">Phường/Xã</Radio.Button>
            <Radio.Button value="STREET">Đường</Radio.Button>
          </Radio.Group>
        )}

        {filteredDataWithQuery.map((item) => {
          return (
            <FilterLocationItem key={item.id} onClick={() => handleClick(item)}>
              {item.prefix ? `${item.prefix} ${item.name}` : item.name}
            </FilterLocationItem>
          );
        })}
      </ModalStyles>
    </>
  );
};
