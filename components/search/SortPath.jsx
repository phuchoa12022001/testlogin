import React, { useState, useContext } from "react";
import styled from "styled-components";

import SortModal from "./SortModal";
import { ThemeContext } from "../../Context/Context";
const SortPathStyles = styled.div`
  .path {
    display: flex;
    align-items: center;
    justify-content: center;
    border: 1px solid #ececec;
    border-radius: 4px;
    height: 34px;
    min-width: 100%;
    cursor: pointer;
  };
`;
function SortPath() {
  const [isVisible, setIsVisible] = useState(false);
  const { location } = useContext(ThemeContext);
  const handleOpen = () => {
    setIsVisible(true);
  };
  return (
    <SortPathStyles>
      <div className="path" onClick={handleOpen}>
        {!!(location.lat && location.long)
          ? "Sắp xếp theo[Gần nhất]"
          : "Sắp xếp theo[Đúng nhất]"}
      </div>
      <SortModal isVisible={isVisible} setIsVisible={setIsVisible} />
    </SortPathStyles>
  );
}

export default SortPath;
