import React, { useState, useContext, useEffect } from "react";
import { Modal } from "antd";
import { useRouter } from "next/router";
import styled from "styled-components";

import SliderTheme from "../customizeTheme/slider";
import { ThemeContext } from "../../Context/Context";
import CheckBoxTheme from "../customizeTheme/checkBox";

const marks = {
  0: "0",
  1: "10Km",
  2: "20Km",
  3: "30Km",
  4: "40Km",
  5: "50Km",
};

const ModalStyles = styled(Modal)`
  .titleInfo {
    flex-grow: 1;
    text-align: center;
  }
  .item {
    padding: 10px 30px;
  }
`;
function SortModal({ isVisible, setIsVisible }) {
  const [filterState, setFilterState] = useState({
    nearest: false,
    km: 0,
  });
  //  false là đúng nhất .
  //  true là gần nhất
  // nearest
  const { ReloadHistory, setLocation, location } = useContext(ThemeContext);
  const router = useRouter();
  const handleChangeCheckbox = (e) => {
    if (e.target.name === "nearest") {
      setFilterState((prev) => ({ ...prev, nearest: true }))
    } else {
      setFilterState((prev) => ({ ...prev, nearest: false }));
    }
  };
  const handleChangeSlider = (number) => {
    setFilterState((prev) => ({ ...prev, km: number }));
  };
  const handleOk = () => {
    if (filterState.nearest) {
      router.push(
        {
          pathname: "/search",
          query: {
            ...router.query,
            distance: (filterState.km * 10000).toString(),
          },
        },
        undefined,
        { shallow: true }
      );
      ReloadHistory();
    } else {
      delete router.query.distance;
      router.push(
        {
          pathname: router.pathname,
          query: {
            ...router.query,
            page: 1,
          },
        },
        undefined,
        { shallow: true }
      );
      setLocation((prev) => ({ ...prev, lat: null, long: null }));
    }
    setIsVisible(false);
  };
  useEffect(() => {
    setFilterState((prev) => {
      return {
        ...prev,
        nearest: !!(location.lat && location.long),
      };
    });
  }, [location]);
  return (
    <ModalStyles
      visible={isVisible}
      onOk={handleOk}
      onCancel={() => setIsVisible(false)}
      bodyStyle={{
        maxHeight: 500,
        height: "100%",
        overflowY: "auto",
        padding: 0,
      }}
      okText="Áp Dụng"
      title={<p className="titleInfo">Sắp xếp theo</p>}
    >
      <div className="item">
        <CheckBoxTheme
          onChange={handleChangeCheckbox}
          name="MostCorrect"
          checked={!filterState.nearest}
        >
          Đúng nhất
        </CheckBoxTheme>
      </div>
      <div className="item">
        <CheckBoxTheme
          onChange={handleChangeCheckbox}
          name="nearest"
          checked={filterState.nearest}
        >
          Gần nhất
        </CheckBoxTheme>
      </div>
      <div className="item">
        <p>Khoảng cách :</p>
        <SliderTheme
          disabled={!filterState.nearest}
          marks={marks}
          value={filterState.km}
          max={5}
          onChange={handleChangeSlider}
        />
      </div>
    </ModalStyles>
  );
}

export default SortModal;
