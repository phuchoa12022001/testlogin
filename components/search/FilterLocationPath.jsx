import { Fragment } from 'react';
import { Typography } from 'antd';
import { FilterIcon } from '../../assets/icons';

export const FilterLocationPath = ({ data = [] }) => {
  const filteredData = data.filter((item) => item);

  return (
    <div style={{ marginBottom: 16 }}>
      <Typography.Paragraph>
        <Typography.Text strong>
          <FilterIcon /> Khu vực:{' '}
        </Typography.Text>

        <Typography.Text>
          {filteredData.length > 0
            ? filteredData.map((item, index) => {
                return (
                  <Fragment key={item.id}>
                    <span>
                      {item.prefix ? `${item.prefix} ${item.name}` : item.name}
                    </span>
                    {index !== filteredData.length - 1 && ' > '}
                  </Fragment>
                );
              })
            : 'Toàn quốc'}
        </Typography.Text>
      </Typography.Paragraph>
    </div>
  );
};
