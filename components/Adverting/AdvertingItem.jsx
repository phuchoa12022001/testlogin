import React from "react";
import styled from "styled-components";

import { GlobalIcon, PhoneIcon, MailIcon } from "../../assets/icons";

const AdvertingItemContainer = styled.div`
  padding: 8px 3px 8px 16px;
  margin-bottom : 12px;
  border: 1px solid ${({ theme }) => theme.colors.grayD3};
  border-radius: 4px;
  .title,
  .text {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 16px;
    line-height: 19.36px;
    margin-bottom: 2px;
  }
  .text {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 14px;
    font-weight: 400;
    line-height: 15px;
  }
  .trademark {
    color: ${({ theme }) => theme.colors.black82};
    font-size: 12px;
    line-height: 20px;
    margin-bottom: 2px;
  }
  .info {
    display: flex;
    justify-content: space-between;
  }
  .list {
    > li + li {
      margin-top: 5px;
    }
  }
  .list li {
    list-style-type: none;
    display: flex;
    align-items: center;
  }
  .list__info {
    margin-left: 5px;
    display: block;
    font-size: 12px;
    line-height: 15px;
  }
  .desc {
    margin-top: 5px;
    font-size: 12px;
    line-height: 15px;
    color: ${({ theme }) => theme.colors.black82};
  }
  @media only screen and (max-width: 959px) {
      max-width : 100%;
  }
`;
function AdvertingItem(props) {
  return (
    <AdvertingItemContainer>
      <div className="info">
        <h3 className="title">Vinhomes</h3>
        <p className="text">Tori</p>
      </div>
      <p className="trademark">Commericial company</p>
      <div className="list">
        <li>
          <PhoneIcon />
          <a className="list__info">(603) 555-0123</a>
        </li>
        <li>
          <MailIcon />
          <a className="list__info">huy123@gmail.com</a>
        </li>
        <li>
          <GlobalIcon />
          <a className="list__info">www.brandviet.com</a>
        </li>
        <p className="desc">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor enim
          arcu hendrerit tristique mauris. Lacus pulvinar convallis eget id
          risus auctor arcu enim. Quis montes, vitae turpis etiam porta a. Leo
          erat purus feugiat donec.
        </p>
      </div>
    </AdvertingItemContainer>
  );
}

export default AdvertingItem;
