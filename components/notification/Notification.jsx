
import { notification } from "antd";
import { DeleteIcon } from "../../assets/icons";
const openNotificationWithIcon = (type , text , icon) => {
  notification[type]({
    message: text,
    description:null,
    icon: icon,
    className: 'custom-notification',
    top: 100,
    closeIcon: <DeleteIcon />
  });
};

export default openNotificationWithIcon;
