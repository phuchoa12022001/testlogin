import React, { useContext } from "react";
import { Modal } from "antd";
import { useSWRConfig } from "swr";
import { useMediaQuery } from "react-responsive";
import styled from "styled-components";

import { ThemeContext } from "../../Context/Context";
import InputForm from "../form/input";
import { Form, Button } from "antd";
import { useLocations } from "../../hooks/useLocations";
import Protected from "../auth/Protected";

const ModalStyles = styled(Modal)`
  .ant-form-item {
    margin-bottom: 5px;
  }
  .box {
    display: flex;
    width: 100%;
    justify-content: flex-end;
  }
`;

function ModalName({ visible, setVisible, handleCancel }) {
  const { location } = useContext(ThemeContext);
  const { mutate } = useSWRConfig();
  const [form] = Form.useForm();
  const { addLocations } = useLocations({});

  const isMobile = useMediaQuery({ query: "(max-width: 576px)" });
  const bodyStyle = isMobile
    ? {
        maxWidth: 500,
        height: "100%",
        overflowY: "auto",
        background: "#fff",
        padding: 10,
        paddingTop: 20,
      }
    : {
        maxWidth: 500,
        height: "100%",
        overflowY: "auto",
        background: "#fff",
        padding: 10,
      };
  const onFinish = (values) => {
    const NewValues = {
      name: values.name,
      address: location.address,
      latitude: location.lat,
      longitude: location.long,
    };
    setVisible(false);
    addLocations(NewValues);
    mutate("/auth/locations");
    form.resetFields();
  };
  const handleClick = (e) => {
    e.stopPropagation();
  };
  return (
    <Protected>
      <div onClick={handleClick}>
        <ModalStyles
          visible={visible}
          title={null}
          footer={null}
          width={500}
          onCancel={(event) => handleCancel(event)}
          bodyStyle={bodyStyle}
        >
          <Form
            name="form"
            form={form}
            onFinish={onFinish}
          >
            <InputForm
              name={"name"}
              label={"Tên"}
              placeholder="Tên vị trí bạn cần lưu"
              focus={true}
            />
            <div className="box">
              <Button type="primary" className="add" htmlType="submit">
                Thêm
              </Button>
            </div>
          </Form>
        </ModalStyles>
      </div>
    </Protected>
  );
}

export default ModalName;
