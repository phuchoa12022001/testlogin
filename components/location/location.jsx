import React, { useContext, useState } from "react";
import styled from "styled-components";

import { LocationIcon, SaveIcon } from "../../assets/icons/index";
import { ThemeContext } from "../../Context/Context";
import ModalName from "./ModalName";

const YourPositionStyles = styled.div`
  padding-top: 105px;
  display: flex;
  max-width: 515px;
  margin-bottom: 21px;
  cursor: pointer;
  .icon {
    margin-right: 20px;
  }
  .info {
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex: 1;
    .content {
      margin-right: 20px;
      p {
        font-weight: 400;
        font-size: 12px;
        line-height: 15px;
        margin-bottom: 10px;
        color: ${({ theme }) => theme.colors.black82};
      }
      h3 {
        font-weight: 600;
        font-size: 12px;
        line-height: 15px;
      }
    }
  }
`;
function Location({ onClick, className  , ...props}) {
  const { location } = useContext(ThemeContext);
  const [visible, setVisible] = useState(false);
  const handleSave = (event) => {
    setVisible(true);
    event.stopPropagation();
  };
  const handleCancel = (event) => {
    event.stopPropagation();
    setVisible(false);
  }
  return (
    <>
      <YourPositionStyles onClick={onClick} className={className} {...props}>
        <div className="icon">
          <LocationIcon />
        </div>
        <div className="info">
          <div className="content">
            <p>Ví trí của bạn</p>
            <h3>{location.address}</h3>
          </div>
          <div className="action" onClick={handleSave}>
            <SaveIcon />
          </div>
        </div>
        {visible && <ModalName visible={visible} setVisible={setVisible} handleCancel={handleCancel} />}
      </YourPositionStyles>
    </>
  );
}

export default Location;
