import React from "react";
import styled from "styled-components";

import ItemLocation from "./itemLocation";
import { useLocations } from "../../hooks/useLocations";
import Protected from "../auth/Protected";

const ListStyles = styled.div`
  margin-top: 42px;
  margin-bottom: 36px;
  max-height: 480px;
  overflow: auto;
`;
function ListLocation() {
  const { loading, data } = useLocations({});
  return (
    <Protected>
      <ListStyles>
        {(!loading && data) &&
          data.map((item) => (
            <ItemLocation
              key={item.id}
              address={item.address}
              name={item.name}
              id={item.id}
              latitude={item.latitude}
              longitude={item.longitude}
            />
          ))}
      </ListStyles>
    </Protected>
  );
}

export default ListLocation;
