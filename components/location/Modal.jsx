import React, { useContext } from "react";
import { Modal, Tabs, Form } from "antd";
import { useMediaQuery } from "react-responsive";
import styled from "styled-components";

import Location from "./location";
import GoogleMapWithSearch from "../GoogleMap/GoogleMapWithSearch";
import ListLocation from "./listLocation";
import { ThemeContext } from "../../Context/Context";
import Geocode from "../../helpers/configGeocode";

const { TabPane } = Tabs;

const ModalStyles = styled(Modal)`
  .Location {
    padding: 0px;
    margin-bottom: 10px;
  }
  .title {
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    margin-bottom: 20px;
  }
  .tab {
    font-weight: 600;
    font-size: 12px;
    line-height: 15px;
  }
  .ant-tabs-top > .ant-tabs-nav::before {
    display: none;
  }
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn,
  .ant-tabs-tab:hover {
    color: unset;
    text-shadow: unset;
    .tab {
      padding-bottom: 5px;
      border-bottom: 1px solid;
    }
  }
  .ant-tabs-ink-bar.ant-tabs-ink-bar-animated {
    width: 0px !important;
  }
`;

function ModalLocation({ visible, setVisible }) {
  const { location, setLocation, CopyLocation, ReloadHistory } =
    useContext(ThemeContext);
  const isMobile = useMediaQuery({ query: "(max-width: 576px)" });
  const value =
    location.lat && location.long
      ? {
          lat: location.lat,
          lng: location.long,
        }
      : null;
  const bodyStyle = isMobile
    ? {
        maxWidth: 918,
        height: "100%",
        overflowY: "auto",
        background: "#fff",
        padding: 10,
        paddingTop: 40,
      }
    : {
        maxWidth: 918,
        height: "100%",
        overflowY: "auto",
        background: "#fff",
        padding: 10,
      };
  const handleChange = (value) => {
    const locationNew = {
      lat: +value.lat(),
      lng: +value.lng(),
    };
    Geocode.fromLatLng(locationNew.lat, locationNew.lng).then(
      (response) => {
        const address = response.results[0].formatted_address;
        setLocation({ address, lat: locationNew.lat, long: locationNew.lng });
      },
      (error) => {
        console.error(error);
      }
    );
  };
  const handleCancel = () => {
    ReloadHistory();
    setVisible(false);
  };
  const handleOk = () => {
    CopyLocation();
    setVisible(false);
  };
  return (
    <ModalStyles
      visible={visible}
      title={null}
      width={918}
      onCancel={handleCancel}
      onOk={handleOk}
      className={"ModalStyles"}
      bodyStyle={bodyStyle}
    >
      <Location className={"Location"} />
      <h3 className="title">XÁC ĐỊNH VỊ TRÍ</h3>
      <div className="tabs">
        <Tabs defaultActiveKey="1">
          <TabPane tab={<span className="tab">Google Map</span>} key="1">
            {/* Googlemap nên bỏ trong Form */}
            <Form>
              <GoogleMapWithSearch value={value} onChange={handleChange} />
            </Form>
          </TabPane>
          <TabPane tab={<span className="tab">Vị trí đã lưu</span>} key="2">
            <ListLocation />
          </TabPane>
        </Tabs>
      </div>
    </ModalStyles>
  );
}

export default ModalLocation;
