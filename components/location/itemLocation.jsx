import React, { useContext } from "react";
import styled from "styled-components";
import { useSWRConfig } from "swr";
import { DeleteOutlined } from "@ant-design/icons";

import { LocationIcon } from "../../assets/icons/index";
import { useLocations } from "../../hooks/useLocations";
import { ThemeContext } from "../../Context/Context";

const ItemLocationStyle = styled.div`
  display: flex;
  max-width: 515px;
  cursor: pointer;
  margin-bottom: 28px;
  .icon {
    margin-right: 20px;
  }
  .info {
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex: 1;
    .content {
      margin-right: 20px;
      p {
        font-weight: 600;
        font-size: 12px;
        line-height: 15px;
        margin-bottom: 5px;
      }
      h3 {
        font-weight: 700;
        font-size: 20px;
        line-height: 24px;
        color: ${({ theme }) => theme.colors.primary};
      }
    }
  }
  .actionIcon {
    color: ${({ theme }) => theme.colors.primary};
    font-size: 21px;
    :hover {
      color: black;
    }
  }
`;
function ItemLocation({ address, name, id, latitude, longitude }) {
  const { deleteLocations } = useLocations({});
  const { setLocation } = useContext(ThemeContext);
  const { mutate } = useSWRConfig();
  const handleDetele = (event) => {
    event.stopPropagation();
    deleteLocations(id);
    mutate("/auth/locations");
  };
  const handleClick = () => {
    setLocation({
      address,
      lat: latitude,
      long: longitude,
      LocationId: id,
    });
  };
  return (
    <ItemLocationStyle onClick={handleClick}>
      <div className="icon">
        <LocationIcon />
      </div>
      <div className="info">
        <div className="content">
          <h3>{name}</h3>
          <p>{address}</p>
        </div>
        <div className="action" onClick={handleDetele}>
          <DeleteOutlined className={"actionIcon"} />
        </div>
      </div>
    </ItemLocationStyle>
  );
}

export default ItemLocation;
