import { Drawer } from 'antd';
import styled from 'styled-components';

const MenuDrawerElement = styled(Drawer)``;

export const MenuDrawer = (props) => {
  return <MenuDrawerElement {...props} />;
};
