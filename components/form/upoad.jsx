import { UploadOutlined } from "@ant-design/icons";
import { Button, Upload, Form } from "antd";
import React, { useState } from "react";

const getBase64 = (img, callback) => {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
};

const UploadForm = ({ value, onChangeUpload, className, label, name }) => {
  const [fileList, setFileList] = useState(value ? value : []);
  const props = {
    multiple: true,
    onChange(info) {
      setFileList(info.fileList)
    },
    beforeUpload: (file) => {
      onChangeUpload(file);
      getBase64(info.file.originFileObj, (url) => {
        setFileList((prev) => [...prev, url]);
      });
    },
  };
  return (
    <Form.Item label={label} className={className}>
      <Upload {...props} fileList={fileList} className={className}>
        <Button icon={<UploadOutlined />}>{label}</Button>
      </Upload>
    </Form.Item>
  );
};

export default UploadForm;
