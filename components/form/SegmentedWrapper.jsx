import styled from 'styled-components';
import { Segmented } from 'antd';

const SegmentedWrapperContainer = styled.div`
  border-bottom: 1px solid ${({ theme }) => theme.colors.grayC4};
  padding-bottom: 6px;

  .ant-segmented {
    color: ${({ theme }) => theme.colors.gray66};
    position: relative;
    padding: 0;
  }

  .ant-segmented-group {
    > .ant-segmented-item + .ant-segmented-item {
      margin-left: 14px;
    }
  }

  .ant-segmented-item {
    &::after {
      content: '';
      position: absolute;
      bottom: -9px;
      left: 50%;
      right: 50%;
      height: 3px;
      background-color: ${({ theme }) => theme.colors.primary};
      transition: all 0.3s ease;
    }

    &.ant-segmented-item-selected {
      &::after {
        left: 0;
        right: 0;
      }
    }
  }

  .ant-segmented-item-selected {
    background-color: transparent;
    border-radius: none;
    box-shadow: none;
    color: ${({ theme }) => theme.colors.primary};
  }

  .ant-segmented-item-label {
    padding: 0;
  }

  .ant-segmented,
  .ant-segmented:not(.ant-segmented-disabled):hover,
  .ant-segmented:not(.ant-segmented-disabled):focus {
    background-color: transparent;
  }

  .ant-segmented-item:hover,
  .ant-segmented-item:focus {
    color: ${({ theme }) => theme.colors.primary};
  }

  .ant-segmented-thumb {
    box-shadow: none;
  }
`;

export default function SegmentedWrapper(props) {
  return (
    <SegmentedWrapperContainer>
      <Segmented {...props} />
    </SegmentedWrapperContainer>
  );
}
