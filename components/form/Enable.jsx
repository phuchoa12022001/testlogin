import React from "react";
import { Form, Switch } from "antd";

function EnableForm({ enable, onChange, name, label , rules = true }) {

  const rulesProps = rules ? [
    {
      required: true,
      message : `không thể bỏ trường này`
    },
  ] : []

  const handleChange = (value) => {
    onChange(value ? 1 : 0);
  };
  return (
    <Form.Item
      name={name}
      label={label}
      rules={rulesProps}
    >
      <Switch onChange={handleChange} checked={enable === 1} />
    </Form.Item>
  );
}

export default EnableForm;
