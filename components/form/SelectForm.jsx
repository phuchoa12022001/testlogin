import { Select, Space, Form } from "antd";
import React from "react";
import { isIncludeTextSearch } from "../../helpers/common";

export const SelectMultipleForm = ({
  value,
  data,
  onChange,
  label,
  layout,
  className,
  rulesProps = true,
}) => {
  const selectProps = {
    mode: "multiple",
    style: {
      width: "100%",
    },
    value,
    options: data,
    onChange: (values) => onChange(values),
    placeholder: "Chọn loại công ty",
    maxTagCount: "responsive",
  };
  return (
    <Form.Item
      label={label}
      {...layout}
      className={className}
      rules={rulesProps}
    >
      <Space
        direction="vertical"
        style={{
          width: "100%",
        }}
      >
        <Select
          {...selectProps}
          filterOption={(input, option) =>
            isIncludeTextSearch(input, option.name)
          }
        />
      </Space>
    </Form.Item>
  );
};
