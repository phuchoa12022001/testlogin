import React, { useRef } from "react";

import styled from "styled-components";

const TextAreaStyle = styled.div`
  padding-left: 13px;
  flex: 1;
  .textarea {
    transition: height 1s;
    overflow: hidden;
    resize: none;
    background: transparent;
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
    padding: 5px 0px;
    width: 100%;
    border: none;
    outline: none;
  }
`;
const TextareaAutoResize = ({ Value, height, onChange, ...props }) => {
  const textareaRef = useRef(null);

  const handleChange = (e) => {
    if (textareaRef.current) {
      textareaRef.current.style.height = "auto";
      textareaRef.current.style.height = `${textareaRef?.current?.scrollHeight}px`;
      onChange({
        text: e.target.value,
      });
    }
  };
  return (
    <TextAreaStyle style={{ height: height }}>
      <textarea
        placeholder={props.placeholder}
        value={Value}
        ref={textareaRef}
        onChange={handleChange}
      ></textarea>
    </TextAreaStyle>
  );
};

export default TextareaAutoResize;
