import React from "react";
import { Input, Form } from "antd";

const { TextArea } = Input;

function TextAreaForm({
  name,
  label,
  placeholder,
  className = "TextArea",
  rows = 4,
  layout = {}
}) {
  return (
    <Form.Item
      name={name}
      label={label}
      className={className}
      {...layout}
      rules={[
        {
          required: true,
          message: `không thể bỏ trường này`,
        },
      ]}
    >
      <TextArea placeholder={placeholder} rows={rows} />
    </Form.Item>
  );
}

export default TextAreaForm;
