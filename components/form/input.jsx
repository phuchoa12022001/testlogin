import React, { useEffect, useRef } from "react";
import { Input, Form } from "antd";

function InputForm({
  name,
  label,
  placeholder,
  className = "input",
  rules = true,
  focus = false,
  layout = {},
}) {
  const inputRef = useRef();
  const rulesProps = rules
    ? [
        {
          required: true,
          message: `không thể bỏ trường này`,
        },
      ]
    : [];
  useEffect(() => {
    if (focus && inputRef.current) {
      inputRef.current.focus();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inputRef]);
  return (
    <Form.Item
      name={name}
      label={label}
      {...layout}
      className={className}
      rules={rulesProps}
    >
      <Input placeholder={placeholder} ref={inputRef} />
    </Form.Item>
  );
}

export default InputForm;
