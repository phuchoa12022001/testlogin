import React, { useEffect, useRef } from "react";
import styled from "styled-components";

const PopconfirmStyles = styled.div`
  .PopconfirmItem {
    padding: 0px 10px;
  }
`;
function Popconfirm({ children, setBool , className}) {
  const PopconfirmRef = useRef(null);
  const handleClick = (event) => {
    event.stopPropagation();
  }
  useEffect(() => {
    function handleClickOutside(event) {
      if (
        PopconfirmRef.current &&
        !PopconfirmRef.current.contains(event.target)
      ) {
        setBool(false);
      }
    }
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [PopconfirmRef]); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <PopconfirmStyles ref={PopconfirmRef} className={className} onClick={handleClick}>
      <div className="PopconfirmItem">{children}</div>
    </PopconfirmStyles>
  );
}

export default Popconfirm;
