import { Input } from "antd";
import {
  useState,
  forwardRef,
  useRef,
  useEffect,
  createRef,
  useCallback,
  useLayoutEffect,
} from "react";
import styled from "styled-components";
import dynamic from "next/dynamic";
import draftToHtml from "draftjs-to-html";
import { EditorState, convertToRaw } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

const { TextArea } = Input;

const Editor = dynamic(
  () => import("react-draft-wysiwyg").then((module) => module.Editor),
  {
    ssr: false,
  }
);
const TextAreaContainer = styled.div`
  .ant-input {
    padding: 0px;
    font-weight: 600;
    font-size: 12px;
    line-height: 15px;
    color: ${({ theme }) => theme.colors.gray66};
  }
  display: flex;
  height: auto;
  @media only screen and (max-width: 959px) {
    .ant-input {
      font-weight: 500;
      font-size: 10px;
      line-height: 12px;
    }
  }
`;
const TextAreaPost = forwardRef(({ onChange, value, className }, ref) => {
  return (
    <TextAreaContainer className={className}>
      <TextArea
        value={value}
        onChange={(e) => onChange(e.target.value)}
        placeholder="Bạn đang nghĩ gì?"
        bordered={false}
        ref={ref}
        autoSize={{
          minRows: 1,
          maxRows: 10,
        }}
      />
    </TextAreaContainer>
  );
});
TextAreaPost.displayName = "TextAreaPost";
export const TextAreaPostTest = forwardRef(
  ({ onChange, value, className, isModalVisible }, ref) => {
    const textarea = useRef();
    const HandleChange = (e) => {
      onChange(e.target.value);
    };
    useLayoutEffect(() => {
      if (textarea) {
        textarea.current.autofocus = true;
      }
    }, []);
    return (
      <div className={className}>
        <textarea
          ref={textarea}
          rows="9"
          onChange={HandleChange}
          value={value} 
          placeholder="Viết lời nhận xét"
        ></textarea>
      </div>
    );
  }
);
TextAreaPostTest.displayName = "TextAreaPostTest";
export const TextAreaCompanyinfo = ({
  className,
  value,
  onChange,
  isModalVisible,
}) => {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const [ref, setRef] = useState();

  const onEditorStateChange = (newState) => {
    setEditorState(newState);
    const htmlString = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    onChange(htmlString);
  };
  const setEditorReference = useCallback((refs) => {
    if (refs) {
      if (refs.editor) {
        return refs.focus();
      }
    }
  }, []);

  return (
    <div className="textAreaModal">
      <Editor
        editorState={editorState}
        wrapperClassName="wrapper"
        editorClassName="demo-editor"
        placeholder="Viết lời nhận xét"
        editorRef={setEditorReference}
        toolbarHidden
        toolbar={{
          options: [
            "inline",
            "blockType",
            "fontSize",
            "list",
            "textAlign",
            "history",
          ],
          inline: { inDropdown: true },
          list: { inDropdown: true },
          textAlign: { inDropdown: true },
          link: { inDropdown: true },
          history: { inDropdown: true },
        }}
        onEditorStateChange={onEditorStateChange}
      />
    </div>
  );
};
TextAreaCompanyinfo.displayName = "TextAreaCompanyinfo";

export default TextAreaPost;
