import { Row, Col, Space } from "antd";
import { useState } from "react";
import Router from "next/router";
import { RightOutlined } from "@ant-design/icons";

import TitleWrapper from "../common/title/TitleWrapper";
import NewsItem from "./NewsItem";
import { useNew } from "../../hooks/useNews";
import { useRouter } from "next/router";
import PaginationWrapper from "../common/PaginationWrapper";

import styled from "styled-components";
import Link from "next/link";

const NewsListContainer = styled.section`
  .load__more {
    margin-top: 15px;
    display: block;
    padding: 12px;
    color: ${({ theme }) => theme.colors.primary};
    cursor: pointer;
  }
  .boxCenter {
    display: flex;
    width: 100%;
    justify-content: center;
    margin-top: 27px;
  }
`;

export default function NewsList() {
  const router = useRouter();
  const { page } = router.query;
  const [pagination, setPagination] = useState(false);
  const { data: NewData } = useNew({ page: page || 1 });
  const List = NewData?.data || [];
  const handleChange = (page) => {
    Router.push(`?page=${page}`, undefined, {
      scroll: false,
    });
  };
  return (
    <NewsListContainer style={{ marginTop: "60px" }}>
      <TitleWrapper>Tin tức</TitleWrapper>
      <Row gutter={[30, 41]}>
        {List.slice(0, pagination ? 8 : 4).map((item, index) => (
          <Col md={12} span={24} key={index}>
            <NewsItem {...item} />
          </Col>
        ))}
      </Row>
      {List.length > 4 && (
        <>
          {pagination ? (
            <div className="boxCenter">
              <PaginationWrapper
                HideText={true}
                current={NewData.meta.current_page}
                defaultPageSize={+NewData.meta.per_page}
                total={NewData.meta.total}
                onChange={handleChange}
              />
            </div>
          ) : (
            <Space
              style={{ justifyContent: "center" }}
              onClick={() => setPagination(true)}
            >
              <Link href="/">
                <span className="load__more">
                  Xem thêm{" "}
                  <RightOutlined
                    style={{ opacity: "0.5", marginRight: "-5px" }}
                  />
                  <RightOutlined />
                </span>
              </Link>
            </Space>
          )}
        </>
      )}
    </NewsListContainer>
  );
}
