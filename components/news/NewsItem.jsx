import Link from "next/link";
import styled from "styled-components";
import { Space, Typography, Tag, Divider } from "antd";

const NewsItemContainer = styled.div`
  .date {
    color: ${({ theme }) => theme.colors.black4D};
    width: 60px;
  }
  .titleLink {
    h3.ant-typography {
      margin-bottom: 0;
      font-size: 14px;
      font-weight: 700;
      color: inherit;
    }

    &:hover {
      color: ${({ theme }) => theme.colors.primary};
    }

    @media (max-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
      h3 {
        font-size: ${({ theme }) => theme.fontSizes.mobile};
      }
    }
  }
  .month , .year {
    font-weight: 400;
    font-size: 12px;
    line-height: 15px;
    display: flex;
  }
  .day {
    font-weight: 700;
    font-size: 28px;
    line-height: 34px;
    margin-bottom: 4px;
  }
  .by {
    color: ${({ theme }) => theme.colors.grayEC};

    span {
      color: ${({ theme }) => theme.colors.black};
    }
  }

  .ant-tag {
    margin-right: 0;
    @media (max-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
      font-size: ${({ theme }) => theme.fontSizes.sizeSM};
    }
  }

  .info {
    margin-top: 25px;
    > .ant-space-item + .ant-space-item {
      padding-left: 6px;
      margin-left: 6px;
      border-left: 1px solid ${({ theme }) => theme.colors.black};
    }

    @media (max-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
      font-size: ${({ theme }) => theme.fontSizes.sizeSM};
    }
  }
`;

export default function NewsItem({ title, created_by, created_at, slug }) {
  const d = new Date(created_at);
  let day = d.getDate();
  let month = d.getMonth() + 1;
  let year = d.getFullYear();
  const addZero = (number) => (number < 10 ? "0" + number : number);
  return (
    <NewsItemContainer>
      <Space size={30}>
        <div className="date">
          <div className="day">{addZero(day)}</div>
          <div className="month">tháng {addZero(month)}</div>
          <div className="year">{year}</div>
        </div>

        <div>
          <Link href={`/news/${slug}`}>
            <a className="titleLink">
              <Typography.Title level={3}>{title}</Typography.Title>
            </a>
          </Link>
          <Space size={0} className="info">
            <Tag color="#BABAF3">Tin tức</Tag>

            <Typography.Paragraph className="by">
              by <Typography.Text>{created_by.name}</Typography.Text>
            </Typography.Paragraph>
          </Space>
        </div>
      </Space>
    </NewsItemContainer>
  );
}
