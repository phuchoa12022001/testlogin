import { Space, Typography } from "antd";
import { Spin } from "antd";
import { useContext } from "react";

import { ThemeContext } from "../../Context/Context";
import PopularKeyword from "./PopularKeyword";
import TitleWrapper from "../common/title/TitleWrapper";
import { useKeyWord } from "../../hooks/useKeyWord";

export default function PopularKeywords() {
  const { data: ListKeyWord } = useKeyWord({});
  const { loading } = useContext(ThemeContext);
  const List = ListKeyWord || [];
  return (
    <section>
      <TitleWrapper>
        Từ khoá <span>phổ biến</span>
      </TitleWrapper>
      {!loading ? (
        <Space size={[15, 20]} wrap>
          {List.slice(0, 8).map((item) => (
            <PopularKeyword key={item.id} link={item.keyword}>
              {item.keyword}
            </PopularKeyword>
          ))}
        </Space>
      ) : (
        <Spin />
      )}
    </section>
  );
}
