import styled from "styled-components";


import { useRouter } from "next/router";

const PopularKeywordContainer = styled.span`
  display: inline-block;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  border-radius: 36px;
  padding: 5px 16px;
  color: ${({ theme }) => theme.colors.primary};
  cursor: pointer;
  transition: all 0.3s ease;

  @media (max-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
    font-size: ${({ theme }) => theme.fontSizes.mobile};
  }
  &:hover {
    color: ${({ theme }) => theme.colors.white};
    background-color: ${({ theme }) => theme.colors.primary};
  }
`;

export default function PopularKeyword({ children, link: query }) {
  const router = useRouter();
  const handleHref = (query) => {
    router.push(
      {
        pathname: "/search",
        query: {
          query,
        },
      },
      undefined,
      { shallow: true }
    );
  };
  return (
    <PopularKeywordContainer>
      <p onClick={() => handleHref(query)}>
        <span>{children}</span>
      </p>
    </PopularKeywordContainer>
  );
}
