import React, { useState, useRef } from "react";
import FormData from "form-data";
import { Form, Button } from "antd";
import Router from "next/router";
import styled from "styled-components";
import { useSWRConfig } from "swr";

import InputForm from "../form/input";
import UploadForm from "../form/upoad";
import TextArea from "../form/TextArea";
import EnableForm from "../form/Enable";
import GoogleForm from "./GoogleForm";
import Address from "./Address";
import { useCompanyFollow } from "../../hooks/Company/useCompanyFollow";
import { useCategories } from "../../hooks/usePublic";
import { SelectMultipleForm } from "../form/SelectForm";

const layout = {
  labelCol: {
    span: 24,
  },
  wrapperCol: {
    span: 24,
  },
};

const AddStyles = styled.div`
  .box {
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
    .add {
      width: 150px;
    }
  }
  .uploadAvatar {
    margin-bottom: 20px;
    display: block;
  }
`;
function AddCompanies() {
  const [formOther, setFormOther] = useState({
    status: 1,
    enable: 1,
    images: [],
    province_id: null,
    district_id: null,
    ward_id: null,
    street_id: null,
    categories: [],
  });
  const { addcompanyFollow, uploadImagecompanyFollow } = useCompanyFollow({});
  const { data, isLoading } = useCategories({});
  const { mutate } = useSWRConfig();
  const form = useRef();
  const onFinish = async () => {
    const data = await addcompanyFollow({
      ...formOther,
      avatar: "",
    });
    var form = new FormData();
    for (let i = 0; i < formOther.images.length; i++) {
      form.append("images", formOther.images[i]);
    }
    form.append("company_id ", data.data.id);
    await uploadImagecompanyFollow(form);
    await mutate(`/auth/company`);
    Router.push("/companymanagement");
  };
  const handleChangeImage = (file) => {
    setFormOther((prev) => {
      return {
        ...prev,
        images: [...prev.images, file],
      };
    });
  };
  const handleChangeGoogle = (value) => {
    SaveForm({
      horizontal_axis: value.lat(),
      vertical_axis: value.lng(),
    });
  };
  const SaveForm = (object) => {
    form.current.setFieldsValue({ ...object });
    setFormOther((prev) => {
      return {
        ...prev,
        ...object,
      };
    });
  };
  const handleChangeStatus = (value) => {
    form.current.setFieldsValue({
      status: value,
    });
    setFormOther((prev) => {
      return {
        ...prev,
        status: value,
      };
    });
  };
  const handleChangeSelect = (values) => {
    setFormOther((prev) => {
      return {
        ...prev,
        categories: values,
      };
    });
  };
  const handleChangeForm = (values) => {
    switch (true) {
      case !!values.province_id:
        SaveForm({
          province_id: values.province_id,
          district_id: null,
          ward_id: null,
          street_id: null,
        });
        break;
      case !!values.district_id:
        SaveForm({
          district_id: values.district_id,
          ward_id: null,
          street_id: null,
        });
        break;
      default:
        SaveForm(values);
    }
  };
  const handleChangeEnable = (value) => {
    form.current.setFieldsValue({
      enable: value,
    });
    setFormOther((prev) => {
      return {
        ...prev,
        enable: value,
      };
    });
  };
  return (
    <AddStyles>
      <Form
        name="form"
        onFinish={onFinish}
        ref={form}
        onValuesChange={handleChangeForm}
        initialValues={formOther}
      >
        <InputForm
          name={"name"}
          label={"Tên"}
          placeholder="Nhập tên công ty"
          focus={true}
          layout={layout}
        />
        <UploadForm
          name={"avatar"}
          value={null}
          onChangeUpload={handleChangeImage}
          label={"Hình ảnh công ty"}
          className="uploadAvatar"
        />
        <TextArea
          name={"description"}
          label={"Mô tả"}
          placeholder="Nhập tên fax_code"
          layout={layout}
          rows={5}
        />
        {!isLoading && (
          <SelectMultipleForm
            value={formOther.categories}
            data={data}
            onChange={handleChangeSelect}
            name={"categories"}
            label={"Loại"}
            placeholder="Chọn loại công ty"
            layout={layout}
            rulesProps={false}
          />
        )}
        <Address formOther={formOther} layout={layout} />
        <InputForm
          name={"address"}
          label={"Địa chỉ"}
          placeholder="Nhập địa chỉ "
          layout={layout}
        />
        <InputForm
          name={"fax_code"}
          label={"Fax_code"}
          placeholder="Nhập fax_code"
          rules={false}
          layout={layout}
        />
        <InputForm
          name={"phone"}
          label={"Điện thoại"}
          placeholder="Nhập điện thoại"
          layout={layout}
        />
        <InputForm
          name={"email"}
          label={"Email"}
          placeholder="Nhập Email"
          layout={layout}
        />
        <InputForm
          name={"website"}
          label={"Trang website"}
          placeholder="Nhập Trang website"
          rules={false}
          layout={layout}
        />
        <InputForm
          name={"category"}
          label={"Thể loại"}
          placeholder="Lưu ý dùng dấu quẩy cắt từ khóa"
          rules={false}
          layout={layout}
        />
        <InputForm
          name={"keyword"}
          label={"Từ khóa"}
          placeholder="Lưu ý dùng dấu quẩy cắt từ khóa"
          rules={false}
          layout={layout}
        />
        <InputForm
          name={"manager"}
          label={"Quản lí"}
          placeholder="Lưu ý dùng dấu quẩy cắt từ khóa"
          rules={false}
          layout={layout}
        />
        <InputForm
          name={"manager_phone"}
          label={"Số điện thoại quản lí"}
          placeholder="Số điện thoại quản lí"
          rules={false}
          layout={layout}
        />
        <EnableForm
          form={form}
          enable={formOther.status}
          onChange={handleChangeStatus}
          name={"status"}
          label="Trạng thái"
        />
        <EnableForm
          form={form}
          enable={formOther.enable}
          onChange={handleChangeEnable}
          name={"enable"}
          label="Hiện"
          rules={false}
        />
        <GoogleForm
          onChange={handleChangeGoogle}
          value={
            formOther.horizontal_axis && formOther.vertical_axis
              ? {
                  lat: formOther.horizontal_axis,
                  lng: formOther.vertical_axis,
                }
              : null
          }
        />
        <div className="box">
          <Button type="primary" className="add" htmlType="submit">
            Thêm
          </Button>
        </div>
      </Form>
    </AddStyles>
  );
}

export default AddCompanies;
