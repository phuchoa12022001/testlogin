import React from "react";
import { Spin } from "antd";
import styled from "styled-components";
import Router from "next/router";
import Link from "next/link";
import { useRouter } from "next/router";

import CompanyItem from "../company/CompanyItem";
import { useCompanyFollow } from "../../hooks/Company/useCompanyFollow";
import PaginationWrapper from "../common/PaginationWrapper";
import { formatFullAddress } from "../../helpers/common";
import TitleBoxInfo from "../title/TitleBoxInfo";

const CompanyfollowStyled = styled.div`
  margin-bottom: 10px;
  .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-bottom: 7px;
    border-bottom: 1px solid #ececec;
    margin-bottom: 14px;
    .add {
      cursor: pointer;
    }
  }
  .boxCenter {
    text-align: center;
    display: flex;
    justify-content: center;
    margin-top: 27px;
  }
  .item {
    margin-bottom: 10px;
  }
  .btn {
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
  }
`;
function Companyfollow() {
  const router = useRouter();
  const { page } = router.query;
  const { data, loading } = useCompanyFollow({ page: page || 1 });
  const handleChange = (page) => {
    Router.push(`/companymanagement?page=${page}`, undefined, {
      scroll: false,
    });
  };
  return (
    <CompanyfollowStyled>
      <div className="header">
        <TitleBoxInfo text={"Công ty bạn đã lưu trữ"} />
        <Link href="/companymanagement/add">
          <p className="add">Thêm Công ty</p>
        </Link>
      </div>
      {!loading ? (
        <>
          {data.data.data.map((company) => {
            const fullAddress = formatFullAddress(
              company.address,
              company.street?.name,
              company.ward?.name,
              company.district?.name,
              company.province?.name
            );
            return (
              <div className="item" key={company.id}>
                <CompanyItem
                  key={company.id}
                  id={company.id}
                  name={company.name}
                  fullAddress={fullAddress}
                  description={company.description}
                  email={company.email}
                  website={company.website}
                  phones={company.phones || company.phone.split(" ")}
                  updatedAt={company.updated_at}
                  isOnlineStatus={!!company.status}
                  mobiles={company.mobiles || []}
                  geo={company.geo}
                  slug={company.slug}
                  hideBoxlater={true}
                  user_id={company.user_id}
                  enable={company.enable}
                />
              </div>
            );
          })}
        </>
      ) : (
        <Spin />
      )}
      {!loading && (
        <div className="boxCenter">
          <PaginationWrapper
            HideText={true}
            current={data.data.current_page}
            defaultPageSize={+data.data.page_size}
            total={data.data.total}
            onChange={handleChange}
          />
        </div>
      )}
    </CompanyfollowStyled>
  );
}

export default Companyfollow;
