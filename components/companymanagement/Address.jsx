import React from "react";
import { Select, Form, Spin } from "antd";
import styled from "styled-components";

import { useAddress } from "../../hooks/usePublic";
import { isIncludeTextSearch } from "../../helpers/common";

const { Option } = Select;

const AddressStyles = styled.div``;

function Address({ formOther, layout = {} }) {
  const { data, isLoading } = useAddress(formOther);
  return (
    <AddressStyles>
      {!isLoading ? (
        <>
          <Form.Item
            name={"province_id"}
            label={"Tỉnh"}
            className={"address"}
            {...layout}
            rules={[
              {
                required: true,
                message: `không thể bỏ trường này`,
              },
            ]}
          >
            <Select
              showSearch
              placeholder="Chọn tỉnh tại đây"
              filterOption={(input, option) =>
                isIncludeTextSearch(input, option.children)
              }
            >
              {data.AllProvinces.map((province) => (
                <Option value={province.id} key={province.id}>
                  {province.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"district_id"}
            initialvalues={formOther.district_id}
            {...layout}
            label={"Huyện"}
            className={"address"}
            rules={[
              {
                required: true,
                message: `không thể bỏ trường này`,
              },
            ]}
          >
            <Select
              showSearch
              placeholder="Chọn huyện tại đây"
              filterOption={(input, option) =>
                isIncludeTextSearch(input, option.children)
              }
            >
              {data.AllDistricts.map((districts) => (
                <Option value={districts.id} key={districts.id}>
                  {districts.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"ward_id"}
            label={"Thị trấn"}
            className={"address"}
            {...layout}
            rules={[
              {
                required: true,
                message: `không thể bỏ trường này`,
              },
            ]}
          >
            <Select
              showSearch
              placeholder="Chọn thị trấn tại đây"
              optionFilterProp="children"
              filterOption={(input, option) =>
                isIncludeTextSearch(input, option.children)
              }
            >
              {data.AllWarn.map((warn) => (
                <Option value={warn.id} key={warn.id}>
                  {warn.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name={"street_id"}
            label={"Đường"}
            {...layout}
            className={""}
            rules={[  
              {
                required: true,
                message: `không thể bỏ trường này`,
              },
            ]}
          >
            <Select
              showSearch
              placeholder="Chọn đường tại đây"
              optionFilterProp="children"
              filterOption={(input, option) =>
                isIncludeTextSearch(input, option.children)
              }
            >
              {data.AllStreets.map((streets) => (
                <Option value={streets.id} key={streets.id}>
                  {streets.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </>
      ) : (
        <Spin />
      )}
    </AddressStyles>
  );
}

export default Address;
