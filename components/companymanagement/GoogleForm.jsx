import React from "react";
import { Form } from "antd";

import GoogleMapWithSearch from "../GoogleMap/GoogleMapWithSearch";

function GoogleForm({onChange , value}) {
  return (
    <Form.Item>
      <GoogleMapWithSearch onChange={onChange} value={value} />
    </Form.Item>
  );
}

export default GoogleForm;
