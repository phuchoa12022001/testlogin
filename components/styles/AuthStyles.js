import styled from "styled-components";

export const AuthStyles = styled.div`
  .title {
    font-size: 28px;
    font-weight: 700;
    color: ${({ theme }) => theme.colors.secondary} !important;
    margin-bottom: 0;
    text-transform: uppercase;
  }

  .ant-form-item {
    margin-bottom: 8px;
  }
  .boxImage {
    position: relative;
    transform: translateX(-60px);
  }
  .ant-input,
  .ant-input-affix-wrapper {
    background-color: ${({ theme }) => theme.colors.grayEC};
    border-radius: 4px;
  }

  .ant-input-status-error:not(.ant-input-disabled):not(.ant-input-borderless).ant-input,
  .ant-input-status-error:not(.ant-input-disabled):not(.ant-input-borderless).ant-input:hover,
  .ant-input-affix-wrapper-status-error:not(.ant-input-affix-wrapper-disabled):not(.ant-input-affix-wrapper-borderless).ant-input-affix-wrapper,
  .ant-input-affix-wrapper-status-error:not(.ant-input-affix-wrapper-disabled):not(.ant-input-affix-wrapper-borderless).ant-input-affix-wrapper:hover {
    background-color: ${({ theme }) => theme.colors.grayEC};
  }

  .ant-input {
    &::-webkit-input-placeholder {
      color: ${({ theme }) => theme.colors.black4D};
    }
    &::-moz-placeholder {
      color: ${({ theme }) => theme.colors.black4D};
    }
    &:-ms-input-placeholder {
      color: ${({ theme }) => theme.colors.black4D};
    }
    &:-moz-placeholder {
      color: ${({ theme }) => theme.colors.black4D};
    }
  }

  .ant-checkbox-inner {
    width: 25px;
    height: 25px;
    border-radius: 3px;
    border-color: ${({ theme }) => theme.colors.primary};

    &::after {
      left: 35%;
    }
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${({ theme }) => theme.colors.primary};
  }

  .ant-checkbox + span {
    display: flex;
    align-self: center;
    font-size: 12px;
  }
  .more {
    font-family: "Inter";
    font-style: italic;
    font-weight: 400;
    font-size: 12px;
    line-height: 15px;
    text-align: left;
    color: ${({ theme }) => theme.colors.black4D} !important;
  }
  .link {
    color: ${({ theme }) => theme.colors.secondary};
    font-weight: 700;
    font-size: 12px;
    line-height: 15px;
    font-style: normal;
  }
  .forgotpassword {
    font-weight: 500;
    font-size: 10px;
    line-height: 12px;
    font-style: italic;
  }
  .title {
    font-weight: 700;
    font-size: 28px;
    line-height: 34px;
    margin-bottom: 20px;
  }
  .login {
    font-weight: 400;
    font-size: 14px;
    line-height: 15px;
    color: ${({ theme }) => theme.colors.black4D} !important;
    margin-bottom: 20px;
  }
  .text {
    margin-bottom: 23px;
    font-weight: 400;
    font-size: 14px;
    line-height: 15px;
    color: ${({ theme }) => theme.colors.black4D} !important;
  }
  .form {
    display: flex;
    justify-items: center;
    flex-direction: column;
  }
  .formStyle {
    text-align: center;
  }
  @media only screen and (max-width: 576px) {
    .title {
      margin-top: 30px;
    }
  }
  @media only screen and (max-width: 992px) {
    .boxImage {
      margin-top: 85px;
      position: unset;
      transform: unset;
      justify-content: center;
      display: flex;
    }
    .box {
      width: 100%;
    }
    .title {
      margin-top: 30px;
    }
    .formStyle {
      text-align: left;
    }
  }
`;
