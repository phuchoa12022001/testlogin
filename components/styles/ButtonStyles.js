import styled, { css } from 'styled-components';
import { Button } from 'antd';

export const ButtonStyles = styled(Button)`
  font-size: 16px;
  color: ${({ theme }) => theme.colors.white};
  border-radius: 4px;
  height: auto;
  padding: 4px 20px;

  ${({ type }) => {
    if (type === 'green') {
      return css`
        background-color: ${({ theme }) => theme.colors.primary};
      `;
    }

    if (type === 'blue') {
      return css`
        background-color: ${({ theme }) => theme.colors.blue};
      `;
    }

    if (type === 'red') {
      return css`
        background-color: ${({ theme }) => theme.colors.red};
      `;
    }
  }}
  margin-bottom: 12px;
`;
