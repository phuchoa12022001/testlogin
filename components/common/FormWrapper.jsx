import styled from 'styled-components';
import {forwardRef} from "react";
import { Form } from 'antd';

const FormWrapperStyles = styled.div`
  .ant-input:focus,
  .ant-input-focused {
    border-color: transparent;
    box-shadow: none;
    border-right-width: 0;
  }

  .ant-select-arrow {
    color: ${({ theme }) => theme.colors.black82};
  }

  .ant-select-single:not(.ant-select-customize-input) .ant-select-selector {
    height: 34px;
  }

  .ant-select-single .ant-select-selector .ant-select-selection-item,
  .ant-select-single .ant-select-selector .ant-select-selection-placeholder {
    line-height: 32px;
  }

  .ant-select:not(.ant-select-customize-input) .ant-select-selector {
    border-color: ${({ theme }) => theme.colors.grayEC};
    border-radius: 4px;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(.ant-select-customize-input)
    .ant-select-selector {
    border-color: ${({ theme }) => theme.colors.grayEC};
    box-shadow: none;
  }
`;

export default forwardRef(function FormWrapper({ children, ...props } , ref) {

  return (
    <FormWrapperStyles>
      <Form layout='vertical' {...props} ref={ref}>
        {children}
      </Form>
    </FormWrapperStyles>
  );
})
