import styled from "styled-components";
import { Space, Pagination, Typography } from "antd";

const PaginationWrapperContainer = styled.div`
  .ant-pagination-item {
    margin-right: 10px;
    min-width: 26px;
    height: 26px;
    line-height: 24px;
    border-color: ${({ theme }) => theme.colors.black82};
    border-radius: 3px;

    a {
      color: ${({ theme }) => theme.colors.black};
    }
  }

  .ant-pagination-item-active {
    background-color: ${({ theme }) => theme.colors.primary};
    border-color: ${({ theme }) => theme.colors.primary};

    a {
      color: ${({ theme }) => theme.colors.white};
    }
  }

  .ant-pagination-prev,
  .ant-pagination-next,
  .ant-pagination-jump-prev,
  .ant-pagination-jump-next {
    min-width: 26px;
    height: 26px;
    line-height: 26px;
    color: ${({ theme }) => theme.colors.black};
  }

  .ant-pagination-jump-prev
    .ant-pagination-item-container
    .ant-pagination-item-link-icon,
  .ant-pagination-jump-next
    .ant-pagination-item-container
    .ant-pagination-item-link-icon {
    color: ${({ theme }) => theme.colors.primary};
  }

  .ant-pagination-jump-prev
    .ant-pagination-item-container
    .ant-pagination-item-ellipsis,
  .ant-pagination-jump-next
    .ant-pagination-item-container
    .ant-pagination-item-ellipsis {
    text-indent: 0;
    color: ${({ theme }) => theme.colors.black};
  }

  .ant-pagination-disabled,
  .ant-pagination-disabled:hover {
    color: ${({ theme }) => theme.colors.grayBD};
  }

  span.ant-typography {
    color: ${({ theme }) => theme.colors.primary};
  }
  @media only screen and (max-width: 576px) {
    .ant-space.ant-space-horizontal.ant-space-align-center {
      display: flex;
      gap: 0px;
      flex-wrap: wrap;
      justify-content: center !important;
    }
  }
`;

export default function PaginationWrapper(props) {
  return (
    <PaginationWrapperContainer>
      <Space style={{ justifyContent: "space-between" }}>
        <Pagination
          itemRender={(page, type, originalElement) => {
            if (type === "prev") {
              return "Trước";
            }
            if (type === "next") {
              return "Sau";
            }

            return originalElement;
          }}
          showSizeChanger={false}
          {...props}
        />
        {!props.HideText && (
          <Typography.Paragraph>
            Hiển thị <Typography.Text>{props.current}</Typography.Text> trên{" "}
            <Typography.Text>
              {Math.ceil(props.total / (props.pageSize ?? 10))}
            </Typography.Text>
          </Typography.Paragraph>
        )}
      </Space>
    </PaginationWrapperContainer>
  );
}
