import styled from 'styled-components';
import { Typography } from 'antd';

const TitleWrapperContainer = styled.div`
  margin-bottom: 15px;

  h2.ant-typography,
  div.ant-typography-h2,
  div.ant-typography-h2 > textarea,
  .ant-typography h2 {
    font-size: 20px;
    font-weight: 700;
    color: ${({ theme }) => theme.colors.black};
    margin-bottom: 0;

    @media (max-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
      font-size: ${({ theme }) => theme.fontSizes.sizeMD};
    }
  }
  span {
    color: ${({ theme }) => theme.colors.primary};
  }

  span.ant-typography {
    color: ${({ theme }) => theme.colors.primary};
  }
`;

export default function TitleWrapper({ children }) {
  return (
    <TitleWrapperContainer>
      <Typography.Title level={2}>{children}</Typography.Title>
    </TitleWrapperContainer>
  );
}
