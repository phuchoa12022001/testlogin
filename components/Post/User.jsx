/* eslint-disable react/display-name */
import {
  UserOutlined,
} from "@ant-design/icons";
import styled from "styled-components";
import { Comment } from "antd";
import React from "react";
import Link from "next/link";
import Image from "next/image";
import clsx from "clsx";

const UserContainer = styled.div`
  .user_size--normal {
    font-weight: 700;
    font-size: 14px;
    line-height: 17px;
  }
  .user_size--large {
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
  }
  .subLink {
    font-weight: 400;
    color: ${({ theme }) => theme.colors.gray66};
    font-size: 12px;
    line-height: 15px;
  }
  .ant-comment-inner {
    padding: 0px;
  }
  .avatarHide {
    .ant-comment-avatar {
      margin-right: 0px;
    }
  }
  @media only screen and (max-width: 959px) {
    .subLink {
      font-size: 10px;
      line-height: 12px;
    }
  }
`;

function User({
  size = "normal",
  color = "#191919",
  img,
  name,
  nameUrl,
  subname,
  subLink,
  className = "",
  sizeAvatar = 30,
}) {
  // eslint-disable-next-line jsx-a11y/alt-text
  const ImageRef = React.forwardRef((props, ref) => <Image {...props} />);
  return (
    <UserContainer>
      <Comment
        className={clsx(className, `${img === "hide" && "avatarHide"}`)}
        author={
          <Link href={nameUrl}>
            <p
              className={`user_size--${size}`}
              style={{ color, cursor: "pointer" }}
            >
              {name}
            </p>
          </Link>
        }
        avatar={
          <Link href={nameUrl}>
            {img !== "hide" ? (
              img ? (
                <ImageRef
                  src={img}
                  alt={name}
                  width={sizeAvatar}
                  height={sizeAvatar}
                />
              ) : (
                <UserOutlined style={{ fontSize: sizeAvatar }} />
              )
            ) : (
              <h3></h3>
            )}
          </Link>
        }
        content={
          <Link href={subLink}>
            <p className="subLink" style={{ cursor: "pointer" }}>
              {subname}
            </p>
          </Link>
        }
      />
    </UserContainer>
  );
}

export default User;