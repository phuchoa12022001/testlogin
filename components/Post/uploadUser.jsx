import { UploadOutlined, CloseOutlined } from "@ant-design/icons";
import { Button, Upload } from "antd";
import { useState } from "react";
import styled from "styled-components";

const UploadImageContainer = styled.div`
  .upload__delete {
    display: flex;
    width: 100%;
    justify-content: flex-end;
  }
  .boxIcon {
    padding: 5px 10px;
    background: black;
    color: white;
    border-radius: 50%;
    cursor: pointer;
  }
`;
const UploadImage = ({
  OnChange,
  handleImage,
  className,
  Hidedelete,
  value,
}) => {
  const [fileList, setfileList] = useState(
    value
      ? [
          {
            uid: "-1",
            name: "image.png",
            status: "done",
            url: value,
          },
        ]
      : []
  );
  const props = {
    beforeUpload: async (file) => {
      handleImage(file);
      file.thumbUrl = await convertFileBase64(file);
      props.saveImgae(file);
      onUpload(file.thumbUrl);
    },
    onRemove: () => {
      setfileList([]);
    },
    saveImgae: (file) => {
      file.status = "done";
      setfileList([file]);
    },
  };
  const handleClick = () => {
    OnChange(false);
    setfileList([]);
  };
  return (
    <UploadImageContainer className={className}>
      <div className="upload__delete">
        {!Hidedelete && (
          <div className="boxIcon" onClick={handleClick}>
            <CloseOutlined style={{ fontSize: 14 }} />
          </div>
        )}
      </div>
      <Upload
        listType="picture"
        defaultFileList={[...fileList]}
        accept="image/*"
        maxCount={1}
        {...props}
      >
        <Button icon={<UploadOutlined />}>Upload</Button>
      </Upload>
      <br />
    </UploadImageContainer>
  );
};

export default UploadImage;
