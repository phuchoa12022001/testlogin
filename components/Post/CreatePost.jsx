/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/display-name */
import React, { useRef, useState } from "react";
import User from "./User";
import styled from "styled-components";
import { Form, Modal, Upload, Image } from "antd";

import TextAreaPost from "../Text Editor/TextAreaPost";
import UploadImage from "./uploadUser";
import { useBlog } from "../../hooks/useBlog";
import { useAuth } from "../../hooks/useAuth";
const CreatePostContainer = styled.div`
  background:  #FBFBFB;
  padding: 16px;
  border-radius: 2px;
  border: 1px solid ${({ theme }) => theme.colors.blackF2};
  .stt {
    border: none;
    outline: none;
    font-size: 12px;
    line-height: 15px;
    padding: 0px;
    font-weight: 600;
    color: ${({ theme }) => theme.colors.gray66};
  }
  .user {
    padding-bottom: 20px;
  }
  .wapper {
    margin-bottom: 36px;
  }
  .ant-input:focus {
    box-shadow: none;
  }
  .error {
    font-size: 10px;
    line-height: 12px;
    color: red;
  }
  .user_postStt {
    border: none;
    outline: none;
    padding: 0px;
    display: none;
  }
  .action {
    display: flex;
    justify-content: space-between;
    align-items: center;
    .action_btn {
      font-weight: 700;
      font-size: 16px;
      line-height: 19px;
      display: flex;
      background: #e0e0e0;
      align-items: center;
      text-align: center;
      padding: 10px 16px;
      border: none;
      border-radius: 2px;
      color: ${({ theme }) => theme.colors.black4D};
      cursor: pointer;
    }
  }
  .action__upload {
    display: flex;
    img {
      border: 1px solid #e5e5e5;
    }
    .box__upload:nth-child(1) {
      padding-right: 10px;
      border-right: 1px solid #f2f2f2;
    }
    .box__upload:nth-child(2) {
      padding-left: 10px;
      display: flex;
      align-items: center;
      gap: 5px;
      p.title-alluser {
        font-size: 10px;
        line-height: 12px;
        font-weight: 400;
        color: #66676B;
      }
    }
  }
  .box__upload__image {
    cursor: pointer;
  }
  .ant-form-item {
    margin-bottom: 0px;
  }
  .ant-form-item-control-input {
    min-height: unset;
  }
  @media only screen and (max-width: 959px) {
    .box__upload:nth-child(1) {
      border-right: none !important;
    }
    .action__upload {
      img {
        border: none;
      }
    }
    button.action_btn {
      background: transparent !important;
    }
  }
`;

const CreatePost = () => {
  const [open, setOpen] = useState(false);
  const [form] = Form.useForm();
  const { add } = useBlog({});
  const { profile } = useAuth({});
  const [objectForm, setObjectForm] = useState({
    content: "",
    image: "",
  });
  const handleChange = (value) => {
    form.setFieldsValue({
      content: value,
    });
    setObjectForm((prev) => {
      return {
        ...prev,
        content: value,
      };
    });
  };
  const handleOpen = () => {
    setOpen(true);
  };
  const handleImage = (file) => {
    form.setFieldsValue({
      image: file,
    });
    setObjectForm((prev) => {
      return {
        ...prev,
        image: file,
      };
    });
  };
  const onFinish = (values) => {
    var formData = new FormData();
    formData.append("content", values.content);
    formData.append("image", values.image ? values.image : "");
    form.setFieldsValue({
      image: "",
      content: "",
    });
    setObjectForm((prev) => {
      return {
        image: "",
        content: "",
      };
    });
    setOpen(false);
    add(formData);
  };
  return (
    <CreatePostContainer>
      <Form form={form} name="control-hooks" onFinish={onFinish}>
        <User
          img={profile.avatar || ""}
          name={profile.name || profile.email}
          nameUrl="/"
          subname="Xem trang ca nhan cua ban"
          subLink="/"
          className="user"
        />
        <div className="wapper">
          <Form.Item name="content">
            <TextAreaPost onChange={handleChange} value={objectForm.value} />
          </Form.Item>
        </div>
        {open && (
          <Form.Item name="image">
            <UploadImage OnChange={setOpen} handleImage={handleImage} />
          </Form.Item>
        )}

        <div className="action">
          <div className="action__upload">
            <div className="box__upload">
              <img
                width={14}
                src="/static/icon/upload_user.svg"
                className="box__upload__image"
                onClick={handleOpen}
              />
            </div>
            <div className="box__upload">
              <Image width={14} src="/static/icon/upload_demo.svg" />
              <p className="title-alluser">Mọi người</p>
            </div>
          </div>
          <button className="action_btn">Đăng</button>
        </div>
      </Form>
    </CreatePostContainer>
  );
};

export default CreatePost;
