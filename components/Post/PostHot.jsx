/* eslint-disable @next/next/no-img-element */
import React from "react";
import styled from "styled-components";

const PostHotContainer = styled.div`
  display: flex;
  .boxImgae {
    margin-right: 11px;
  }
  .title {
    font-weight: 400;
    font-size: 14px;
    line-height: 15px;
  }
  .subname {
    font-weight: 400;
    font-size: 12px;
    line-height: 15px;  
    margin-bottom : 2px;
    color: ${({ theme }) => theme.colors.black82};
  }
  .text {
    font-weight: 400;
    font-size: 12px;  
    line-height: 15px;
  }
  margin-bottom : 12px;
  @media only screen and (max-width: 959px) {
    margin-bottom : 10px;
  }
`;
function PostHot() {
  return (
    <PostHotContainer>
      <div className="boxImgae">
        <img src="/static/images/image1.png" alt="image" />
      </div>
      <div className="Content">
        <p className="title">Nhà cung cấp công nghệ VPN</p>
        <p className="subname">Công nghệ tiếp thị</p>
        <p className="text">
          Lorem ipsum dolor sit amet, consecte adipiscing elit.
        </p>
      </div>
    </PostHotContainer>
  );
}

export default PostHot;
