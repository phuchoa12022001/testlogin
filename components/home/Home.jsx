import styled from "styled-components";
import { Typography } from "antd";
import Search from "../Search";

const HomeContainer = styled.section`
  margin-bottom: 60px;

  h1.ant-typography {
    font-size: 25px;
    font-weight: 700;
    color: ${({ theme }) => theme.colors.black};
    margin-bottom: 4px;

    span {
      color: ${({ theme }) => theme.colors.primary};
    }
  }

  .subTitle {
    color: ${({ theme }) => theme.colors.black};
    font-size: 16px;
  }

  .desc {
    color: ${({ theme }) => theme.colors.black};

    span {
      // font-style: italic;
    }

    @media (max-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
      font-size: ${({ theme }) => theme.fontSizes.sizeSM};
    }
  }

  .search {
    margin-top: 105px;
    margin-bottom: 13px;
    @media (max-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
      margin-top: 0;
    }
  }
`;

export default function Home() {
  return (
    <HomeContainer>

      <div className="search">
        <Search hasColor />
      </div>

      <Typography.Paragraph className="desc">
        <Typography.Text strong>Ex:</Typography.Text>{" "}
        <Typography.Text>
          Bất động sản nghỉ dưỡng, Nhà liền kề, Dự án đất nền Vũng Tàu
        </Typography.Text>
      </Typography.Paragraph>
    </HomeContainer>
  );
}
