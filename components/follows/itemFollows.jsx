import React from "react";
import styled from "styled-components";

import ImageComponent from "../image/Image";

const ItemFollowsStyled = styled.div`
  display: flex;
  margin-bottom: 14px;
  .right {
    flex: 1;
    margin-left: 9px;
    display: flex;
    align-items: flex-end;
    flex-wrap: wrap;
    .name {
      font-weight: 700;
      font-size: 14px;
      line-height: 17px;
      color: ${({ theme }) => theme.colors.primary};
    }
    .notification {
      font-weight: 400;
      font-size: 12px;
      line-height: 15px;
      color: ${({ theme }) => theme.colors.black4D};
      margin-left: 20px;
    }
    .info {
      width: 100%;
      display: flex;
      align-items: center;
      margin-bottom: 5px;
    }
    .action {
      display: flex;
      p {
        color: ${({ theme }) => theme.colors.green05};
        font-weight: 500;
        font-size: 10px;
        line-height: 12px;
        margin-right: 20px;
      }
    }
  }
  .avatar {
    border-radius: 50%;
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
    .img {
      display: flex;
    }
  }
`;
function ItemFollows({ user_follow_info }) {
  const { name, email, avatar } = user_follow_info;
  return (
    <ItemFollowsStyled>
      <div className="avatar">
        <ImageComponent src={avatar} className="img" alt="image" size={53} />
      </div>
      <div className="right">
        <div className="box">
          <div className="info">
            <h3 className="name">{name || email}</h3>
            <p className="notification">Vừa đăng 1 tin tức mới</p>
          </div>
          <div className="action">
            <p className="detailPage">Xem chi tiết</p>
            <p className="toggle">Bỏ theo dõi</p>
          </div>
        </div>
      </div>
    </ItemFollowsStyled>
  );
}

export default ItemFollows;
