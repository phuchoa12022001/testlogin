import React from "react";
import styled from "styled-components";
import { PlusOutlined } from "@ant-design/icons";

import User from "../Post/User";

const FollowContainer = styled.div`
  .follow_box {
    margin-left: 44px;
  }
  .follow_text {
    font-weight: 400;
    font-size: 12px;
    line-height: 15px;
  }
  .follow_btn {
    margin-top: 6px;
    border-radius: 30px;
    display: flex;
    align-items: center;
    outline: none;
    padding: 4px 3px 4px 12px;
    border: 1px solid ${({ theme }) => theme.colors.gray66};
  }
  .follow_btn_text {
    font-weight: 400;
    font-size: 12px;
    line-height: 15px;
  }
  .follow_icon {
    margin-right: 2px;
  }
  margin-bottom: 11px;
  @media only screen and (max-width: 959px) {
    margin-bottom: 12px;
  }
`;
function follow() {
  return (
    <FollowContainer className={"followBottom"}>
      <User
        img={
          "https://i.pinimg.com/564x/a5/77/fd/a577fd8e9c442cc8ab301b2bd6871f56.jpg"
        }
        name="Huy Nguyen Dinh"
        nameUrl="/"
        subname="Comapany • Imformation Technology and Services"
        subLink="/"
        className="user"
        sizeAvatar={32}
      />
      <div className="follow_box">
        <button className={"follow_btn"}>
          <PlusOutlined
            className={"follow_icon"}
            style={{ fontSize: "14px" }}
          />
          <p className="follow_btn_text">Theo dõi</p>
        </button>
      </div>
    </FollowContainer>
  );
}

export default follow;
