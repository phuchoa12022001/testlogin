import React from "react";
import { Spin } from "antd";
import styled from "styled-components";
import Router from "next/router";
import { useRouter } from "next/router";

import ItemFollows from "./itemFollows";
import { useIFollow } from "../../hooks/follows/useIFollow";
import PaginationWrapper from "../common/PaginationWrapper";

const ListFollowStyled = styled.div`
  .boxCenter {
    width: 100%;
    margin-top: 27px;
    display: flex;
    justify-content: center;
  }
`;
function ListFollow() {
  const router = useRouter();
  const { page } = router.query;
  const { data, loading } = useIFollow({ page: page || 1 });
  const handleChange = (page) => {
    Router.push(`/companymanagement?page=${page}`, undefined, {
      scroll: false,
    });
  };
  return (
    <ListFollowStyled>
      {!loading ? (
        <>
          {data.data.map((follow ) => (
            <ItemFollows {...follow} key={follow.id} />
          ))}
        </>
      ) : (
        <Spin />
      )}
      {!loading && (
        <div className="boxCenter">
          <PaginationWrapper
            HideText={true}
            current={data.meta.current_page}
            defaultPageSize={+data.meta.per_page}
            total={data.meta.total}
            onChange={handleChange}
          />
        </div>
      )}
    </ListFollowStyled>
  );
}

export default ListFollow;
