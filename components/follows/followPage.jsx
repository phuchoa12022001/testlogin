import React from "react";
import styled from "styled-components";

import TitleBoxInfo from "../title/TitleBoxInfo";
import ListFollow from "./listFollow";

const FollowPageStyled = styled.div`
  .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-bottom: 7px;
    border-bottom: 1px solid #ececec;
    margin-bottom: 17px;
  }
`;
function FollowPage() {
  return (
    <FollowPageStyled>
      <div className="header">
        <TitleBoxInfo text={"Người dùng bạn đang quan tâm"} />
      </div>
      <ListFollow />
    </FollowPageStyled>
  );
}

export default FollowPage;
