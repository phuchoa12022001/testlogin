/* eslint-disable @next/next/no-img-element */
import React, { useEffect, createRef, useState } from "react";
import styled from "styled-components";
import ChangeTime from "../ChangeTime";
import { Button } from "antd";

import User from "../Post/User";
import MenuBlog from "./Menu";
import LineText from "./lineText";
import ListComment from "../comments/listComment";
import { CommentIcon } from "../../assets/icons/index";
import UploadImage from "../Post/uploadUser";
import { useAuth } from "../../hooks/useAuth";
import TextAreaPost from "../Text Editor/TextAreaPost";

// register it.

const BlogContainer = styled.div`
  margin-bottom: 83px;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  .info {
    display: flex;
    justify-content: space-between;
    width: 100%;
  }
  .action {
    display: flex;
    align-items: flex-start;
    .time {
      font-size: 10px;
      line-height: 12px;
      margin-right: 15px;
    }
  }
  .desc {
    width: 100%;
    margin-top: 12px;
    font-size: 14px;
    font-weight: 400;
    line-height: 15px;
    margin-bottom: 2px;
    overflow: hidden;
    max-height: 75px;
  }
  .more {
    font-size: 12px;
    line-height: 14.52px;
    font-weight: 600;
    cursor: pointer;
    color: ${({ theme }) => theme.colors.primary};
  }
  .image {
    margin-bottom: 5px;
    width: 100%;
  }
  .amount {
    display: flex;
    align-items: center;
    .comment {
      font-weight: 400;
      font-size: 12px;
      line-height: 15px;
      margin-left: 5px;
    }
    margin-bottom: 21px;
  }
  .boxImage {
    background: ${({ theme }) => theme.colors.primary};
    height: 20px;
    width: 20px;
    border-radius: 50%;
    position: relative;
    .CommentIcon {
      width: 10.09px;
      height: 11.31px;
      color: #fff;
      position: absolute;
      left: 50%;
      top: 55%;
      transform: translate(-50%, -50%);
    }
  }
  .BoxAction {
    margin-bottom: 10px;
    display: flex;
    width: 100%;
    justify-content: flex-end;
    button {
      margin-left: 10px;
    }
  }
  .TextAreaPost {
    width: 100%;
    padding: 10px 0px;
  }
  .BoxBtn {
    width: 100%;
    display: flex;
    padding-bottom: 19px;
    .Blog_icon {
      color: ${({ theme }) => theme.colors.primary};
    }
    .Blog__btn {
      display: flex;
      cursor: pointer;
    }
    .Blog__btn img {
      height: 12px;
      width: 12px;
      margin-right: 6px;
    }
    .Blog__btn p {
      font-size: 12px;
      line-height: 12px;
    }
    .Blog__btn + .Blog__btn {
      margin-left: 15px;
    }
  }
  .Blog__line {
    background: ${({ theme }) => theme.colors.black82};
    height: 1px;
    width: 100%;
    margin-bottom: 21px;
  }
  .BoxComment {
    width: 100%;
  }
  .CreateComment {
    display: flex;
    width: 100%;
  }
  .ListComment {
    margin-top: 12px;
  }
  .UploadImage {
    width: 100%;
    margin-top: 10px;
  }
  @media only screen and (max-width: 959px) {
    position: relative;
    .amount {
      p.comment {
        font-size: 10px;
        line-height: 12px;
      }
    }
    .action {
      order: -1;
      width: 100%;
      display: flex;
      justify-content: end;
    }
    .info {
      flex-wrap: wrap;
    }
    .user {
      width: 100%;
      order: 1;
    }
    .desc {
      margin-top: 8px;
      font-size: 10px;
      line-height: 12px;
      margin-bottom: 8px;
      overflow: hidden;
      max-height: 60px;
    }
    .image {
      margin-bottom: 10px;
    }
    .more {
      font-size: 10px;
      line-height: 12px;
      font-weight: 500;
    }
    .Blog__line {
      margin-bottom: 10px;
    }
    .Blog__line--mobile {
      margin-bottom: 16px;
    }
    margin-bottom: 10px;
  }
`;
function Blog(props) {
  const [openComment, setOpenComment] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [content, setContent] = useState(null);
  const { profile } = useAuth({});
  const ref = createRef();
  const handleToggleComment = () => {
    setOpenComment((pver) => !pver);
  };
  const handleChangeMenu = (action) => {
    if (action === "item-edit") {
      setOpenEdit(true);
    } else {
      props.deleteBlog(props.id);
    }
  };
  const handleCloseEdit = () => {
    setOpenEdit(false);
  };
  const handleImage = (file) => {};
  const handleEdit = () => {
    setOpenEdit(false);
  };
  useEffect(() => {
    if (openEdit) {
      ref.current.focus();
      //  focus textArea và đưa nó đến cuối văn bản
      ref.current.resizableTextArea.textArea.setSelectionRange(
        String.length,
        String.length
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [openEdit]);

  return (
    <BlogContainer>
      {props.enable ? (
        <>
          <div className="info">
            <User
              img={profile.avatar || ""}
              name={profile.name || profile.email}
              fontMobile={true}
              nameUrl="/"
              subname=""
              subLink="/"
              className="user"
              sizeAvatar={30}
            />
            <div className="action">
              <p className="time">
                <ChangeTime datetime={props.created_at} locale="vi" />
              </p>
              <div className="menu">
                <MenuBlog handleChangeMenu={handleChangeMenu} />
              </div>
            </div>
          </div>
          {openEdit ? (
            <>
              <UploadImage
                className={"UploadImage"}
                handleImage={handleImage}
                Hidedelete={true}
                value={props.image}
              />
              <TextAreaPost
                className="TextAreaPost"
                value={content}
                ref={ref}
              />
              <div className="BoxAction">
                <Button type="danger" onClick={handleCloseEdit}>
                  Hủy bỏ
                </Button>
                <Button type="primary" onClick={handleEdit}>
                  Sửa
                </Button>
              </div>
            </>
          ) : (
            <LineText content={props.content} />
          )}
          {props.image && (
            <img src={props.image} alt="image" className="image" />
          )}
          <div className="amount">
            <div className="boxImage">
              <CommentIcon className="CommentIcon" />
            </div>
            <p className="comment">6 Comment</p>
          </div>
          <div className="BoxBtn">
            <div className="Blog__btn" onClick={handleToggleComment}>
              <img src="/static/icon/comment2.svg" alt="comment2" />
              <p>Comment</p>
            </div>
            <div className="Blog__btn">
              <img src="/static/icon/seen.svg" alt="comment2" />
              <p>Send</p>
            </div>
          </div>
          <div
            className={`Blog__line ${openComment && "Blog__line--mobile"}`}
          ></div>
          <ListComment
            id={props.id}
            openComment={openComment}
            AvatarUser={profile.avatar || null}
          />
        </>
      ) : null}
    </BlogContainer>
  );
}

export default Blog;
