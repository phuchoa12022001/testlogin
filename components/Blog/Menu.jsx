import { useState } from "react";
import {
  DeleteOutlined,
  EllipsisOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { Menu, Dropdown } from "antd";
import styled from "styled-components";

const MenuContainer = styled.div`
  display: flex;
  align-items: flex-start;
  cursor: pointer;
  .ant-menu-horizontal {
    height: 12px;
    border: 0;
    border-bottom: 1px solid none;
    box-shadow: none;
    background: unset;
  }
  .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-submenu:hover::after,
  .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-submenu-active::after,
  .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-submenu-active::after,
  .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-submenu-selected::after {
    border-bottom: 2px solid #fff !important;
  }
  .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-item,
  .ant-menu-horizontal:not(.ant-menu-dark) > .ant-menu-submenu {
    padding: 0px;
  }
`;

const MenuBlog = ({ handleChangeMenu }) => {
  const [current, setCurrent] = useState("mail");
  const items = [
    { label: "Xóa bài viết", key: "item-delete", icon: <DeleteOutlined /> }, // which is required
  ];
  const handleClick = (e) => {
    handleChangeMenu(e.key);
    setCurrent(e.key);
  };
  const menu = (
    <Menu onClick={handleClick} selectedKeys={[current]} items={items}></Menu>
  );
  return (
    <MenuContainer>
      <Dropdown overlay={menu} placement="bottomLeft" arrow>
        <EllipsisOutlined />
      </Dropdown>
    </MenuContainer>
  );
};

export default MenuBlog;
