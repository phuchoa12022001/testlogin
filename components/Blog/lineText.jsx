import React, { useEffect, useRef, useState } from "react";
import Link from "next/link";
import LineCount from "../../helpers/lineCount";

function LineText({ current, content }) {
  const desc = useRef(desc);
  const [line, setLine] = useState(null);
  useEffect(() => {
    // setElemt(desc.current);
    if (window.innerWidth > 959) {
      const line = LineCount(desc.current, 15);
      setLine(line);

    } else {
      const line = LineCount(desc.current, 12);
      setLine(line);

    }
  }, [content]);
  return (
    <>
      <p className="desc" ref={desc}>
        {content && <>{content}</>}
      </p>
      {line === 5 && (
        <Link href="/">
          <span className="more">...Xem thêm</span>
        </Link>
      )}
    </>
  );
}

export default LineText;
