import React, { useContext } from "react";
import { Spin } from "antd";
import styled from "styled-components";
import Router from "next/router";
import { useRouter } from "next/router";

import CompanyItem from "../company/CompanyItem";
import { useCompanyWishlist } from "../../hooks/Company/useCompanyWishlist";
import PaginationWrapper from "../common/PaginationWrapper";
import { formatFullAddress } from "../../helpers/common";
import TitleBoxInfo from "../title/TitleBoxInfo";
import { ThemeContext } from "../../Context/Context";

const MyCompanyStyled = styled.div`
  margin-bottom: 10px;
  .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-bottom: 7px;
    border-bottom: 1px solid #ececec;
    margin-bottom: 14px;
    .add {
      cursor: pointer;
    }
  }
  .boxCenter {
    text-align: center;
    display: flex;
    justify-content: center;
    margin-top: 27px;
  }
  .item {
    margin-bottom: 10px;
  }
  .btn {
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
  }
`;
function MyCompany() {
  const router = useRouter();
  const { page } = router.query;
  const { location } = useContext(ThemeContext);
  const Locationparam = location.LocationId
    ? { LocationId: location.LocationId }
    : {};
  const param = {
    page: page || 1,
    ...Locationparam,
  };
  const { data, loading } = useCompanyWishlist({ param });
  console.log("data", data);
  const handleChange = (page) => {
    Router.push(`/mycompany?page=${page}`, undefined, {
      scroll: false,
    });
  };
  return (
    <MyCompanyStyled>
      <div className="header">
        <TitleBoxInfo text={"Công ty của tôi "} />
      </div>
      {!loading ? (
        <>
          {data.data.map((company) => {
            const fullAddress = formatFullAddress(
              company.company_info.address,
              company.company_info.street_info?.name,
              company.company_info.ward_info?.name,
              company.company_info.district_info?.name,
              company.company_info.province_info?.name
            );
            return (
              <div className="item" key={company.id}>
                <CompanyItem
                  key={company.id}
                  id={company.id}
                  name={company.company_info.name}
                  fullAddress={fullAddress}
                  description={company.company_info.description}
                  email={company.company_info.email}
                  website={company.company_info.website}
                  phones={
                    company.company_info.phones || company.phone.split(" ")
                  }
                  updatedAt={company.updated_at}
                  isOnlineStatus={!!company.company_info.status}
                  mobiles={company.company_info.mobiles || []}
                  geo={company.company_info.geo}
                  slug={company.company_info.slug}
                  hideBoxlater={true}
                  user_id={company.company_info.user_id}
                  enable={company.company_info.enable}
                />
              </div>
            );
          })}
        </>
      ) : (
        <Spin />
      )}
      {!loading && (
        <div className="boxCenter">
          <PaginationWrapper
            HideText={true}
            current={data.meta.current_page}
            defaultPageSize={+data.meta.per_page}
            total={data.meta.total}
            onChange={handleChange}
          />
        </div>
      )}
    </MyCompanyStyled>
  );
}

export default MyCompany;
