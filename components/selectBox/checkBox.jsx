import React, { useEffect, useRef } from "react";
import { Checkbox, Col, Row } from "antd";
import { CheckBoxGroupTheme } from "../customizeTheme/checkBox";
import styled from "styled-components";

const CheckBoxStyles = styled.div`
  .CheckboxItem {
    padding: 0px 10px;
  }
`;
function CheckBox({ data, onChange, setIsVisible, select }) {
  const CheckBoxRef = useRef(null);
  useEffect(() => {
    function handleClickOutside(event) {
      if (CheckBoxRef.current && !CheckBoxRef.current.contains(event.target)) {
        setIsVisible(false);
      }
    }
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [CheckBoxRef]); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <CheckBoxStyles className="CheckBox" ref={CheckBoxRef}>
      <CheckBoxGroupTheme onChange={onChange} value={select}>
        <Row>
          {data.map((companytype) => (
            <Col
              lg={{ span: 24 }}
              md={{ span: 24 }}
              sm={{ span: 24 }}
              xs={{ span: 24 }}
              key={companytype.id}
              className="CheckboxItem"
            >
              <Checkbox value={companytype.id}>{companytype.name}</Checkbox>
            </Col>
          ))}
        </Row>
      </CheckBoxGroupTheme>
    </CheckBoxStyles>
  );
}

export default CheckBox;
