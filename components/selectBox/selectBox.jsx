import React, { useState } from "react";
import styled from "styled-components";
import CheckBox from "./checkBox";

const SelectBoxStyles = styled.div`
  width: 100%;
  position: relative;
  .CheckBox {
    position: absolute;
    top: 38px;
    right: 0px;
    min-height: 200px;
    background: #fff;
    width: 575px;
    z-index: 99;
    padding: 20px 0px;
    border: 1px solid ;
  }
  @media only screen and (max-width: 576px) {
    .CheckBox {
      right: unset;
      left: 0px;
      width: 95vw;
    }
  }
`;
function SelectBox(props) {
  const [isVisible, setIsVisible] = useState(false);
  return (
    <SelectBoxStyles>
      <div
        onClick={() => setIsVisible(true)}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          border: "1px solid #ECECEC",
          borderRadius: 4,
          height: 34,
          minWidth: 180,
          cursor: "pointer",
        }}
      >
        <span style={{ marginLeft: 4 }}>
          {props.select.length > 0
            ? `Lọc loại [${props.select.length}]`
            : "Loại hình và Công ty"}
        </span>
      </div>
      {isVisible && <CheckBox {...props} setIsVisible={setIsVisible} />}
    </SelectBoxStyles>
  );
}

export default SelectBox;
