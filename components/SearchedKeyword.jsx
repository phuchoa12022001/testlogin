import styled from 'styled-components';
import { Typography } from 'antd';

const SearchedKeywordContainer = styled.div`
  margin-bottom: 20px;

  span.ant-typography {
    color: ${({ theme }) => theme.colors.primary} !important;
  }
  
`;

export default function SearchedKeyword({ total, keyword }) {
  return (
    <SearchedKeywordContainer>
      <Typography.Paragraph>
        <Typography.Text>{total ?? 0}</Typography.Text> kết quả tìm kiếm phù hợp
        với <Typography.Text>“{keyword}”</Typography.Text>
      </Typography.Paragraph>
    </SearchedKeywordContainer>
  );
}
