import React from "react";
import styled from "styled-components";
import { SuccessIcon } from "../assets/icons";
import Link from "next/link";

const RegisterSuccessStyled = styled.div`
  margin: auto;
  display: block;
  text-align: center;
  max-width: 550px;
  .title {
    font-weight: 700;
    font-size: 28px;
    line-height: 34px;
    color: ${({ theme }) => theme.colors.primary};
    width: 100%;
    margin-bottom: 20px;
  }
  .icon {
    margin-bottom: 20px;
  }
  .text {
    text-align: left;
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
    margin-bottom: 20px;
    a {
      color: ${({ theme }) => theme.colors.primary};
    }
  }
`;

function RegisterSuccess() {
  return (
    <RegisterSuccessStyled>
      <h3 className="title">TẠO TÀI KHOẢN THÀNH CÔNG</h3>
      <div className="icon">
        <SuccessIcon />
      </div>
      <p className="text">
        Chúc mừng bạn đã đăng ký thành công. Vui lòng kiểm tra email để lấy mã
        kích hoạt &amp; sử dung
      </p>
      <p className="text">
        Trong thời gian 02 ngày, sau ngày này bạn chưa kích hoạt thì tài khoản
        sẽ bị hủy. Vui lòng
        <Link href="/">
          <a>	&quot;Kích hoạt tài khoản	&quot;</a>
        </Link>{" "}
        ngay
      </p>
      <p className="text">Chân thành cảm ơn!</p>
    </RegisterSuccessStyled>
  );
}

export default RegisterSuccess;
