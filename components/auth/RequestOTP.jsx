import Link from 'next/link';
import { Form, Input, Space, Typography } from 'antd';
import { PATH_NAME } from '../../constants/routes';
import { ButtonStyles } from '../styles';

export const RequestOTP = (props) => {
  return (
    <>
      <div style={{ marginBottom: 14 }}>
        <Typography.Title level={3} className='title'>
          ĐĂNG KÝ TÀI KHOẢN
        </Typography.Title>
        <Typography.Paragraph>Đăng ký bằng</Typography.Paragraph>

        <Space size={12} style={{ margin: '4px 0' }}>
          <ButtonStyles type='blue'>FACEBOOK</ButtonStyles>
          <ButtonStyles type='red'>GOOGLE</ButtonStyles>
        </Space>

        <Typography.Paragraph>
          Hoặc sử dụng tài khoản của bạn
        </Typography.Paragraph>
      </div>

      <Form layout='vertical' {...props}>
        <Form.Item
          name='email'
          rules={[
            { type: 'email', message: 'Email không hợp lệ.' },
            { required: true, message: 'Bắt buộc' },
          ]}
        >
          <Input placeholder='Email *' />
        </Form.Item>

        <Form.Item className="formStyle">
          <ButtonStyles type='green' htmlType='submit'>
            ĐĂNG KÝ
          </ButtonStyles>
        </Form.Item>
      </Form>

      <Typography.Paragraph style={{ marginTop: 30 }}>
        Bạn đã có tài khoản?{' '}
        <Link href={PATH_NAME.SIGN_IN}>
          <a className='link'>Đăng nhập ngay</a>
        </Link>
      </Typography.Paragraph>
    </>
  );
};
