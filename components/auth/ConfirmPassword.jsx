import Link from 'next/link';
import { Form, Input, Space, Typography, Button } from 'antd';
import { PATH_NAME } from '../../constants/routes';
import { ButtonStyles } from '../styles';

export const ConfirmPassword = (props) => {
  return (
    <>
      <div style={{ marginBottom: 14 }}>
        <Typography.Title level={3} className='title'>
          Xác nhận mật khẩu
        </Typography.Title>

        <Typography.Paragraph>
          Vui lòng nhập mật khẩu để hoàn thành đăng ký
        </Typography.Paragraph>
      </div>

      <Form layout='vertical' {...props}>
        <Form.Item
          name='password'
          rules={[{ required: true, message: 'Bắt buộc' }]}
        >
          <Input.Password placeholder='Mật khẩu *' />
        </Form.Item>

        <Form.Item
          name='confirm_password'
          rules={[
            {
              required: true,
              message: 'Bắt buộc',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error('Hai mật khẩu đã nhập không khớp.')
                );
              },
            }),
          ]}
        >
          <Input.Password placeholder='Xác nhận Mật khẩu *' />
        </Form.Item>

        <Form.Item className="formStyle">
          <ButtonStyles type='green' htmlType='submit'>
            Xác nhận
          </ButtonStyles>
        </Form.Item>
      </Form>
    </>
  );
};
