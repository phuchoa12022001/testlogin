import Link from 'next/link';
import { Form, Input, Space, Typography, Button } from 'antd';
import { PATH_NAME } from '../../constants/routes';
import { ButtonStyles } from '../styles';

export const VerifyOTP = (props) => {
  return (
    <>
      <div style={{ marginBottom: 14 }}>
        <Typography.Title level={3} className='title'>
          Nhập OTP
        </Typography.Title>
        <Typography.Paragraph>
          Vui lòng kiểm tra email để lấy mã OTP
        </Typography.Paragraph>
      </div>

      <Form layout='vertical' {...props}>
        <Form.Item name='otp' rules={[{ required: true, message: 'Bắt buộc' }]}>
          <Input placeholder='Nhập OTP *' />
        </Form.Item>

        <Form.Item className="formStyle">
          <ButtonStyles type='green' htmlType='submit'>
            Xác nhận
          </ButtonStyles>
        </Form.Item>
      </Form>

      <Typography.Paragraph style={{ marginTop: 30 }}>
        Bạn chưa nhận được OTP? <a className='link'>Gửi lại</a>
      </Typography.Paragraph>
    </>
  );
};
