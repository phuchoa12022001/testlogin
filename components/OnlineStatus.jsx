import PropTypes from "prop-types";
import styled, { useTheme } from "styled-components";
import { Badge } from "antd";

const OnlineStatusContainer = styled.div`
  .ant-badge-status-dot {
    width: 12px;
    height: 12px;
  }

  .ant-badge-status-text {
    margin-left: 4px;
    color: ${({ isOnlineStatus, theme }) =>
      isOnlineStatus ? theme.colors.green : theme.colors.black82};
  }
  .ant-badge-status-text {
    font-weight: 500;
    font-size: 10px;
    line-height: 12px;
    font-style: italic;
  }
`;

export default function OnlineStatus({ isOnlineStatus }) {
  const theme = useTheme();

  return (
    <OnlineStatusContainer isOnlineStatus={isOnlineStatus}>
      <Badge
        color={isOnlineStatus ? theme.colors.green : theme.colors.black82}
        text={isOnlineStatus ? "Đang Hoạt động" : "Offline"}
      />
    </OnlineStatusContainer>
  );
}

OnlineStatus.defaultProps = {
  isOnlineStatus: false,
};

OnlineStatus.propTypes = {
  isOnlineStatus: PropTypes.bool,
};
