import React from "react";
import { Slider } from "antd";
import styled from "styled-components";

const SliderStyles = styled(Slider)`
  .ant-slider-handle {
    border: solid 2px ${({ theme }) => theme.colors.primary} !important;
  }
  .ant-slider-track {
    background-color: ${({ theme }) => theme.colors.primary} !important;
  }
  .ant-slider-dot-active {
    border-color : ${({ theme }) => theme.colors.primary} !important;
  }
  .ant-slider:hover .ant-slider-handle:not(.ant-tooltip-open){
    border-color : ${({ theme }) => theme.colors.primary} !important;
  }
`;

function SliderTheme(props ) {
  return <SliderStyles {...props}  />;
}

export default SliderTheme;
