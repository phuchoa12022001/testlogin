import React from "react";
import { Checkbox } from "antd";
import styled from "styled-components";

const CheckBoxGroup = styled(Checkbox.Group)`
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${({ theme }) => theme.colors.primary};
    border-color: ${({ theme }) => theme.colors.primary};
  }
  .ant-checkbox-checked::after {
    border-color: ${({ theme }) => theme.colors.primary};
  }
  .ant-checkbox-inner {
    border-color: currentColor !important;
  }
  .ant-checkbox:hover .ant-checkbox-inner {
    border-color: ${({ theme }) => theme.colors.primary} !important;
  }
`;

export const CheckBoxGroupTheme = (props) => {
  return <CheckBoxGroup {...props}>{props.children}</CheckBoxGroup>;
};
const CheckBox = styled(Checkbox)`
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${({ theme }) => theme.colors.primary};
    border-color: ${({ theme }) => theme.colors.primary};
  }
  .ant-checkbox-checked::after {
    border-color: ${({ theme }) => theme.colors.primary};
  }
  .ant-checkbox-inner {
    border-color: currentColor !important;
  }
  .ant-checkbox:hover {
    color: ${({ theme }) => theme.colors.primary} !important;
  }
`;
function CheckBoxTheme(props) {
  return <CheckBox {...props}>{props.children}</CheckBox>;
}

export default CheckBoxTheme;
