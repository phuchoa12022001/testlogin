import React from "react";
import { Button } from "antd";
import styled, { css } from "styled-components";

const ButtonStyle = styled(Button)`
  ${(props) =>
    props.type === "primary" &&
    css`
      border-color: ${props.theme.colors.primary};
      background: ${props.theme.colors.primary};
      :hover,
      :focus {
        border-color: ${props.theme.colors.primary};
        background: ${props.theme.colors.primary};
      }
    `}
  :hover , :focus {
    ${(props) => props.isCss && css``}
    ${(props) =>
      !props.isCss &&
      css`
        color: ${({ theme }) => theme.colors.primary};
        border-color: ${({ theme }) => theme.colors.primary};
      `}
  }
`;

//  Dùng được cho type  : primary ghost dashed link text default danger

function ButtonTheme(props) {
  return (
    <ButtonStyle {...props} isCss={props.type === "primary" || props.danger}>
      {props.children}
    </ButtonStyle>
  );
}

export default ButtonTheme;
