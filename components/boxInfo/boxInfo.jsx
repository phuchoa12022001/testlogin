import React from "react";
import Link from "next/link";
import { DownOutlined, LogoutOutlined } from "@ant-design/icons";
import styled from "styled-components";
import User from "../Post/User";
import Follow from "../follows/follow";
import TitleFolder from "../title/TitleFolder";
import PostHot from "../Post/PostHot";
import TagPost from "../Post/Tags";
import Setting from "./setting";
import {
  YearIcon,
  SexIcon,
  HomeIcon,
  ArrawRightIcon,
} from "../../assets/icons";
import { useAuth } from "../../hooks/useAuth";

const BoxInfoContainer = styled.div`
  .boxInfos {
    max-width: 308px;
    flex: 1;
  }
  .boxInfos_wapper {
  }
  .boxuser {
    margin-top: 20px;
    padding-bottom: 12px;
    border-bottom: 1px solid ${({ theme }) => theme.colors.grayEC};

    .ant-comment-avatar {
      span {
        border: 2px solid #007575 !important;
        border-radius: 50%;
      }
    }
  }
  .box {
    padding-left: 14px;
    border-bottom: 1px solid ${({ theme }) => theme.colors.grayEC};
    list-style-type: none;
  }
  .boxInfo {
    margin-top: 11px;
  }
  .list__about {
    margin-top: 13px;
  }
  .about__text {
    font-weight: 700;
    font-size: 16px;
    line-height: 19px;
    color: #191919;
  }
  .list__about li {
    display: flex;
    margin-bottom: 12px;
    align-items: center;
    span {
      color: ${({ theme }) => theme.colors.gray66};
      margin-right: 3px;
      span {
        color: ${({ theme }) => theme.colors.black};
        line-height: 20px;
      }
    }
  }
  .list__about li a {
    font-weight: 400;
    font-size: 14px;
    line-height: 15px;
  }
  .home-icon {
    flex-shrink: 0;
    color: ${({ theme }) => theme.colors.primary};
    margin-right: 8px;
  }
  .about__icon {
    color: ${({ theme }) => theme.colors.primary};
    margin-right: 8px;
    width: 14px;
  }
  .icon-home {
    width: 40px !important;
  }
  li.Boxinfo_title {
    padding: 10px 0;
    border-bottom: 1px solid #FBFBFB;
    border-top: 1px solid #FBFBFB;
    margin-bottom: unset;
    a {
      color: ${({ theme }) => theme.colors.black};
    }
  }
  li.Boxinfo_title:last-child {
    margin-bottom: unset;
  }
  .box.boxFolder {
      margin-top: unset;
    }
  .follows {
    margin-top: 23px;
    border-bottom: 1px solid ${({ theme }) => theme.colors.grayEC};
  }
  .follows__list {
    margin-top: 7px;
    width: 195px;
    .follows__list_wrapper {
      .followBottom:nth-last-child(1) {
        margin-bottom: 0px;
      }
    }
  }
  .follows__list__end {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-top: 2px;
    p {
      width: 165px;
      font-weight: 400;
      font-size: 14px;
      line-height: 15px;
    }
    cursor: pointer;
    margin-bottom: 15px;
  }
  .follows__list__end:hover {
    color: ${({ theme }) => theme.colors.primary};
  }
  .PostHot {
    margin-top: 13px;
  }
  .PostHot_list {
    margin-top: 20px;
  }
  .PostHot-more {
    margin-top: 15px;
    display: flex;
    padding-bottom: 10px;
    align-items: center;
    p {
      margin-right: 11px;
      cursor: pointer;
    }
  }
  .PostHot-more:hover {
    color: ${({ theme }) => theme.colors.primary};
  }
  .Tags {
    margin-top: 11px;
  }
  .Tags__list {
    margin-top: 12px;
    display: flex;
    display: flex;
    flex-wrap: wrap;
    p {
      font-weight: 600;
      font-size: 12px;
      border-radius: 36px;
      margin: 9px 0px;
      margin-right: 14px;
      line-height: 12px;
      padding: 5px 10px;
      border: 1px solid ${({ theme }) => theme.colors.gray66};
    }
  }
  @media only screen and (min-width: 1200px) {
    // svg.home-icon{
    //   width:28px;
    //   margin-right:10px;
    // }
  }

  @media only screen and (max-width: 959px) {
    /* .follows__list {
      margin-top: 10px;
    }
    .follows__list__end {
      margin-top: 4px;
    } */
    .Boxinfo_title {
      margin-top: unset !important;
    }
    .box.boxFolder {
      margin-top: unset;
    }
    .box {
      padding-left: 0px;
    }
  }
  @media only screen and (min-width: 1200px) {
    svg.home-icon {
      margin-right: 8px;
    }
  }
`;
export default function BoxInfos(props) {
  const { profile } = useAuth({ revalidateOnMount: true });
  if (!profile) return false;
  const birthday = profile?.date_of_birth?.slice(0, 10);
  const day = birthday?.slice(8, 10);
  const month = birthday?.slice(5, 7);
  const year = birthday?.slice(0, 4);
  const birthDaynew = day + "/" + month + "/" + year;

  return (
    <BoxInfoContainer>
      <>
        <div className="boxInfos_wapper">
          <div className="boxuser">
            <User
              img={profile.avatar || ""}
              name={profile.name || profile.email}
              nameUrl="/"
              subname="Xem trang ca nhan cua ban"
              subLink="/"
              className="user"
              sizeAvatar={48}
              color="#007575"
              size="large"
            />
          </div>
          <div className="box boxInfo">
            <h3 className="about__text">Về tôi</h3>
            <ul className="list__about">
              <li>
                {/* <LogoutOutlined /> */}
                <HomeIcon className="home-icon" />
                <Link href="/">
                  <a>
                    <span>
                      Sống tại:{" "}
                      <span>{profile.address ? profile.address : "Chưa cập nhật"}</span>
                    </span>
                  </a>
                </Link>
              </li>
              <li>
                <YearIcon className="about__icon" />
                <Link href="/">
                  <a>
                    <span>
                      Sinh vào:{" "}
                      <span>{profile.date_of_birth ? birthDaynew : "Chưa cập nhật"}{" "}</span>
                    </span>
                    <h2></h2>
                  </a>
                </Link>
              </li>
              <li>
                <SexIcon className="about__icon" />
                <Link href="/">
                  <a>
                    <span>Giới tính:{" "}
                      <span>{profile.sex === 1
                        ? "Nam"
                        : profile.sex === 0
                        ? "Nữ"
                        : profile.sex === null
                        ? "Chưa cập nhật"
                        : ""}
                        </span>
                      </span>
                  </a>
                </Link>
              </li>
            </ul>
          </div>
          <Setting />
          <div className="follows">
            <div className="box">
              <TitleFolder text={"Bạn quan tâm?"} icon />
              <div className={"wapper"}>
                <div className="follows__list">
                  <div className="follows__list_wrapper">
                    <Follow />
                    <Follow />
                  </div>
                  <Link href="/">
                    <div className="follows__list__end">
                      <p>Xem thêm tất cả thông tin</p>
                      <ArrawRightIcon />
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="PostHot">
            <div className="box">
              <TitleFolder text={"Tin nóng!"} icon />
              <div className="PostHot_list">
                <PostHot />
                <PostHot />
                <PostHot />
              </div>
              <div className="PostHot-more">
                <p>Xem thêm</p>
                <DownOutlined style={{ fontSize: 7 }} />
              </div>
            </div>
          </div>
          <div className="Tags">
            <div className="box">
              <TitleFolder text={"Giành cho bạn"} icon={false} />
              <div className="Tags__list">
                <TagPost />
                <TagPost />
                <TagPost />
                <TagPost />
                <TagPost />
                <TagPost />
                <TagPost />
                <TagPost />
              </div>
            </div>
          </div>
        </div>
      </>
    </BoxInfoContainer>
  );
}
