/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */

import React from "react";
import { UserOutlined, HeartOutlined } from "@ant-design/icons";
import Link from "next/link";
import styled from "styled-components";
import { useRouter } from "next/router";

import { TimeIcon, DocIcon, StarIcon2 } from "../../assets/icons";

const SettingItem = [
  {
    title: "Tài khoản cá nhân",
    icon: <UserOutlined />,
    path: "/editprofile",
  },
  {
    title: "Quản lý công ty",
    icon: <img src="./static/icon/companyIcon.svg" />,
    path: "/companymanagement",
  },
  {
    title: "Mục xem sau",
    icon: <TimeIcon />,
    path: "/mycompany",
  },
  {
    title: "Mục quan tâm",
    icon: <HeartOutlined />,
    path: "/follow",
  },
  {
    title: "Mục đánh giá",
    icon: <StarIcon2 />,
    path: "/evaluate",
  },
];

const SettingStyle = styled.div`
  .boxFolder {
    margin-top: 12px;
  }
  .Boxinfo_title {
    display: flex;
    align-items: center;
    margin-bottom: 21px;
  }
  .Boxinfo_titleIcon {
    font-weight: 400;
    font-size: 14px;
    line-height: 15px;
    width: 24px;
  }
  .Boxinfo_title.active,
  .Boxinfo_title:hover {
    color: ${({ theme }) => theme.colors.primary};
    a {
      color: ${({ theme }) => theme.colors.primary};
    }
    a:hover {
      color: unset !important;
    }
    path {
      stroke: transparent;
    }
  }
  .evaluate {
    transform: translateX(-3px);
    svg {
      color: transparent;
    }
  }
  .Boxinfo_title.active .evaluate,
  .Boxinfo_title:hover .evaluate {
    path {
      stroke: ${({ theme }) => theme.colors.primary} !important;
    }
  }
  .Boxinfo_title:last-child {
    margin-bottom: 10px;
  }
  @media only screen and (max-width: 959px) {
    .box.boxFolder {
      padding: 0px;
    }
    .Boxinfo_title {
      margin-top: 13px;
      border-bottom: 1px solid #333;
      padding: 0px 14px 9px 14px;
      padding-left: 0px;
    }
    .Boxinfo_title:nth-child(6) {
      border-bottom: none;
      padding-bottom: 7px;
      margin-bottom: 0px;
    }
  }
`;
function Setting() {
  const { pathname } = useRouter();
  return (
    <SettingStyle>
      <div className="box boxFolder">
        <ul className="listFolder">
          {SettingItem.map((item, index) => (
            <li
              key={index}
              className={`Boxinfo_title ${
                pathname === item.path ? "active" : ""
              } `}
            >
              <div
                className={`Boxinfo_titleIcon ${
                  "/evaluate" === item.path ? "evaluate" : ""
                } `}
              >
                {item.icon}
              </div>
              <Link href={item.path}>
                <a>{item.title}</a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </SettingStyle>
  );
}

export default Setting;
