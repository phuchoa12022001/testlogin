/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/display-name */

import React, { useState } from "react";
import styled from "styled-components";
import { UserOutlined } from "@ant-design/icons";

import ChangeTime from "../ChangeTime";
import CreateProtest from "./CreateProtest";
import ListProtest from "./listProtest";

import User from "../Post/User";
import ImageComponent from "../image/Image";

const CommentContainers = styled.div`
  display: flex;
  flex-wrap: wrap;
  .Avatar {
    height: 57px;
    justify-content: center;
    align-items: center;
    display: flex;
  }
  .Avatar img {
    border-radius: 50%;
  }
  .boxComment {
    flex: 1;
    margin-left: 11px;
  }
  .wrapper {
    padding: 0px 16px;
    width: 100%;
    padding-top: 17px !important;
  }
  .info {
    display: flex;
    justify-content: space-between;
    width: 100%;
    .time {
      font-size: 12px;
      line-height: 12px;
      font-weight: 400;
    }
  }
  .boxText {
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    .user {
      color: ${({ theme }) => theme.colors.primary};
      cursor: pointer;
    }
    .moreComment {
      font-size: 14px;
      line-height: 15px;
      font-weight: 400;
      cursor: pointer;
    }
    .moreComment:hover {
      color: ${({ theme }) => theme.colors.primary};
    }
  }
  .text {
    display: block;
    font-size: 14px;
    line-height: 15px;
    font-weight: 400;
    word-wrap: break-word;
    width: 375px;
    margin-top: 10px;
  }
  @media only screen and (max-width: 959px) {
    .info {
      flex-wrap: wrap;
    }
    .wrapper {
      padding-top: 7px !important;
    }
    .boxText {
      flex-wrap: wrap;
      justify-content: flex-end;
      .moreComment {
        font-weight: 500;
        font-size: 10px;
        line-height: 12px;
      }
    }
    .text {
      width: 360px;
      font-weight: 500;
      word-wrap: break-word;
      font-size: 10px;
      line-height: 12px;
    }
    .wrapper {
      padding: 0px;
      padding-left: 16px;
    }
    .boxComment {
      width: 80%;
    }
    .info {
      .time {
        font-size: 10px;
        line-height: 12px;
        width: 100%;
        justify-content: flex-end;
        display: flex;
        order: -1;
      }
    }
    justify-content: space-between;
  }
  .protest {
    font-weight: 500;
    font-size: 12px;
    line-height: 15px;
    cursor: pointer;
    margin-top: 10px;
    color: ${({ theme }) => theme.colors.primary};
    text-align: left;
  }
  @media only screen and (min-width: 540px) and (max-width: 719px) {
    .text,
    .protest {
      width: 100%;
      word-wrap: break-word;
      font-weight: 500;
      font-size: 10px;
      line-height: 12px;
    }
  }
  @media only screen and (min-width: 720px) and (max-width: 959px) {
    .text,
    .protest {
      flex: 1;
      width: auto;
      word-wrap: break-word;
      font-weight: 500;
      font-size: 10px;
      line-height: 12px;
    }
  }
`;
function CommentProtest({
  id,
  AvatarUser = "",
  comment,
  idBlog,
  parentId,
  ...props
}) {
  const [createComment, setCreateComment] = useState();
  return (
    <CommentContainers>
      <div className="Avatar">
        {props.avatar ? (
          <ImageComponent src={props.avatar} alt={props.name} size={20} />
        ) : (
          <UserOutlined style={{ fontSize: 20 }} />
        )}
      </div>
      <div className="boxComment">
        <div className="wrapper">
          <div className="info">
            <User
              img={"hide"}
              name={props.name || props.email}
              nameUrl="/"
              subname=""
              subLink="/"
              className="user"
              appellation={"1st"}
              sizeAvatar={40}
            />
            <p className="time">
            <ChangeTime datetime={props.created_at} locale="vi"  />
            </p>
          </div>
          <div className="boxText">
            <p className="text">{comment}</p>
          </div>
          {createComment ? (
            <CreateProtest id={id} idBlog={idBlog} parentId={parentId} />
          ) : (
            <div className="protest" onClick={() => setCreateComment(true)}>
              <p>Phản hồi</p>
            </div>
          )}
        </div>
      </div>
      {props?.children?.length && (
        <ListProtest child={props.children} idBlog={idBlog} />
      )}
    </CommentContainers>
  );
}

export default CommentProtest;
