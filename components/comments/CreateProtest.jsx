/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/display-name */
import React, { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import Image from "next/image";
import { Input, Form } from "antd";
import { useSWRConfig } from "swr";
import { useAuth } from "../../hooks/useAuth";
import { UserOutlined, SendOutlined } from "@ant-design/icons";

import { useComments } from "../../hooks/useComment";

const { TextArea } = Input;

const TextAreaComponents = styled(TextArea)`
  padding: 0px !important ;
  padding-left: 13px !important;
  min-height: unset !important;
  background: transparent;
  font-weight: 400;
  font-size: 12px;
  line-height: 15px !important;
  width: 90%;
  border: none;
  outline: none;
  height: 19px;
  flex: 1;
  :focus {
    border: none;
    outline: none;
  }
`;

const CommentContainer = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 13px;
  margin-top: 10px;
  .CreateComment {
    justify-content: center;
    align-items: center;
  }
  .CreateComment__input {
    margin-left: 11px;
    border-radius: 100px;
    background: ${({ theme }) => theme.colors.blackF2};
    display: flex;
    align-items: center;
    width: 100%;
    .CreateComment__box {
      flex: 1;
      display: flex;
      height: 100%;
      align-items: center;
    }
    .boxIcon {
      cursor: pointer;
      display: flex;
      height: 100%;
      justify-content: flex-start;
      .icon {
        padding: 11px 12px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 20px;
      }
      .icon:hover {
        color: ${({ theme }) => theme.colors.primary};
      }
    }
    flex: 1;
  }
  .CreateComment__input.focus {
    flex-direction: column;
    border-radius: 20px !important;
    .CreateComment__box {
      width: 100%;
      padding: 10px 0px !important;
      textarea {
        padding: 0px 13px !important;
      }
    }
    .boxIcon {
      width: 100%;
      justify-content: flex-end;
    }
  }
  .Avatar img {
    border-radius: 50%;
  }
  @media only screen and (max-width: 576px) {
    .CreateComment__box {
      width: 90% !important;
    }
    .CreateComment__input.focus {
      .CreateComment__box {
        textarea {
          padding: 0px 5px !important;
        }
      }
    }
  }
`;
function CreateProtest({ id, idBlog, parentId }) {
  const { mutate } = useSWRConfig();
  const TextAreaRef = useRef(null);
  const { profile } = useAuth({});
  const { add } = useComments({
    params: {
      id: idBlog,
      page: 1,
    },
  });
  const form = useRef();
  const [text, setText] = useState("");
  const [line, setLine] = useState(false);
  const [start, setStart] = useState(false);
  const [focus, setFocus] = useState(false);
  const handleChange = (e) => {
    setText(e.target.value);
  };
  useEffect(() => {
    if (TextAreaRef.current) {
      if (
        TextAreaRef.current.resizableTextArea.textArea.offsetHeight > 19 &&
        start
      ) {
        setLine(true);
      } else {
        setStart(true);
        setLine(false);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [TextAreaRef, text, focus]);
  const handleBlur = (e) => {
    setFocus(false);
  };
  const onFinish = async () => {
    const NewData = {
      blog_id: idBlog,
      comment: text,
      parent_id: parentId,
    };
    await add(NewData);
    await mutate(`/blog-comment?blog_id=${idBlog}&page=${1}`);
    setText("");
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    form.current.submit();
  };
  const ImageRef = React.forwardRef((props, ref) => (
    <Image {...props} ref={ref} />
  ));
  return (
    <Form name="form" onFinish={onFinish} ref={form}>
      <CommentContainer>
        <div className="CreateComment">
          <div className="Avatar">
            {profile ? (
              <ImageRef
                src={profile.avatar}
                alt={profile.name}
                width={20}
                height={20}
              />
            ) : (
              <UserOutlined style={{ fontSize: 20 }} />
            )}
          </div>
          <div
            className={`CreateComment__input ${focus || line ? "focus" : ""}`}
          >
            <div className="CreateComment__box">
              <TextAreaComponents
                ref={TextAreaRef}
                placeholder="Viết 1 nhận xét"
                autoSize
                bordered={false}
                onFocus={() => setFocus(true)}
                onBlur={handleBlur}
                value={text}
                onChange={handleChange}
              />
            </div>
            <div className="boxIcon">
              <div className="icon" onMouseDown={handleSubmit}>
                <SendOutlined />
              </div>
            </div>
          </div>
        </div>
      </CommentContainer>
    </Form>
  );
}

export default CreateProtest;
