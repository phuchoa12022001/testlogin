import React, { useState } from "react";
import CommentProtest from "./CommentProtest";
function ListProtest({ child, idBlog, id }) {
  const [open, setOpen] = useState(false);
  return (
    <div>
      {open || child?.length <= 1 ? (
        child.map((protest) => (
          <CommentProtest
            {...protest}
            key={protest.id}
            idBlog={idBlog}
            parentId={id}
          />
        ))
      ) : (
        <>
          <CommentProtest
            {...child[0]}
            idBlog={idBlog}
            key={child[0].id}
            parentId={id}
          />
          <p className="more" onClick={() => setOpen(true)}>
            Xem thêm {child.length - 1} bình luận
          </p>
        </>
      )}
    </div>
  );
}

export default ListProtest;
