import React, { useState } from "react";
import styled from "styled-components";

import CreateComment from "./CreateComment";
import Comment from "./Comment";
import { useComments } from "../../hooks/useComment";

const ListContainer = styled.div`
  width: 100%;
  height: 100%;
  .more {
    cursor: pointer;
    margin-top: 10px;
  }
`;
function ListComment({ openComment, AvatarUser, id }) {
  const [page, setPage] = useState(1);
  const [open, setOpen] = useState(false);
  const { data, loading } = useComments({
    params: {
      id,
      page: page || 1,
    },
  });
  console.log("data" , data);
  return (
    <>
      {openComment && (
        <ListContainer>
          <CreateComment AvatarUser={AvatarUser} id={id} />
          {!loading && data?.data ? (
            open || data?.data?.length <= 1 ? (
              data.data.map((comment) => (
                <Comment
                  {...comment}
                  AvatarUser={AvatarUser}
                  idBlog={id}
                  key={comment.id}
                />
              ))
            ) : (
              <>
                <Comment
                  {...data.data[0]}
                  AvatarUser={AvatarUser}
                  idBlog={id}
                  key={data.data[0].id}
                />
                <p className="more" onClick={() => setOpen(true)}>
                  Xem thêm {data.data.length - 1} bình luận
                </p>
              </>
            )
          ) : (
            false
          )}
        </ListContainer>
      )}
    </>
  );
}

export default ListComment;
