import React, { forwardRef } from "react";
import { Form } from "antd";
import ReaptchaComponents from "react-google-recaptcha";

const Recaptcha = forwardRef(({ onChange }, ref) => {
  return (
    <>
      <Form.Item name="recaptcha">
        <div className="reCAPTCHA">
          <ReaptchaComponents
            ref={ref}
            sitekey={process.env.NEXT_PUBLIC_SITE_KEY}
            hl="vi"
            onChange={onChange}
          />
        </div>
      </Form.Item>
    </>
  );
});
Recaptcha.displayName = "Recaptcha";

export default Recaptcha;
