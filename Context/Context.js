import { useState, createContext, useEffect } from "react";

import { useTrackLocation } from "../hooks/useTrackLocation";
import Geocode from "../helpers/configGeocode";
export const ThemeContext = createContext();

function Context({ children }) {
  const [history, setHistory] = useState({
    lat: null,
    long: null,
    address: "",
    LocationId: null,
  });
  const [location, setLocation] = useState({
    lat: null,
    long: null,
    address: "",
    LocationId: null,
  });
  const [reCaptcha, setReCaptcha] = useState(true);
  const { handleTrackLocation } = useTrackLocation();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true);
    handleTrackLocation(
      (location) => {
        setLoading(false);
        setLocation(location);
        setHistory(location);
      },
      () => {
        setLoading(false);
      }
    );
  }, []);
  const ReloadHistory = () => {
    setLocation(history);
  };
  const CopyLocation = () => {
    setHistory(location);
  };
  useEffect(() => {
    if (!loading) {
      if (location.lat && location.long) {
        Geocode.fromLatLng(location.lat, location.long).then(
          (response) => {
            const address = response.results[0].formatted_address;
            setLocation((prev) => {
              return {
                address,
                ...prev,
              };
            });
            setHistory((prev) => {
              return {
                address,
                ...prev,
              };
            });
          },
          (error) => {
            console.error(error);
          }
        );
      } else {
        setLocation((prev) => {
          return {
            ...prev,
            address: "Bạn đã tắt vị trí",
            lat: null,
            long: null,
          };
        });
        setHistory((prev) => {
          return {
            ...prev,
            address: "Bạn đã tắt vị trí",
            lat: null,
            long: null,
          };
        });
      }
    }
  }, [loading]);
  return (
    <ThemeContext.Provider
      value={{
        location,
        setLocation,
        setHistory,
        ReloadHistory,
        loading,
        CopyLocation,
        reCaptcha,
        setReCaptcha,
      }}
    >
      {children}
    </ThemeContext.Provider>
  );
}

export default Context;
