function LineCount(elemt, lineHeight) {
    //  cận thận elemt chuyển vào undefined
    return !elemt ? 0 : elemt.offsetHeight / lineHeight;

}
export default LineCount;