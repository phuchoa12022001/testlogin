import styled from "styled-components";
import { useRouter } from "next/router";
import Link from "next/link";
import Image from "next/image";
import { Grid, Typography } from "antd";
import { useSession } from "next-auth/react";
import useSWR from "swr";

import { PATH_NAME } from "../../constants/routes";
import Search from "../../components/Search";
import { HamburgerMenuDesktop } from "./HamburgerMenuDesktop";
import { HamburgerMenuMobile } from "./HamburgerMenuMobile";
import { authApi } from "../../api-client/authApi";

const HeaderContainer = styled.header`
  background-color: ${({ theme }) => theme.colors.primary};
  /* padding: 23px 0; */
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 999;
  box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.15);

  .heading {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  .ant-avatar {
    background-color: ${({ theme }) => theme.colors.white};
  }

  .anticon-user {
    color: ${({ theme }) => theme.colors.black2E};
  }

  .link {
    color: ${({ theme }) => theme.colors.white};
  }

  .menuHamburger {
    padding: 5px;
    font-size: 24px;
    color: ${({ theme }) => theme.colors.white};
    cursor: pointer;
    &.color-green {
      color: ${({ theme }) => theme.colors.primary};
    }
  }

  .searchWrapper {
    flex-grow: 1;
    padding: 0 50px;
  }

  .collapse__menu {
    position: absolute;
    z-index: 99;
    top: calc(100% + 24px);
    right: 0;
    box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.15);
    border-radius: 8px;
    overflow: hidden;
    width: 216px;
    background-color: #fff;

    @media (max-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
      position: fixed;
      top: 0;
      right: 0;
      height: 100%;
      width: 186px;
      border-radius: 0;
      // z-index: 99;
    }
  }

  .ant-menu {
    font-size: ${({ theme }) => theme.fontSizes.mobile};
  }

  .menu__footer {
    background-color: #fff;
    padding: 6px 20px;
    color: #828282;
    border-top: 1px solid #ececec;
    font-size: ${({ theme }) => theme.fontSizes.sizeSM};

    ul {
      display: flex;
      align-items: center;
      justify-content: space-between;
      list-style: disc;
    }
  }
  .menu__user {
    padding: 20px 10px;

    @media (max-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
      .ant-space-item > span {
        width: 25px !important;
        height: 25px !important;
      }
    }
  }

  .menu__user-name {
    color: ${({ theme }) => theme.colors.primary};
    font-weight: 700;
    font-size: 12px;
  }
  .text {
    margin-left: 10px;
    margin-top: 10px;
  }
  .close__menu {
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #00000030;
    @media (min-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
      display: none;
    }
  }
  @media only screen and (max-width: 959px) {
    .searchWrapper {
      display: none;
    }
  }
`;

export default function Header() {
  const router = useRouter();
  const { lg } = Grid.useBreakpoint();
  const { data: session, status } = useSession();
  const { data: profile, error } = useSWR(
    session ? "/auth/profile" : null,
    authApi.getProfile
  );
  const loading =
    profile === undefined &&
    status !== "unauthenticated" &&
    error === undefined;
  return (
    <HeaderContainer>
      <div className="container">
        <div className="heading" style={{ position: "relative" }}>
          <Link href={PATH_NAME.ROOT}>
            <a
              style={{ display: "flex", alignItems: "center", color: "#fff" }}
              className="logo"
            >
              <Image
                src="/static/images/logo_02.png"
                alt=""
                width={59}
                height={42}
                objectFit="cover"
              />
              <Typography.Paragraph className="text">
                Tìm kiếm & lọc dữ liệu hiệu quả
              </Typography.Paragraph>
            </a>
          </Link>

          {router.pathname !== "/" && (
            <div className="searchWrapper">
              <Search />
            </div>
          )}

          {lg ? (
            <HamburgerMenuDesktop profile={profile} loading={loading} />
          ) : (
            <HamburgerMenuMobile profile={profile} loading={loading} />
          )}
        </div>
      </div>
    </HeaderContainer>
  );
}
