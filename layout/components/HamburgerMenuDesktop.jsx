import Link from "next/link";
import styled from "styled-components";
import { Space, Avatar, Menu, Dropdown, Typography } from "antd";
import {
  UserOutlined,
  SettingOutlined,
  HistoryOutlined,
  CarryOutOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/router";
import { signOut } from "next-auth/react";

import { PATH_NAME } from "../../constants/routes";
import { HamburgerIcon } from "../../assets/icons";

const MenuStyles = styled(Menu)`
  
`;

const HamburgerMenuDesktopContainer = styled.div`
  color: ${({ theme }) => theme.colors.white};

  .ant-typography {
    color: inherit;

    .welcome {
      font-size: 10px;
      font-weight: 500;
    }

    .name {
      font-weight: 700;
    }
  }

  .hamburger {
    font-size: 24px;
    cursor: pointer;
    padding: 27px 0;
  }
`;

export const HamburgerMenuDesktop = ({ profile, onLogout }) => {
  const router = useRouter();
  const handleLogout = async () => {
    try {
      await onLogout();
    } catch (error) {}
  };

  const menu = (
    <MenuStyles
      items={[
        {
          key: "1",
          label: "Cấu hình",
          icon: <SettingOutlined />,
        },
        {
          key: "2",
          label: "Lịch sử tìm kiếm",
          icon: <HistoryOutlined />,
        },
        ...(profile
          ? [
              {
                key: "3",
                label: "Hàng ngày",
                icon: <CarryOutOutlined />,
                children: [
                  {
                    key: "thong-tin-ca-nhan",
                    label: "Thông tin cá nhân",
                    onClick: () => {
                      router.push("/editprofile");
                    },
                  },
                  {
                    key: "tin-cua-ban",
                    label: "Tin của bạn",
                    onClick: () => {
                      router.push("/postblog");
                    },
                  },
                  { key: "theo-doi", label: "Theo dõi" },
                  { key: "phan-hoi", label: "Phản hồi" },
                  {
                    key: "logout",
                    label: "Đăng xuất",
                    onClick: () => signOut(),
                  },
                ],
              },
            ]
          : []),
        {
          type: "divider",
        },
        {
          label: (
            <div className="dropdown-footer">
              <div className="links">
                <Link href="/">
                  <a>Liên hệ</a>
                </Link>
                <Link href="/">
                  <a>Điều khoản</a>
                </Link>
                <Link href="/">
                  <a>DMCA</a>
                </Link>
              </div>
              <p>@2022 Tori</p>
            </div>
          ),
        },
      ]}
    />
  );

  return (
    <HamburgerMenuDesktopContainer>
      <Space>
        <Avatar
          size={40}
          src={profile?.avatar}
          icon={!profile?.avatar ? <UserOutlined /> : undefined}
        />

        {profile ? (
          <Typography.Paragraph>
            <p className="welcome">Xin chào</p>
            <p className="name">{profile?.name || profile?.email}</p>
          </Typography.Paragraph>
        ) : (
          <Link href={PATH_NAME.SIGN_IN}>
            <a className="link">Đăng nhập</a>
          </Link>
        )}

        <Dropdown
          overlay={menu}
          placement="bottomRight"
          overlayStyle={{ width: 186 }}
          overlayClassName="dropdownHamburger"
        >
          <HamburgerIcon className="hamburger" />
        </Dropdown>
      </Space>
    </HamburgerMenuDesktopContainer>
  );
};
