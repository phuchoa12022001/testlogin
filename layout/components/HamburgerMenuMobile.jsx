import {
  CarryOutOutlined,
  HistoryOutlined,
  SettingOutlined,
  UserOutlined,
  LoginOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/router";
import { signOut } from "next-auth/react";
import { Avatar, Drawer, Space, Typography, Menu } from "antd";
import { useState } from "react";
import styled from "styled-components";

import { HamburgerIcon } from "../../assets/icons";

const HamburgerMenuMobileContainer = styled.div`
  color: ${({ theme }) => theme.colors.white};

  .hamburger {
    font-size: 24px;
    cursor: pointer;
    padding: 27px 0;
  }
`;

export const HamburgerMenuMobile = ({ profile, loading }) => {
  const [isVisible, setIsVisible] = useState(false);
  const router = useRouter();
  const handleToggle = () => {
    setIsVisible((prev) => !prev);
  };
  const ButtonLogon = profile
    ? {
        key: "4",
        label: "Đăng xuất",
        icon: <LoginOutlined />,
        onClick: () => {
          signOut();
        },
      }
    : {
        key: "4",
        label: "Đăng Nhập",
        icon: <LoginOutlined />,
        onClick: () => {
          router.push("/login");
        },
      };
  return (
    <>
      <HamburgerMenuMobileContainer>
        <HamburgerIcon className="hamburger" onClick={handleToggle} />
      </HamburgerMenuMobileContainer>

      <Drawer
        visible={isVisible}
        onClose={handleToggle}
        width={186}
        closable={false}
        bodyStyle={{ paddingLeft: 8, paddingRight: 8 }}
        className="menuMobileDrawer"
      >
        <Space style={{ justifyContent: "center", marginBottom: 24 }}>
          <Avatar
            size={40}
            src={profile?.avatar}
            icon={!profile?.avatar ? <UserOutlined /> : undefined}
          />
          <Typography.Paragraph>
            <p className="welcome">Xin chào</p>
            {profile ? (
              <p className="name">{profile?.name || profile?.email}</p>
            ) : (
              <p className="name">Yune Hero</p>
            )}
          </Typography.Paragraph>
        </Space>
        {!loading && (
          <Menu
            mode="inline"
            items={[
              {
                key: "1",
                label: "Cấu hình",
                icon: <SettingOutlined />,
              },
              {
                key: "2",
                label: "Lịch sử tìm kiếm",
                icon: <HistoryOutlined />,
              },
              {
                key: "3",
                label: "Hàng ngày",
                icon: <CarryOutOutlined />,
                children: [
                  {
                    key: "thong-tin-ca-nhan",
                    label: "Thông tin cá nhân",
                    onClick: () => {
                      router.push("/editprofile");
                      handleToggle();
                    },
                  },
                  {
                    key: "tin-cua-ban",
                    label: "Tin của bạn",
                    onClick: () => {
                      router.push("/postblog");
                      handleToggle();
                    },
                  },
                  {
                    key: "theo-doi",
                    label: "Theo dõi",
                    onClick: () => {
                      handleToggle();
                    },
                  },
                  {
                    key: "phan-hoi",
                    label: "Phản hồi",
                    onClick: () => {
                      handleToggle();
                    },
                  },
                  ButtonLogon,
                ],
              },
            ]}
          />
        )}
      </Drawer>
    </>
  );
};
