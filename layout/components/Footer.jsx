import Link from 'next/link';
import styled from 'styled-components';
import { Space } from 'antd';
import { useFooter } from '../../hooks/useFooter';
const FooterContainer = styled.footer`
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.primary};
  line-height: 20px;
  padding: 15px 0;

  ul {
    display: flex;
    align-items: center;

    > li + li {
      margin-left: 20px;
    }

    a {
      padding: 12px;
    }
  }

  @media (max-width: 1199px) {
    .ant-space {
      justify-content: center !important;
    }
    .ant-space ul {
      flex-wrap: wrap;
      justify-content: center;
    }
    .ant-space ul li {
      margin-left: 0;
    }
  }

  @media (max-width: ${({ theme }) => theme.breakpoint.maxMobileSM}) {
    font-size: ${({ theme }) => theme.fontSizes.sizeSM};
  }
`;


export default function Footer() {
  const { data: FooterData } = useFooter({});
  const List = FooterData || [];  
  return (
    <FooterContainer>
      <div className='container'>
        <Space style={{ justifyContent: 'space-between', flexWrap: 'wrap' }}>
          <nav>
            <ul>
              {List.slice(0 , 4).map((item) => (
                <li key={item.id}>
                  <Link href={`/footer/${item.slug}`}>
                    <a>{item.name}</a>
                  </Link>
                </li>
              ))}
            </ul>
          </nav>

          <p style={{ textAlign: 'center', paddingRight: '12px' }}>&copy; 2022 xyz. All rights reserved | Design by Huy</p>
        </Space>
      </div>
    </FooterContainer>
  );
}
