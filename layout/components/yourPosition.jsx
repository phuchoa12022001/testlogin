import React, { useState, useContext } from "react";
import styled from "styled-components";
import { useMediaQuery } from "react-responsive";
import { useRouter } from "next/router";

import ModalLocation from "../../components/location/Modal";
import YourPositionStyles from "../../components/location/location";
import Search from "../../components/Search";
import { ThemeContext } from "../../Context/Context";


const YourPositionStyle = styled.div`
  .searchWrapper {
    display: none;
    margin-top: 95px;
  }
  @media only screen and (max-width: 959px) {
    .searchWrapper {
      display: block;
    }
  }
`;
function YourPosition({}) {
  const [visible, setVisible] = useState(false);
  const { loading } = useContext(ThemeContext);
  const router = useRouter();
  const isMobile = useMediaQuery({ query: "(max-width: 959px)" });
  const isRouter = router.pathname !== "/" && router.pathname !== "/mycompany" && isMobile;
  const handleClick = () => {
    setVisible(true);
  };
  return (
    <div className="container">
      <YourPositionStyle>
        {isRouter && !loading ? (
          <div className="searchWrapper">
            <Search />
          </div>
        ) : (
          false
        )}
        {loading ? (
          <p style={{ marginTop: isRouter ? "20px" : "105px" }}>
            Đang lấy vị trí...
          </p>
        ) : (
          !visible && (
            <YourPositionStyles
              onClick={handleClick}
              style={{ paddingTop: isRouter ? "20px" : "105px" }}
            />
          )
        )}
        {!loading && (
          <ModalLocation visible={visible} setVisible={setVisible} />
        )}
      </YourPositionStyle>
    </div>
  );
}

export default YourPosition;
