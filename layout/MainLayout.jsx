import styled from "styled-components";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";

import Header from "./components/Header";
import Footer from "./components/Footer";
import Context from "../Context/Context";


const YourPosition = dynamic(
  () => import("./components/yourPosition").then((module) => module),
  {
    ssr: false,
  }
);

const MainLayoutContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;

  > main {
    flex-grow: 1;
  }
  @media only screen and (max-width: 959px) {
    .container {
      padding : 0px 20px;
    }
  }
`;

export default function MainLayout({ children }) {
  const router = useRouter();
  const styles = {
    display: "flex",
    justifyContent: "center",
    paddingBottom: 25,
    alignItems: "center",
  };
  const isBoolShowPosition =
    router.pathname === "/" || router.pathname === "/search" || router.pathname === "/mycompany";
  return (
    <MainLayoutContainer>
      <Context>
        <Header />
        {isBoolShowPosition && <YourPosition />}
        <main
          style={
            router.pathname === "/registersuccess" || router.pathname === "/login"  ||  router.pathname === "/register" 
              ? { ...styles }
              : { paddingBottom: 25, paddingTop: isBoolShowPosition ? 0 : 108 }
          }
        >
          <div className="container">{children}</div>
        </main>

        <Footer />
      </Context>
    </MainLayoutContainer>
  );
}
